class Solution {
public:
    bool isChar(char ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }
    bool isPalindrome(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        for (int left = 0, right = s.length()-1; left<right; ++left, --right) {
            while (left<right && !isChar(s[left]) && !isdigit(s[left])) { ++left; }
            while (left<right && !isChar(s[right]) && !isdigit(s[right])) { --right; }
            if (left < right && (tolower(s[left]) != tolower(s[right])) ) { 
                return false;
            }
        }
        return true;
    }
};
