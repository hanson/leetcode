class Solution {
private:
    int dr[4] = {0, 1, 0, -1}, dc[4] = {1, 0, -1, 0};
    int row_start, row_end, col_start, col_end;
public:
    bool isValid(int cr, int cc) {
        return cr>=row_start && cr<=row_end && cc>=col_start && cc<=col_end;
    }
    vector<vector<int> > generateMatrix(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > ret(n, vector<int>(n));
        int cur_row = 0, cur_col = -1, dir = 0, cur_val = 1;
        row_start = 0, row_end = n-1, col_start = 0, col_end = n-1;
        while (row_start<=row_end && col_start<=col_end) {
            while (isValid(cur_row+dr[dir], cur_col+dc[dir])) {
                cur_row += dr[dir], cur_col += dc[dir];
                ret[cur_row][cur_col] = cur_val++;
            }
            if (dir == 0) { ++row_start; }
            else if (dir == 1) { --col_end; }
            else if (dir == 2) { --row_end; }
            else { ++col_start; }
            dir = (dir+1)%4;
        }
        return ret;
    }
};
