/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int sum;
public:
    void sumNumbersHelper(TreeNode *curNode, int curVal) {
        if (curNode == NULL) {
            return;
        }
        if (curNode->left==NULL && curNode->right==NULL) {
            sum += (curVal*10 + curNode->val);
            return;
        }
        curVal = curVal*10 + curNode->val;
        sumNumbersHelper(curNode->left, curVal);
        sumNumbersHelper(curNode->right, curVal);
    }
    int sumNumbers(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        sum = 0;
        sumNumbersHelper(root, 0);
        return sum;
    }
};
