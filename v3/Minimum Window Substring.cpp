class Solution {
public:
    string minWindow(string S, string T) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int cnt[128] = {0}, rt[128] = {0}, lenT = T.length();
        for (int i = 0; i < lenT; ++i) {
            ++cnt[ T[i] ], ++rt[ T[i] ];
        }
        int head=0, tail=-1, opt = INT_MAX, tmp = lenT;
        for (int h = 0, t = 0; t < S.length(); ++t) {
            if (rt[ S[t] ]) {
                --cnt[ S[t] ];
                if (cnt[ S[t] ] >= 0) {
                    --tmp;
                }
            }
            if (tmp==0) {
                do {
                    if (rt[ S[h] ] > 0) {
                        if (cnt[ S[h] ] < 0) {
                            ++cnt[ S[h] ];
                        }
                        else {
                            break;
                        }
                    }
                } while (++h);
                if (t-h+1 < opt) {
                    opt = t-h+1, head = h, tail = t;
                }
                //move to next window
                ++cnt[ S[h] ], ++tmp, ++h;
            }
        }
        return S.substr(head, tail-head+1);
    }
};
