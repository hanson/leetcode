/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<TreeNode *> generateTreesHelper(int left, int right) {
        vector<TreeNode *> ret;
        if (left > right) {
            ret.push_back(NULL);
            return ret;
        }
        TreeNode * cur;
        for (int  i = left; i <= right; ++i) {
            vector<TreeNode *> leftCs = generateTreesHelper(left, i - 1);
            vector<TreeNode *> rightCs = generateTreesHelper(i + 1, right);
            for (int k1 = 0; k1 < leftCs.size(); ++k1) {
                for (int k2 = 0; k2 < rightCs.size(); ++k2) {
                    cur = new TreeNode( i );
                    cur->left = leftCs[k1], cur->right = rightCs[k2];
                    ret.push_back(cur);
                }   
            }
        }
        return ret;
    }
    vector<TreeNode *> generateTrees(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return generateTreesHelper(1, n);
    }
};
