class Solution {
public:
    bool searchMatrix(vector<vector<int> > &matrix, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int m = matrix.size();
        if (m == 0) { return false; }
        int n = matrix[0].size();
        int curm = 0, curn = n-1;
        while (curm>=0 && curm<m && curn>=0 && curn<n) {
            if (matrix[curm][curn] == target) {
                return true;
            }
            else if (matrix[curm][curn] < target) {
                ++curm;
            }
            else {
                --curn;
            }
        }
        return false;
    }
};