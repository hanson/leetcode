class Solution {
public:
    int maximalRectangle(vector<vector<char> > &matrix) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int rows = matrix.size(), ans = 0, t;
        if (rows == 0) { return ans; }
        int cols = matrix[0].size();
        int *ones = (int *)malloc(sizeof(int) * cols);
        stack<pair<int, int>> os;
        for (int c = 0; c < cols; ++c) { ones[c] = 0; }
        for (int r = 0; r < rows; ++r) {
            for (int c = 0; c < cols; ++c) {
                if (matrix[r][c] == '1') { ++ones[c]; }
                else { ones[c] = 0; }
                if (os.empty() || ones[c] > os.top().first) {
                    os.push(make_pair(ones[c], c));
                }
                while (!os.empty() && ones[c] <= os.top().first) {
                    t = os.top().second;
                    ans = max(ans, os.top().first*(c-t));
                    os.pop();
                }
                os.push(make_pair(ones[c], t));
            }
            t = cols;
            while (!os.empty()) {
                ans = max(ans, os.top().first*(t-os.top().second));
                os.pop();
            }
        }
        return ans;
    }
};
