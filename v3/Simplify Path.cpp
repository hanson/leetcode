class Solution {
public:
    string simplifyPath(string path) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<string> s;
        int i, j, n = path.length();
        for (i = 0; i < n; ++i) {
            if (path[i] == '/') {
                for (j = i + 1; j < n && path[j] != '/'; ++j);
                string str = path.substr(i+1, j-i-1);
                if (str == "." || str.length() == 0) { continue; }
                if (str == "..") {
                    if (s.size() > 0) {
                        s.pop_back();
                    }
                    continue;
                }
                s.push_back("/"+str);
            }
        }
        if (s.size() == 0) {
            s.push_back("/");
        }
        string ret;
        for (int i = 0; i < s.size(); ++i) {
            ret += s[i];
        }
        return ret;
    }
};