class Solution {
public:
    int longestValidParentheses(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        stack<int> ps;
        int ans = 0, stPoint = 0, tmp;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '(') {
                ps.push(i);
            }
            else {
                if (ps.empty()) {
                    stPoint = i+1;
                }
                else {
                    ps.pop();
                    tmp = stPoint;
                    if (!ps.empty() && ps.top() >= stPoint) {
                        tmp = ps.top() + 1;
                    }
                    ans = max(ans, i - tmp + 1);
                }
            }
        }
        return ans;
    }
};
