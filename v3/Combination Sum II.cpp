class Solution {
private:
    vector<vector<int> > coms;
    vector<int> tmpCom;
public:
    void combinationSumHelper(const vector<int> &candidates, int sum, int index, int target) {
        if (sum == target) { coms.push_back(tmpCom); return; }
        if (sum>target || index>=candidates.size()) { return; }
        int ti = index;
        while (ti+1<candidates.size() && candidates[ti+1]==candidates[ti]) { ++ti; }
        combinationSumHelper(candidates, sum, ti+1, target);
        for (int i = index; i<=ti; ++i) {
            tmpCom.push_back(candidates[i]);
            sum += candidates[i];
            combinationSumHelper(candidates, sum, ti+1, target);
        }
        for (int i = index; i <= ti; ++i) {
            tmpCom.pop_back();
        }
    }
    vector<vector<int> > combinationSum2(vector<int> &candidates, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        sort(candidates.begin(), candidates.end()), coms.clear(), tmpCom.clear();
        combinationSumHelper(candidates, 0, 0, target);
        return coms;
    }
};
