class Solution {
public:
    int maxProfit(vector<int> &prices) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n = prices.size(), i, maxP = 0, buy = INT_MAX, sell = INT_MIN, ans = 0;
        int *dp1 = (int*)malloc(sizeof(int) * n);
        int *dp2 = (int*)malloc(sizeof(int) * n); 
        for (i=0, maxP=0, buy=INT_MAX; i < prices.size(); ++i) {
            if (prices[i] < buy) {
                buy =  prices[i];
            }
            maxP = max(maxP, prices[i] - buy), dp1[i] = maxP;
        }
        for (i=n-1, maxP=0, sell=INT_MIN; i >= 0; --i) {
            if (prices[i] > sell) {
                sell = prices[i];
            }
            maxP = max(maxP, sell - prices[i]), dp2[i] = maxP;
        }
        for (int i = 0; i < n; ++i) {
            ans = max(ans, dp1[i]+dp2[i]);
        }
        return ans;
    }
};
