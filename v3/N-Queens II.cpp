class Solution {
private:
    int tot;
public:
    void totalNQueensHelper(int n, int row, int col, int ld, int rd) {
        if (row >= n) { 
            ++tot;
            return;
        }
        for (int c = 0; c < n; ++c) {
            if ((col | ld | rd) & (1<<c)) {
                continue;
            }
            totalNQueensHelper(n, row+1, col|1<<c, (ld|1<<c)>>1, (rd|1<<c)<<1);
        }
    }
    
    int totalNQueens(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        tot = 0;
        totalNQueensHelper(n, 0, 0, 0, 0);
        return tot;
    }
};
