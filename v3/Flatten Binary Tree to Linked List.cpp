/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL) { return; }
        stack<TreeNode *> ts;
        TreeNode *curNode, *preNode=NULL;
        ts.push(root);
        while (!ts.empty()) {
            curNode = ts.top();
            ts.pop();
            if (curNode->right != NULL) {
                ts.push(curNode->right);
                curNode->right = NULL;
            }
            if (curNode->left != NULL) {
                ts.push(curNode->left);
                curNode->left = NULL;
            }
            if (preNode == NULL) {
                preNode = curNode;
            }
            else {
                preNode->right = curNode;
                preNode = preNode->right;
            }
        }
    }
};
