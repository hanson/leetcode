/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool flag;
public:
    bool isSameTreeHelper(TreeNode *p, TreeNode *q) {
        if (p==NULL && q==NULL) {
            return true;
        }
        if (flag==false || (p==NULL^q==NULL)) {
            return false;
        }
        if (p->val != q->val) {
            return (flag = false);
        }
        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
    }
    bool isSameTree(TreeNode *p, TreeNode *q) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        flag = true;
        return isSameTreeHelper(p, q);
    }
};
