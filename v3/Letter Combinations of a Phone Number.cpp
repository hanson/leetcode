class Solution {
private:
    map<char, string> dic;
    vector<string> ret; 
public:
    void letterCombinationsHelper(const string &digits, int curIndex, string curStr) {
        if (curIndex >= digits.size()) {
            ret.push_back(curStr);
            return;
        }
        for (int i = 0; i < dic[ digits[curIndex] ].length(); ++i) {
            letterCombinationsHelper(digits, curIndex+1, curStr+dic[ digits[curIndex] ][i]);
        }
    }
    vector<string> letterCombinations(string digits) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        dic.insert( make_pair('2', "abc") );
        dic.insert( make_pair('3', "def") );
        dic.insert( make_pair('4', "ghi") );
        dic.insert( make_pair('5', "jkl") );
        dic.insert( make_pair('6', "mno") );
        dic.insert( make_pair('7', "pqrs") );
        dic.insert( make_pair('8', "tuv") );
        dic.insert( make_pair('9', "wxyz") );
        string curStr; 
        ret.clear();
        letterCombinationsHelper(digits, 0, curStr);
        return ret;
    }
};