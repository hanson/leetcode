/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *partition(ListNode *head, int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode *ret = new ListNode(INT_MIN), *lTail = ret, *tmp, *pre=ret;
        ret->next = head;
        while (head != NULL) {
            if (head->val < x) {
                tmp = lTail->next;
                if (tmp != head) {
                    lTail->next = head;
                    pre->next = head->next;
                    head->next = tmp;
                }
                lTail = lTail->next;
            }
            pre = head, head = head->next;
        }
        return ret->next;
    }
};
