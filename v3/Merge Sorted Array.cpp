class Solution {
public:
    void merge(int A[], int m, int B[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int len = m+n-1;
        while (n > 0) {
            A[len--] = ((m>0?A[m-1]:INT_MIN) < B[n-1] ? B[--n] : A[--m]);
        }
    }
};
