class Solution {
public:
    vector<vector<int> > subsets(vector<int> &S) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n = S.size();
        vector<vector<int> >ret;
        sort(S.begin(), S.end());
        for (int i = 0; i < (1<<n); ++i) {
            vector<int> tmp;
            for (int k = 0; k < n; ++k) {
                if (i&(1<<k)) {
                   tmp.push_back(S[k]); 
                }
            }
            ret.push_back(tmp);
        }
        return ret;
    }
};
