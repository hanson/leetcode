class Solution {
public:
    int romanToInt(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        map<char, int> dic;
        dic.insert(make_pair('I', 1));
        dic.insert(make_pair('V', 5));
        dic.insert(make_pair('X', 10));
        dic.insert(make_pair('L', 50));
        dic.insert(make_pair('C', 100));
        dic.insert(make_pair('D', 500));
        dic.insert(make_pair('M', 1000));
        int ret = 0, cur, pre = -1;
        for (int i = s.length() - 1; i >= 0; --i) {
            cur = dic[s[i]];
            if (cur < pre) {
                cur = -cur;
            }
            ret += cur, pre = abs(cur);
        }
        return ret;
    }
};