class Solution {
public:
    double pow(double x, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        long long tn = (long long)n;
        if (tn == 0) { return 1; }
        if (tn < 0) { x = 1.0/x, tn = -tn; }
        double ret = 1;
        while (tn) {
            if (tn & 1) {
                ret *= x;
            }
            x *= x;
            tn >>= 1;
        }
        return ret;
    }
};
