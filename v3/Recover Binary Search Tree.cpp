/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void recoverTree(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        TreeNode *tmp, *f1 = NULL, *f2 = NULL, *pre = NULL, *cur = root;
        while (cur != NULL) {
            if (cur->left != NULL) {
                tmp = cur->left;
                while (tmp->right != NULL && tmp->right != cur) {
                    tmp = tmp->right;
                }
                if (tmp->right == cur) {
                    tmp->right = NULL;
                    if (pre != NULL && pre->val > cur->val) {
                        if (f1 == NULL) {
                            f1 = pre;
                        }
                        f2 = cur;
                    }
                    pre = cur;
                    cur = cur->right;
                    continue;
                }
                tmp->right = cur;
                cur = cur->left;
            }
            else {
                if (pre != NULL && pre->val > cur->val) {
                    if (f1 == NULL) {
                        f1 = pre;
                    }
                    f2 = cur;
                }
                pre = cur;
                cur = cur->right;
            }
        }
        swap(f1->val, f2->val);
    }
};