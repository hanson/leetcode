class Solution {
public:
    string addBinary(string a, string b) {
        int cura = a.length()-1, curb = b.length()-1, carry=0, tmp;
        string ret="";
        while (cura>=0 || curb>=0 || carry>0) {
            tmp = (cura>=0 ? (a[cura]-'0') : 0) + (curb>=0 ? (b[curb]-'0') : 0) + carry;
            ret += (char)(int(tmp&1) + '0');
            carry = (tmp&2)>>1;
            --cura, --curb;
        }
        reverse(ret.begin(), ret.end());
        return ret;
    }
};
