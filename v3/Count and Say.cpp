class Solution {
public:
    string countAndSay(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        string strs[2];
        int cur, i, j, k, len;
        for (i=1, cur=0, strs[cur]="1"; i < n; ++i, cur=(1-cur)) {
            for (len=strs[cur].length(), j=0, k=j, strs[1-cur]=""; j < len; j=k+1, k=j) {
                while (k+1<len && strs[cur][k+1]==strs[cur][k]) { ++k; }
                strs[1 - cur].push_back( k+1-j+'0' );
                strs[1 - cur].push_back( strs[cur][j] );
            }
        }
        return strs[cur];
    }
};