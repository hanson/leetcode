class Solution {
private:
    int c[2][2], cc[2][2];
public:
    void mul(int ta[2][2], int tb[2][2]) {
        int t[2][2], i, j, k;
        for (i=0;i<2;i++) {
            for (j=0; j<2;j++) {
                for (k=0, t[i][j]=0;k<2;k++) {
                    t[i][j] += ta[i][k]*tb[k][j];
                }
            }
        }
        for (i=0; i<2; i++) {
            for (j=0; j<2; j++) {
                cc[i][j]=t[i][j];
            }
        }
    }
    void getcc(int n){
        if (n<=1) { cc[0][0] = cc[0][1] = cc[1][0] = 1, cc[1][1]=0; return;}
        getcc( n>>1 );
        mul(cc, cc);
        if (n & 1) { mul(cc, c); }
    }
    int climbStairs(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        c[0][0] = c[0][1] = c[1][0] = 1, c[1][1]=0;
        if (n==1) {
            return 1;
        }
        getcc(n-1);
        return cc[0][0] + cc[0][1];
    }
};
