class Solution {
private:
    vector<vector<int> >ups;
public:
    void permuteUniqueHelper(vector<int> &num, vector<int> &cur, vector<bool> &used, int index) {
        if (index >= num.size()) {
            ups.push_back(cur);
            return;
        }
        int last = INT_MIN;
        for (int i = 0; i < num.size(); ++i) {
            if (used[i]==true || last==num[i]) {
                continue;
            }
            used[i] = true;
            last = num[i];
            cur.push_back(last);
            permuteUniqueHelper(num, cur, used, index+1);
            cur.pop_back();
            used[i] = false;
        }
    }
    vector<vector<int> > permuteUnique(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ups.clear();
        sort(num.begin(), num.end());
        vector<bool> used(num.size(), false);
        vector<int> cur;
        permuteUniqueHelper(num, cur, used, 0);
        return ups;
    }
};
