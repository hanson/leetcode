/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
private:
    class heapElem {
    public:
        int index;
        int val;
        heapElem(int ti, int tv):index(ti),val(tv) {
        }
    };
public:
    ListNode *mergeKLists(vector<ListNode *> &lists) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int num = lists.size();
        if (num == 0) {
            return NULL;
        }
        vector<heapElem*> heap;
        int heapSize = 0;
        for (int i = 0; i < num; ++i) {
            if (lists[i] == NULL) {
                continue;
            }
            //insert node into heap
            heap.push_back(new heapElem(i, lists[i]->val));
            int cur = heapSize++;
            int par = (cur-1)/2;
            while (par>=0 && heap[par]->val>heap[cur]->val) {
                swap(heap[par], heap[cur]);
                cur = par;
                par = (par-1)/2;
            }
            lists[i] = lists[i]->next;
        }
        if (heapSize == 0) {
            return NULL;
        }
        ListNode *thead = NULL;
        ListNode *head = new ListNode(heap[0]->val);
        thead = head;
        while (heapSize) {
            if (lists[heap[0]->index] == NULL) {
                swap(heap[0], heap[heapSize-1]);
                --heapSize;
            }
            else {
                heap[0]->val = lists[heap[0]->index]->val;
                lists[heap[0]->index] = lists[heap[0]->index]->next;
            }
            if (heapSize == 0) {
                break;
            }
            int cur = 0;
            while (cur < (heapSize/2)) {
                int sel = 2*cur + 1, right = sel+1;
                if (right < heapSize && heap[right]->val<heap[sel]->val) {
                    sel = right;
                }
                if (heap[sel]->val > heap[cur]->val) {
                    break;
                }
                swap(heap[sel], heap[cur]);
                cur = sel;
            }
            thead->next = new ListNode(heap[0]->val);
            thead = thead->next;
        }
        return head;
    }
};
