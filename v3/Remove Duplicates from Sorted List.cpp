/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode *thead = head;
        while (thead!=NULL && thead->next!=NULL) {
            if (thead->val == thead->next->val) {
                thead->next = thead->next->next;
                continue;
            }
            thead = thead->next;
        }
        return head;
    }
};
