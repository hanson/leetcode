class Solution {
public:
    vector<int> plusOne(vector<int> &digits) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i;
        for (i = digits.size()-1; i>=0 && digits[i]==9; digits[i]=0, --i);
        if (i < 0) {
            digits.insert(digits.begin(), 1);
        }
        else {
            ++digits[i];
        }
        return digits;
    }
};
