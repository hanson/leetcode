class Solution {
public:
    vector<int> getRow(int rowIndex) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int **triangle = (int**)malloc(sizeof(int*)*2), cur = 0;
        for (int i = 0; i < 2; ++i) {
            triangle[i] = (int*)malloc(sizeof(int)*(rowIndex+1));
        }
        for (int i = 0; i <= rowIndex; ++i, cur=cur^1) {
            for (int j = 0; j <= i; ++j) {
                if (j==0 || j==i) {
                    triangle[cur][j] = 1;
                    continue;
                }
                triangle[cur][j] = triangle[cur^1][j-1] + triangle[cur^1][j];
            }
        }
        vector<int> ret;
        for (int i = 0; i <= rowIndex; ++i) {
            ret.push_back(triangle[1-cur][i]);
        }
        return ret;
    }
};
