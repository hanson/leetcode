class Solution {
public:
    int numDistinct(string S, string T) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int lens = S.length(), lent = T.length(), i, j;
        if (lens < lent) { return 0; }
        int **dp = (int **)malloc(sizeof(int*)*(lens+1));
        for (int i = 0; i <= lens; ++i) {
            dp[i] = (int *)malloc(sizeof(int)*(lent+1));
        }
        for (i = 0; i < lent; ++i) {
            dp[0][i] = 0;
        }
        for (i=0; i <= lens; ++i) {
            dp[i][0] = 1;
        }
        for (i = 0; i < lent; ++i) {
            for (j=i, dp[j][i+1]=0; j < lens; ++j) {
                dp[j+1][i+1] = dp[j][i+1];
                if (S[j] == T[i]) {
                    dp[j+1][i+1] += dp[j][i];
                }
            }
        }
        return dp[lens][lent];
    }
};
