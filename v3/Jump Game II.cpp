class Solution {
public:
    int jump(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int bound = 0, next = 0, i, step = 0;
        for (i = 0; i<n && i<=next; ++i) {
            if (i > bound) {
                bound = next, ++step;
            }
            next = max(next, i+A[i]);
        }
        if (i == n) {
            return step;
        }
        return -1;
    }
};
