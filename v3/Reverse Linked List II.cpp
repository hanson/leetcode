/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseBetween(ListNode *head, int m, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (head == NULL) { return head; }
        ListNode *ret = new ListNode(0), *cur, *thead, *tmp, *pre;
        int index = 0;
        ret->next = head, thead = ret;
        while (index < m-1) {
            ++index, thead = thead->next;
        }
        pre = thead->next, cur = pre->next, tmp = cur, index = m + 1;
        while (index++ <= n) {
            tmp = cur->next, cur->next = pre;
            pre = cur, cur = tmp;
        }
        thead->next->next = tmp, thead->next = pre;
        return ret->next;
    }
};
