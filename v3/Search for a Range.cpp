class Solution {
public:
    vector<int> searchRange(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> ret(2);
        int left = 0, right = n - 1, mid;
        while (left <= right) {
            mid = left + (right-left)/2;
            if (A[mid] >= target) { right = mid - 1; }
            else { left = mid + 1; }
        }
        ret[0] = (left < n && A[left] == target) ? left : -1;
        left = 0, right = n - 1;
        while (left <= right) {
            mid = left + (right-left)/2;
            if (A[mid] <= target) { left = mid + 1; }
            else { right = mid - 1; }
        }
        ret[1] = (right >= 0 && A[right] == target) ? right : -1;
        return ret;
    }
};
