class Solution {
public:
    bool isValidSudoku(vector<vector<char> > &board) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> cols(9, 0), rows(9, 0), grids(9, 0);
        for (int r = 0; r < board.size(); ++r) {
            for (int c = 0; c < board[r].size(); ++c) {
                if (board[r][c] == '.') {
                    continue;
                }
                int t = board[r][c] - '1';
                if ( (rows[r]>>t) & 1 ) { return false; }
                rows[r] |= (1<<t);
                if ( (cols[c]>>t) & 1 ) { return false; }
                cols[c] |= (1<<t);
                if ( (grids[ 3*(r/3) + c/3]>>t) & 1 ) { return false; }
                grids[ 3*(r/3) + c/3] |= (1<<t);
            }
        }
        return true;
    }
};
