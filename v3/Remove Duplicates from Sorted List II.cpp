/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (head == NULL) { return head; }
        ListNode *ret = new ListNode(0), *tHead, *cur;
        ret->next = head, tHead = ret;
        while (tHead->next != NULL) {
            cur = tHead->next;
            while (cur->next != NULL && cur->next->val == cur->val) {
                cur = cur->next;
            }
            if (tHead->next != cur) {
                tHead->next = cur->next;
            }
            else {
                tHead = tHead->next;
            }
        }
        return ret->next;
    }
};
