class Solution {
public:
    string longestPalindrome(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        string ts = "#";
        for (int i = 0; i < s.length(); ++i) {
            ts += s[i], ts += '#';
        }
        int len = ts.length(), mx = 0, index;
        int *p = (int*)malloc(sizeof(int)*len);
        for (int i = 0; i < len; ++i) {
            p[i] = 1;
            if (i < mx) {
                p[i] = min(p[2*index - i], mx-i);
            }
            for(; i+p[i]<len && i-p[i]>=0 && ts[i+p[i]] == ts[i-p[i]]; p[i]++);
            if (p[i]+i > mx) {
                mx = p[i] + i, index = i;
            }
        }
        int opt = -1;
        for (int i = 0; i < len; ++i) {
            if (p[i] > opt) {
                opt = p[i], index = i;
            }
        }
        s = "";
        for (int i = index - (p[index]-1); s.length() < p[index]-1; ++i) {
            if (ts[i] != '#') {
                s += ts[i];
            }
        }
        return s;
    }
};
