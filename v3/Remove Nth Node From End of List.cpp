/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode *tail = head;
        while (n) {
            tail = tail->next;
            --n;
        }
        ListNode *thead = head;
        ListNode *pre = NULL;
        while (tail != NULL) {
            tail = tail->next;
            pre = thead;
            thead = thead->next;
        }
        if (thead == head) {
            head = thead->next;
        }
        else { 
            pre->next = thead->next;
        }
        return head;
    }
};
