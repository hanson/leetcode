class Solution {
private:
    vector<vector<int> > ps;
public:
    void permuteHelper(vector<int> &num, int index) {
        if (index >= num.size()) {
            ps.push_back(num);
            return;
        }
        for (int i = index; i < num.size(); ++i) {
            swap(num[index], num[i]);
            permuteHelper(num, index+1);
            swap(num[index], num[i]);
        }
    }
    vector<vector<int> > permute(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ps.clear();
        permuteHelper(num, 0);
        return ps;
    }
};
