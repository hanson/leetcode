class Solution {
public:
    int firstMissingPositive(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i, tmp;
        for (i = 0; i < n; ++i) {
            if (A[i] <= 0) {
                A[i] = n+1;
            }
        }
        for (i = 0; i < n; ++i) {
            tmp = abs(A[i]);
            if (tmp<n+1 && A[tmp-1]>0) {
                A[ tmp-1 ] = -A[ tmp-1 ];
            }
        }
        for (i = 0; i<n && A[i]<0; ++i);
        return i+1;
    }
};
