/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool flag;
public:
    bool hasPathSumHelper(TreeNode *curNode, int sum, int preSum) {
        if (curNode == NULL) { return false; }
        if (flag) { return true; }
        if (curNode->left==NULL && curNode->right==NULL) {
            if (preSum + curNode->val == sum) {
                flag = true;
            }
            return flag;
        }
        return flag || hasPathSumHelper(curNode->left, sum, preSum+curNode->val) 
        || hasPathSumHelper(curNode->right, sum, preSum+curNode->val);
    }
    bool hasPathSum(TreeNode *root, int sum) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        flag = false;
        return hasPathSumHelper(root, sum, 0);
    }
};
