class Solution {
public:
    void sortColors(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int red = 0, blue = n-1;
        for (int i = red; i <= blue; ++i) {
            if (A[i] == 0) {
                swap(A[red++], A[i]);
            }
            else if (A[i] == 2) {
                swap(A[blue--], A[i--]);
            }
        }
    }
};

