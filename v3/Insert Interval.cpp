/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<Interval> ret;
        int index = 0, len = intervals.size();
        while (index<len && newInterval.start>intervals[index].end) {
            ret.push_back( intervals[index] ), index++;
        }
        while (index<len && newInterval.end>=intervals[index].start) {
            newInterval.start = min(newInterval.start, intervals[index].start);
            newInterval.end = max(newInterval.end, intervals[index].end);
            ++index;
        }
        ret.push_back(newInterval);
        while (index < len) {
            ret.push_back(intervals[index]), ++index;
        }
        return ret;
    }
};
