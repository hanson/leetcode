class Solution
{
    private:
        int row, col, dr[4] = {0, 0, -1, 1}, dc[4]={1, -1, 0, 0};
        class Point {
            public:
            int r, c;
            Point(int tr, int tc): r(tr), c(tc) {}
        };
    public:
        bool isvalid(int r, int c) {
            return (r>=0 && r<row && c>=0 && c<col);
        }
        void bfs(vector<vector<char> > &board, int r, int c) {
            Point *cur;
            queue<Point*> que;
            board[r][c] = 'A';
            que.push(new Point(r, c));
            while (que.empty() == false) {
                cur = que.front(), que.pop();
                for (int i = 0; i < 4; ++i) {
                    int tr = cur->r + dr[i], tc = cur->c + dc[i];
                    if (isvalid(tr, tc) == false) { continue; }
                    if (board[tr][tc] == 'O') {
                        board[tr][tc] = 'A';
                        que.push(new Point(tr, tc));
                    }
                }
            }
        }
        void dealwith(vector<vector<char> > &board) {
            int c, r;
            //up most row
            for (c = 0; c < col; ++c) {
                if (board[0][c] == 'O') { bfs(board, 0, c); }
            }
            //left most col
            for (r = 0; r < row; ++r) {
                if (board[r][col-1] == 'O') { bfs(board, r, col-1); }
            }
            //down most row
            for (c = 0; c < col; ++c) {
                if (board[row-1][c] == 'O') { bfs(board, row-1, c); }
            }
            //right most col
            for (r = 0; r < row; ++r) {
                if (board[r][0] == 'O') { bfs(board, r, 0); }
            }
        }
        void solve(vector<vector<char> > &board) {
            row = board.size();
            if (row <= 0) { return; }
            col = board[0].size();
            dealwith(board);
            for (int r = 0; r < row; ++r) {
                for (int c = 0; c < col; ++c) {
                    if (board[r][c] == 'O') { board[r][c] = 'X'; }
                    else if (board[r][c] == 'A') { board[r][c] = 'O'; }
                }
            }
        }
};
