/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        if (l1==NULL ) { return l2; }
        int carry = 0;
        ListNode *pre = NULL, *cur = l1;
        while ((cur!=NULL && l2!=NULL) || carry) {   
            if (cur==NULL && l2==NULL) {
                pre->next = new ListNode(carry); break;
            }
            if (cur == NULL) {
                cur = l2, pre->next = cur, l2 = NULL;
            }
            cur->val += (carry + (l2!=NULL?l2->val:0));
            carry = cur->val/10, cur->val %= 10;
            pre = cur; cur = cur->next; l2 = (l2==NULL?NULL:l2->next);
        }
        if (l2 != NULL) {
            cur = l2, pre->next = cur, l2 = NULL;
        }
        return l1;
    }
};
