class Solution {
public:
    int numTrees(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int *cnt = (int*)malloc(sizeof(int)*(n+1)), i, j;
        for (i = 2, cnt[0] = cnt[1] = 1; i <= n; ++i) {
            cnt[i] = 0;
            for (j = 0; j < i; ++j) {
                cnt[i] += (cnt[j]*cnt[i-1-j]);
            }
        }
        return cnt[n];
    }
};
