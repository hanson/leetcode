class Solution {
public:
    int minimumTotal(vector<vector<int> > &triangle) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        for (int r = triangle.size()-1-1; r >= 0; --r) {
            for (int c = 0; c < triangle[r].size(); ++c) {
                triangle[r][c] += min(triangle[r+1][c], triangle[r+1][c+1]); 
            }
        }
        return triangle[0][0];
    }
};
