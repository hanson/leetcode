class Solution {
private:
    bool ***iss;
public:
    bool isScrambleHelper(string &s1, int b1, string s2, int b2, int len) {
        if (len <= 0) { return true; }
        if (len==1) {
            iss[b1][b2][len] = (s1[b1]==s2[b2]);
            return iss[b1][b2][len];
        }
        string str1 = s1.substr(b1, len);
        string str2 = s2.substr(b2, len);
        if (str1 == str2) { 
            iss[b1][b2][len] = true;
            return iss[b1][b2][len];
        }
        sort(str1.begin(), str1.end()), sort(str2.begin(), str2.end());
        if (str1 != str2) {
            iss[b1][b2][len] = false;
            return iss[b1][b2][len];
        }
        for (int i = 1; i < len && !iss[b1][b2][len]; ++i) {
            iss[b1][b2][len] = (
            (isScrambleHelper(s1, b1, s2, b2, i) && isScrambleHelper(s1, b1+i, s2, b2+i, len-i))||
            (isScrambleHelper(s1, b1, s2, b2+len-i, i) && isScrambleHelper(s1, b1+i, s2, b2, len-i))
                );
        }
        return iss[b1][b2][len];
    }
    bool isScramble(string s1, string s2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int len = s1.length();
        iss = (bool ***)malloc( sizeof(bool*)*(len+1) );
        for (int i = 0; i <= len; ++i) {
            iss[i] = (bool**)malloc( sizeof(bool*)*(len+1) );
            for (int j = 0; j <= len; ++j) {
                iss[i][j] = (bool*)malloc( sizeof(bool)*(len+1) );
                for (int k = 0; k <= len; ++k) {
                    iss[i][j][k] = false;
                }
            }
        }
        bool ret = isScrambleHelper(s1, 0, s2, 0, len);
        free(iss);
        return ret;
    }
};
