class Solution {
private:
    vector<vector<int> > coms;
    vector<int> tmpCom;
public:
    void combinationSumHelper(const vector<int> &candidates, int sum, int index, int target) {
        if (sum > target) { return; }
        if (sum == target) { coms.push_back(tmpCom); return; };
        for (int i = index; i < candidates.size(); ++i) {
            tmpCom.push_back(candidates[i]);
            combinationSumHelper(candidates, sum+candidates[i], i, target);
            tmpCom.pop_back();
        }
    }
    vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        sort(candidates.begin(), candidates.end()), coms.clear(), tmpCom.clear();
        combinationSumHelper(candidates, 0, 0, target);
        return coms;
    }
};
