class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int ans = 0, head, tail;
        vector<bool> occur(128, false);
        for (head = tail = 0; tail < s.length(); ++tail) {
            if (occur[ s[tail] ] == false) {
                occur[ s[tail] ] = true;
            }
            else {
                while (head < tail && s[head] != s[tail]) {
                    occur[ s[head] ] = false, ++head;
                }
                if (head < tail) {
                    ++head;
                }
            }
            ans = max(ans, tail-head+1);
        }
        return ans;
    }
};