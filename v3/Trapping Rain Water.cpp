class Solution {
public:
    int trap(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (n == 0) { return 0; }
        int mx, i, opt, ans = 0, index = -1;
        for (i = 0, mx = -1; i < n; ++i) {
            if (A[i] > mx) {
                mx = A[i], index = i;
            }
        }
        for (i = 0, opt = 0; i < index; ++i) {
            opt = max(opt, A[i]);
            ans += (opt-A[i]);
        }
        for (i = n-1, opt = 0; i > index; --i) {
            opt = max(opt, A[i]);
            ans += (opt-A[i]);
        }
        return ans;
    }
};
