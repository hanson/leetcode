class Solution {
public:
    int divideHelper(long long dividend, long long divisor) {
        assert(divisor != 0);
        if (abs(divisor)==1 || dividend==0) { return divisor*dividend; }
        long long sign = 1-(dividend<0^divisor<0)*2, ret=0, k, dd = abs(dividend), dr = abs(divisor);
        for (k = 0; dr <= dd; ++k, dr=(dr<<1));
        while (dd > 0) {
            while (dr>dd && dr>abs(divisor)) {
                dr = (dr>>1), --k;
            }
            if (dr > dd) {
                break;
            }
            ret += (1<<k), dd -= dr;
        }
        return sign*ret;
    }
    
    int divide(int dividend, int divisor) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return divideHelper(dividend, divisor);
    }
};
