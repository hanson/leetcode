/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode * reverseKGroupHelper(ListNode *head, ListNode *pHead, int k) {
        ListNode *tmpHead=head, *preHead=head, *curHead=preHead->next;
        for (; k>1 && curHead!=NULL; --k) {
            tmpHead = curHead->next, curHead->next = preHead;
            preHead = curHead, curHead = tmpHead;
        }
        head->next = curHead, pHead->next = preHead;
        return head;
    }
    ListNode *reverseKGroup(ListNode *head, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
       if (head==NULL || k==0 || k==1) { return head; }
        ListNode *retNode = new ListNode(0), *preHead = retNode;
        retNode->next = head;
        bool first = true;
        while (preHead != NULL) {
            ListNode *tmpHead = preHead->next;
            int cnt = 0;
            for (; tmpHead != NULL; ++cnt) {
                if (cnt == k) { break; }
                tmpHead = tmpHead->next;
            }
            if (cnt < k) { break; }
            preHead = reverseKGroupHelper(preHead->next, preHead, k);
        }
        return retNode->next;
    }
};