/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *buildTreeHelper(const vector<int> &preorder, int ps, int pe, const vector<int> &inorder, int is, int ie){
        if (ps > pe) { return NULL; }
        TreeNode *curNode = new TreeNode(preorder[ps]);
        int p;
        for (p = is; p <= ie; ++p) {
            if (inorder[p] == preorder[ps]) { break; }
        }
        curNode->left = buildTreeHelper(preorder, ps+1, ps+p-is, inorder, is, p-1);
        curNode->right = buildTreeHelper(preorder, ps+p-is+1, pe, inorder, p+1, ie);
        return curNode;
    }
    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return buildTreeHelper(preorder, 0, preorder.size()-1, inorder, 0, inorder.size()-1);
    }
};
