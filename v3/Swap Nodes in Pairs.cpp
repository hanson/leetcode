/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *swapPairs(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (head == NULL) { return head; }
        ListNode *ret = new ListNode(0), *cur, *tmp;
        ret->next = head, head = ret;
        while (head->next != NULL) {
            cur = head->next;
            if (cur->next == NULL) { 
                break; 
            }
            head->next = cur->next;
            tmp = cur->next->next;
            cur->next->next = cur;
            cur->next = tmp;
            head = head->next->next;
        }
        return ret->next;
    }
};
