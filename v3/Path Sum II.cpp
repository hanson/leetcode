/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<vector<int> > pathes;
    vector<int> path;
public:
    void pathSumHelper(TreeNode *curNode, int sum, int preSum) {
        if (curNode == NULL) { return; }
        if (curNode->left==NULL && curNode->right==NULL) {
            if (preSum + curNode->val == sum) {
                path.push_back(curNode->val);
                pathes.push_back(path);
                path.pop_back();
            }
            return;
        }
        path.push_back(curNode->val);
        pathSumHelper(curNode->left, sum, preSum+curNode->val);
        pathSumHelper(curNode->right, sum, preSum+curNode->val);
        path.pop_back();
    }
    vector<vector<int> > pathSum(TreeNode *root, int sum) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        pathes.clear();
        pathSumHelper(root, sum, 0);
        return pathes;
    }
};
