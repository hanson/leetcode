class Solution {
public:
    int minCut(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n = s.length();
        if (n == 0) { return 0; }
        bool **isp = (bool**)malloc(sizeof(bool*) * n);
        for (int i = 0; i < n; ++i) {
            isp[i] = (bool*)malloc(sizeof(bool) * n);
        }
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                isp[i][j] = false;
            }
        }
        int *dp = (int*)malloc(sizeof(int) * (n+1));
        for (int i = 0; i <= n; ++i) {
            dp[i] = n - i;
        }
        for (int i = n - 1; i >= 0; --i) {
            for (int j = i; j < n; ++j) {
                if (s[i] == s[j] && ( j-i<2 || isp[i+1][j-1])) {
                    dp[i] = min(dp[i], 1 + dp[j+1]), isp[i][j] = true;
                }
            }
        }
        return dp[0] - 1;
    }
};
