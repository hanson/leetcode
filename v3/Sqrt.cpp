class Solution {
public:
    int sqrt(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        double ans = 1;
        while (fabs(ans*ans - x) > 1e-6) {
            ans = (x + ans*ans)/(2*ans);
        }
        return (int)ans;
    }
};
