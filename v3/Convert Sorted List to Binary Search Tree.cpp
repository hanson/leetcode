/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *sortedListToBSTHelper(const vector<int> &nums, int left, int right) {
        if (left > right) { return NULL; }
        int mid = left + (right - left)/2;
        TreeNode *curNode = new TreeNode(nums[mid]);
        curNode->left = sortedListToBSTHelper(nums, left, mid - 1);
        curNode->right = sortedListToBSTHelper(nums, mid + 1, right);
        return curNode;
    }
    
    TreeNode *sortedListToBST(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> nums;
        while (head != NULL) {
            nums.push_back(head->val), head = head->next;
        }
        return sortedListToBSTHelper(nums, 0, nums.size()-1);
    }
};
