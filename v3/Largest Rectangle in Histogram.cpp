class Solution {
public:
    int largestRectangleArea(vector<int> &height) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        stack<pair<int, int> > hs;
        int ans = 0, i, tmp;
        for (i = 0; i < height.size(); ++i) {
            if (hs.empty() || hs.top().first < height[i]) { 
                hs.push(make_pair(height[i], i)); 
                continue;
            }
            while (!hs.empty() && hs.top().first>=height[i]) {
                tmp = hs.top().second;
                ans = max(ans, hs.top().first*(i-tmp));
                hs.pop();
            }
            hs.push(make_pair(height[i], tmp));
        }
        while (!hs.empty()) {
            ans = max(ans, hs.top().first*(i-hs.top().second));
            hs.pop();
        }
        return ans;
    }
};