class Solution {
public:
    vector<vector<int> > threeSum(vector<int> &num) {
        vector<vector<int> >trips;
        sort(num.begin(), num.end());
        for (int i=0, n=num.size(); i < n; ++i) {
            for (int j=i+1, k=n-1; j < k; ) {
                if (num[i]+num[j]+num[k] == 0) {
                    vector<int> tmpTrip;
                    tmpTrip.push_back(num[i]), tmpTrip.push_back(num[j]), tmpTrip.push_back(num[k]);
                    trips.push_back(tmpTrip);
                }
                int tmpj = j, tmpk = k;
                while (j<tmpk && num[tmpk-1]==num[tmpk]) { --tmpk; }
                while (tmpj<k && num[tmpj+1]==num[tmpj]) { ++tmpj; }
                if (abs(num[i]+num[j]+num[tmpk-1]) < abs(num[i]+num[tmpj+1]+num[k])) {
                    k = tmpk-1;
                }
                else { j = tmpj+1; }
            }
            while (i+1<n && num[i+1]==num[i]) { ++i; }
        }
        return trips;
    }
};