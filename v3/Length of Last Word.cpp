class Solution {
public:
    bool ischar(char ch) {
        return (ch>='a'&&ch<='z')||(ch>='A'&&ch<='Z');
    }
    int lengthOfLastWord(const char *s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int ans = 0;
        int cur = 0;
        while (s[cur] != '\0') {
            if (ischar(s[cur])) {
                ans = 1;
                while (s[++cur] != '\0' && ischar(s[cur])) {
                    ++ans;
                }
                continue;
            }
            ++cur;
        }
        return ans;
    }
};
