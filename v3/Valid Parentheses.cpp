class Solution {
public:
    bool isValid(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        stack<char> sta;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '(') { sta.push(')'); }
            else if (s[i] == '{') { sta.push('}'); }
            else if (s[i] == '[') { sta.push(']'); }
            else {
                if (sta.empty() || sta.top() != s[i]) {
                    return false;
                }
                sta.pop();
            }
        }
        return sta.empty();
    }
};
