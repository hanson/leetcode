/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *rotateRight(ListNode *head, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (head == NULL) { return head; }
        ListNode *ret = new ListNode(0), *thead = head, *tail = head;
        int n = 0;
        while (thead != NULL) {
            ++n, thead = thead->next;
        }
        k = k % n;
        if (k == 0) { return head; }
        while (k--) {
            tail = tail->next;
        }
        thead = head, ret->next = head;
        while (tail->next != NULL) {
            tail = tail->next, thead = thead->next;
        }
        ret->next = thead->next, tail->next = head, thead->next = NULL;
        return ret->next;
    }
};
