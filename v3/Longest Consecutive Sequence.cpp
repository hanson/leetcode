class Solution {
public:
    int longestConsecutive(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        unordered_set<int> numSet;
        unordered_set<int>::iterator it;
        int ans = 0, n = num.size(), i, k, tmp;
        for (i = 0; i < n; ++i) {
            numSet.insert(num[i]);
        }
        for (i = 0; i < n; ++i) {
            if ((it = numSet.find(num[i])) != numSet.end()) {
                tmp = 1, numSet.erase(it);
                for (k=num[i] - 1; (it=numSet.find(k)) != numSet.end(); --k) {
                    ++tmp, numSet.erase(it);
                }
                for (k=num[i] + 1; (it=numSet.find(k)) != numSet.end(); ++k) {
                    ++tmp, numSet.erase(it);
                }
                ans = max(ans, tmp);
            }
        }
        return ans;
    }
};