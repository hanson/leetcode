/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int maxSum;
public:
    int maxPathSumHelper(TreeNode *curNode) {
        if (curNode == NULL) {
            return 0;
        }
        int ls = maxPathSumHelper(curNode->left);
        int rs = maxPathSumHelper(curNode->right);
        if (ls < 0) { ls = 0; }
        if (rs < 0) { rs = 0; }
        maxSum = max(maxSum, ls+rs+curNode->val);
        return max(ls+curNode->val, rs+curNode->val);
    }
    
    int maxPathSum(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        maxSum = INT_MIN;
        maxSum = max(maxSum, maxPathSumHelper(root));
        return maxSum;
    }
};
