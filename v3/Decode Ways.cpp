class Solution {
public:
    int numDecodings(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int len = s.length(), i;
        if (len == 0) { return 0; }
        int *dp = (int *)malloc(sizeof(int) * (len+1));
        for (i=len-1, dp[len]=1; i >= 0; --i) {
            dp[i] = 0;
            if (s[i] == '0') { continue; }
            if (i+1>=len || s[i+1]!='0') { dp[i] = dp[i+1]; }
            if (i+2<=len && !(s[i]>'2' || (s[i]=='2' && s[i+1]>'6'))) {
                dp[i] += dp[i+2];
            }
        }
        return dp[0];
    }
};
