/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
//thread tree
class Solution {
public:
    vector<int> inorderTraversal(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> values;
        TreeNode *curNode = root, *tmpNode;
        while (curNode != NULL) {
            if (curNode->left != NULL) {
                tmpNode = curNode->left;
                while (tmpNode->right!=NULL && tmpNode->right!=curNode) {
                    tmpNode = tmpNode->right;
                }
                if (tmpNode->right == curNode) {
                    tmpNode->right = NULL;
                    values.push_back(curNode->val);
                    curNode = curNode->right;
                }
                else {
                    tmpNode->right = curNode;
                    curNode = curNode->left;
                }
            }
            else {
                values.push_back(curNode->val);
                curNode = curNode->right;
            }
        }
        return values;
    }
};
