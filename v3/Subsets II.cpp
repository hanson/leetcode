class Solution {
private:
    vector<vector<int> >ret;
public:
    void subsetsWithDupHelper(vector<int> &S, vector<int> &cur, int index) {
        if (index >= S.size()) {
            ret.push_back(cur);
            return;
        }
        int tmp = index;
        while (tmp+1<S.size() && S[tmp+1]==S[tmp]) { ++tmp; }
        subsetsWithDupHelper(S, cur, tmp+1);
        for (int i = index; i <= tmp; ++i) {
            cur.push_back(S[i]);
            subsetsWithDupHelper(S, cur, tmp+1);
        }
        for (int i = index; i <= tmp; ++i) { cur.pop_back(); }
    }
    vector<vector<int> > subsetsWithDup(vector<int> &S) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ret.clear();
        sort(S.begin(), S.end());
        vector<int> cur;
        subsetsWithDupHelper(S, cur, 0);
        return ret;
    }
};
