/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool flag;
public:
    int isBalancedHelper(TreeNode *curNode) {
        if (curNode==NULL || flag==false) { return 0; }
        int lh = isBalancedHelper(curNode->left);
        int rh = isBalancedHelper(curNode->right);
        if (abs(lh - rh) > 1) { flag = false; }
        return max(lh, rh) + 1;
    }
    
    bool isBalanced(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        flag = true;
        isBalancedHelper(root);
        return flag;
    }
};
