class Solution {
private:
    vector<vector<string> > boards;
    vector<int> cur;
public:
    void totalNQueensHelper(int n, int row, int col, int ld, int rd) {
        if (row >= n) {
            vector<string> tmp;
            for (int r = 0; r < n; ++r) {
                string str;
                for (int c = 0; c < n; ++c) {
                    if (cur[r] == c) {
                        str += 'Q';
                    }
                    else { str += '.'; }
                }
                tmp.push_back(str);
            }
            boards.push_back(tmp);
            return;
        }
        for (int c = 0; c < n; ++c) {
            if ((col | ld | rd) & (1<<c)) {
                continue;
            }
            cur.push_back(c);
            totalNQueensHelper(n, row+1, col|1<<c, (ld|1<<c)>>1, (rd|1<<c)<<1);
            cur.pop_back();
        }
    }
    vector<vector<string> > solveNQueens(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        boards.clear();
        totalNQueensHelper(n, 0, 0, 0, 0);
        return boards;
    }
};
