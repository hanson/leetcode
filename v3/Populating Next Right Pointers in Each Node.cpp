/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
    void connect(TreeLinkNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL) { return ; }
        queue<pair<TreeLinkNode *, int> > q;
        q.push(make_pair(root, 0));
        TreeLinkNode *curNode;
        int curDepth = 0;
        while (!q.empty()) {
            curNode = q.front().first, curDepth = q.front().second, q.pop();
            if (curNode->left != NULL) {
                q.push(make_pair(curNode->left, curDepth + 1));
            }
            if (curNode->right != NULL) {
                q.push(make_pair(curNode->right, curDepth + 1));
            }
            if (q.empty() || curDepth != q.front().second) {
                curNode->next = NULL;
            }
            else {
                curNode->next = q.front().first;
            }
        }
    }
};