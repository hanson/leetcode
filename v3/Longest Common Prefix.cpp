class Solution {
public:
    string longestCommonPrefix(vector<string> &strs) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n = strs.size(), lcp, flag = 1;
        if (n == 0) { return ""; }
        for (lcp = 0; lcp < strs[0].length() && flag; ++lcp) {
            for (int k = 1; k < n; ++k) {
                if (strs[k][lcp] != strs[0][lcp]) {
                    flag = 0, --lcp;
                    break;
                }
            }
        }
        return strs[0].substr(0, lcp);
    }
};
