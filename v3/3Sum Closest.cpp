class Solution {
public:
    int threeSumClosest(vector<int> &num, int target) {
        int n = num.size(), first = 1, ret;
        sort(num.begin(), num.end());
        for (int i = 0; i < n; ++i) {
            for (int j=i+1, k=n-1; j < k; ) {
                if (first==1 || abs(num[i]+num[j]+num[k]-target)<abs(ret-target)) {
                    ret = num[i]+num[j]+num[k], first=0;
                }
                if (abs(num[i]+num[j+1]+num[k]-target) < abs(num[i]+num[j]+num[k-1]-target)) {
                    ++j;
                }
                else { --k; }
            }
        }
        return ret;
    }
};
