class Solution {
public:
    int maxProfit(vector<int> &prices) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int buy = INT_MAX, ans = 0;
        for (int i = 0; i < prices.size(); ++i) {
            if (prices[i] < buy) {
                buy = prices[i]; continue;
            }
            ans = max(ans, prices[i]-buy);
        }
        return ans;
    }
};
