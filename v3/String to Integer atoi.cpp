class Solution {
public:
    int atoi(const char *str) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        while (*str == ' ') {
            ++str;
        } 
        int sign = 1;
        if (*str == '+') {
            ++str;
        }
        else if (*str == '-') {
            sign = -1, ++str;
        }
        long long tmp = 0;
        while (*str!='\0' && isdigit(*str)) {
            tmp = tmp * 10 + *str - '0', ++str;
        }
        tmp = tmp * sign;
        if (tmp < INT_MIN) { tmp = INT_MIN; }
        else if (tmp > INT_MAX) { tmp = INT_MAX; }
        return (int)tmp;
    }
};
