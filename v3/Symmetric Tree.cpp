/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isSymmetricHelper(TreeNode *n1, TreeNode *n2) {
        if (n1==NULL && n2==NULL) { return true; }
        if (n1==NULL ^ n2==NULL) {return false; }
        if (n1->val != n2->val) { return false; }
        return isSymmetricHelper(n1->left, n2->right) && isSymmetricHelper(n1->right, n2->left);
    }
    bool isSymmetric(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL) { return true; }
        return isSymmetricHelper(root->left, root->right) && isSymmetricHelper(root->right, root->left);
    }
};
