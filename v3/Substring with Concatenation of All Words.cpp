class Solution {
public:
    vector<int> findSubstring(string S, vector<string> &L) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n = L.size();
        vector<int> ret;
        if(n == 0) { return ret; }
        map<string, int> words, curState;
        int i, j, len = L[0].length(); 
        for(i = 0; i < n; ++i) {
            ++words[ L[i] ];
        }
        for(i = 0; i <= (int)S.length()-n*len; ++i) {
            curState.clear();
            for (j = 0; j < n; ++j) {
                string tmp = S.substr(i+j*len, len);
                if (words.find(tmp) == words.end()) {
                    break;
                }
                if (++curState[ tmp ] > words[tmp]) {
                    break;
                }
            }
            if (j == n) {
                ret.push_back(i);
            }
        }
        return ret;
    }
};
/*
class Solution {
public:
    vector<int> findSubstring(string S, vector<string> &L) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> ret;
        if (S.length() == 0 || L.size() == 0) { return ret; }
        map<int, string> dic;
        map<int, string>::iterator it, it2;
        map<string, int> words;
        map<string, int> curState;
        for (int i = 0, pos = 0; i < L.size(); ++i) {
            ++words[ L[i] ], pos = 0;
            if (words[ L[i] ] > 1) {
                continue;
            }
            while((pos = S.find(L[i], pos)) != string::npos) {
                dic.insert(make_pair(pos, L[i])), ++pos;
            }
        }
        int pre, k, len = L[0].length();
        for (it = dic.begin(); it != dic.end(); ++it) {
            curState.clear();
            for (k = 0, it2 = it, pre = -1; it2 != dic.end() && k < L.size(); ++k, it2=dic.find(pre+len)) {
                ++ curState[ it2->second ];
                if (pre!=-1 && curState[it2->second] > words[it2->second]) {
                    break;
                }
                pre = it2->first;
            }
            if (k == L.size()) {
                ret.push_back( it->first );
            }
        }
        return ret;
    }
};*/
