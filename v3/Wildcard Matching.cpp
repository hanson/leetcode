class Solution {
public:
    bool isMatch(const char *s, const char *p) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        const char *starIndex = '\0', *sIndex = s, *pIndex = p;
        while (*sIndex != '\0') {
            if (*pIndex == '*') {
                while (*pIndex == '*') {
                    ++pIndex;
                }
                starIndex = pIndex-1, s = sIndex;
            }
            else if (*pIndex == '?' || *pIndex == *sIndex) {
                ++pIndex, ++sIndex;
            }
            else if (*pIndex == '\0' || *pIndex != *sIndex) {
                if (starIndex == NULL) {
                    return false;
                }
                ++s, sIndex = s, pIndex = starIndex + 1;
            }
        }
        while (*pIndex == '*') {
            ++pIndex;
        }
        return *pIndex == '\0';
    }
};
