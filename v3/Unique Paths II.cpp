class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int rows = obstacleGrid.size();
        if (rows <= 0) { return 0; }
        int cols = obstacleGrid[0].size();
        if (cols<=0 || obstacleGrid[0][0]==1) { return 0; }
        obstacleGrid[0][0] = 1;
        for (int i = 1; i < rows; ++i) {
            obstacleGrid[i][0] = !obstacleGrid[i][0]&&obstacleGrid[i-1][0];
        }
        for (int j = 1; j < cols; ++j) {
            obstacleGrid[0][j] = !obstacleGrid[0][j]&&obstacleGrid[0][j-1];
        }
        for (int i = 1; i < rows; ++i) {
            for (int j = 1; j < cols; ++j) {
                if (obstacleGrid[i][j] == 1) {
                    obstacleGrid[i][j] = 0;
                    continue;
                }
                obstacleGrid[i][j] = obstacleGrid[i-1][j] + obstacleGrid[i][j-1];
            }
        }
        return obstacleGrid[rows-1][cols-1];
    }
};
