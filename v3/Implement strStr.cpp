class Solution {
public:
    char *strStr(char *haystack, char *needle) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int len = strlen(needle), len2 = strlen(haystack);
        if (len > len2) { return NULL; }
        if (len <= 0) { return haystack; }
        int *next = (int*)malloc(sizeof(int)*len);
        next[0] = -1;
        for (int i=1, j=-1; i < len; ++i) {
            while (j!=-1 && needle[i]!=needle[j+1]) {
                j = next[j];
            }
            if (needle[i] == needle[j+1]) {
                ++j;
            }
            next[i] = j;
        }
        for (int i=0, j=-1; i < len2; ++i) {
            while (j!=-1 && haystack[i]!=needle[j+1]) {
                j = next[j];
            }
            if (haystack[i] == needle[j+1]) {
                ++j;
            }
            if (j == len-1) {
                return (haystack+(i-len+1));
            }
        }
        return NULL;
    }
};
