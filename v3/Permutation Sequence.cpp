class Solution {
public:
    string getPermutation(int n, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int cnt = 1, num[10];
        for (int i = 1; i <= n; ++i) {
            cnt *= i, num[i-1] = i;
        }
        string ret;
        k = (--k) % cnt;
        while (n>0) {
            cnt = cnt/n;
            ret += (num[k/cnt]+'0');
            for (int i = k/cnt; i < n; ++i) {
                num[i] = num[i+1];
            }
            k = k%cnt, --n;
        }
        return ret;
    }
};
