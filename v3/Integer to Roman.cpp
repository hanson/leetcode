class Solution {
public:
    string intToRomanHelper(int val, char one, char five, char ten) {
        if (val <= 3) { return string(val, one); }
        if (val <= 4) { return string(1, one) + string(1, five); }
        if (val <= 8) { return string(1, five) + string(val-5, one); }
        return string(1, one) + string(1, ten);
    }
    string intToRoman(int num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        string ret;
        ret = intToRomanHelper(num % 10, 'I', 'V', 'X') + ret, num /= 10;
        ret = intToRomanHelper(num % 10, 'X', 'L', 'C') + ret, num /= 10;
        ret = intToRomanHelper(num % 10, 'C', 'D', 'M') + ret, num /= 10;
        ret = intToRomanHelper(num % 10, 'M', ' ', ' ') + ret, num /= 10;
        return ret;
    }
};
