class Solution {
public:
    string convert(string s, int nRows) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function    
        vector<string> strs(nRows+1);
        for (int i=0, index=0, fwd=1; i < s.length(); ++i, index+=fwd, index%=nRows) {
            strs[index] += s[i];
            if (index == 0) {
                fwd = 1;
            }
            if (index == nRows-1) {
                fwd = -1;
            }
        }
        for (int i = 0; i < nRows; ++i) {
            strs[nRows].append(strs[i]);
        }
        return strs[nRows];
    }
};
