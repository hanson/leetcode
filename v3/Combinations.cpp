class Solution {
private:
    map<pair<int, int>, vector<vector<int> > > dic;
public:
    vector<vector<int> > combine(int n, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > ret, tmp;
        if (n<k || k<0) {
            return ret;
        }
        if (n==0) {
            ret.push_back(vector<int>());
            return ret;
        }
        if (dic.find(make_pair(n, k)) != dic.end()) {
            return dic[make_pair(n, k)];
        }
        ret = combine(n-1, k), tmp = combine(n-1, k-1);
        for (int i = 0; i < tmp.size(); ++i) {
            tmp[i].push_back(n), ret.push_back(tmp[i]);
        }
        dic.insert(make_pair(make_pair(n, k), ret));
        return ret;
    }
};
