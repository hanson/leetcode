class Solution {
public:
    int searchInsert(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int left = 0, right = n-1, mid;
        while (left <= right) {
            mid = left + (right-left)/2;
            if (A[mid] == target) { return mid; }
            else if (A[mid] < target) { left = mid + 1; }
            else { right = mid - 1; }
        }
        return left;
    }
};
