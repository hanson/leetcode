/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
bool cmp(const Interval &i1, const Interval &i2) {
    return i1.start < i2.start;
}

class Solution {
public:
    vector<Interval> merge(vector<Interval> &intervals) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        sort(intervals.begin(), intervals.end(), cmp);
        vector<Interval> ret;
        Interval* cur = NULL;
        for (int i = 0; i < intervals.size(); ++i) {
            if (cur == NULL) {
                cur = &intervals[i];
                continue;
            }
            if (cur->end >= intervals[i].start) {
                cur->end = max(cur->end, intervals[i].end);
            }
            else {
                ret.push_back(*cur);
                cur = &intervals[i];
            }
        }
        if (cur != NULL) {
            ret.push_back(*cur);
        }
        return ret;
    }
};
