/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxDepthHelper(TreeNode *root) {
        if (root == NULL) { return INT_MIN; }
        if (root->left==NULL && root->right==NULL) { return 1; }
        int lh = maxDepthHelper(root->left);
        int rh = maxDepthHelper(root->right);
        //not leaf
        return max(lh, rh)+1;
    }
    int maxDepth(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL) { return 0; }
        return maxDepthHelper(root);
    }
};
