class Solution {
public:
    bool isMatch(const char *s, const char *p) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (*p == '\0') { return *s == '\0'; }
        if (*p == '?') {
            if (*s == '\0') {
                return false;
            }
            return isMatch(s+1, p+1);
        }
        else if (*p == '*') {
            while (*(p+1) != '\0' && *(p+1) == '*') {
                ++p;
            }
            while (*s != '\0') {
                if (isMatch(s, p+1)) {
                    return true;
                }
                ++s;
            }
            return isMatch(s, p+1);
        }
        else if (*p == *s) {
            return isMatch(s+1, p+1);
        }
        return false;
    }
};
