/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool flag;
public:
    bool isValidBSTHelper(TreeNode *curNode, int minVal, int maxVal) {
        if (curNode == NULL) { return true; }
        if (!flag || curNode->val<=minVal || curNode->val>=maxVal) { return (flag=false); }
        return isValidBSTHelper(curNode->left, minVal, curNode->val) &&
        isValidBSTHelper(curNode->right, curNode->val, maxVal);
    }
    bool isValidBST(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        flag = true;
        return isValidBSTHelper(root, INT_MIN, INT_MAX);
    }
};
