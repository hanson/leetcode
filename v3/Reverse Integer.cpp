class Solution {
public:
    int reverse(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        while (x && !(x%10)) {
            x /= 10;
        }
        int k = x % 10;
        x /= 10;
        while (x) {
            k = k*10 + x%10, x /= 10;
        }
        return k;
    }
};
