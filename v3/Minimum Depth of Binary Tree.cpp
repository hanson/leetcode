/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int minDepthHelper(TreeNode *root) {
        if (root == NULL) { return INT_MAX; }
        if (root->left==NULL && root->right==NULL) { return 1; }
        int lh = minDepthHelper(root->left);
        int rh = minDepthHelper(root->right);
        //not leaf
        return min(lh, rh)+1;
    }
    int minDepth(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL) { return 0; }
        return minDepthHelper(root);
    }
};
