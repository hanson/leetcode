class Solution {
public:
    vector<int> twoSum(vector<int> &numbers, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> index(2, -1);
        vector<pair<int, int> > dic;
        int len = numbers.size(), left = 0, right = len-1;
        for (int i = 0; i < len; ++i) {
            dic.push_back(make_pair(numbers[i], i+1));
        }
        sort(dic.begin(), dic.end());
        while (left < right) {
            int tmp = dic[left].first + dic[right].first;
            if (tmp == target) {
                index[0] = min(dic[left].second, dic[right].second);
                index[1] = max(dic[left].second, dic[right].second);
                return index;
            }
            else if (tmp > target) {
                --right;
            }
            else {
                ++left;
            }
        }
        return index;
    }
};
