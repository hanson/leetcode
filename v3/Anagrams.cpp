class Solution {
public:
    vector<string> anagrams(vector<string> &strs) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        map<string, vector<string> > dic;
        map<string, vector<string> >::iterator it;
        for (int i = 0; i < strs.size(); ++i) {
            string key = strs[i];
            sort(key.begin(), key.end());
            if ((it=dic.find(key)) == dic.end()) {
                vector<string> tmp;
                tmp.push_back( strs[i] );
                dic.insert( pair<string, vector<string> >(key, tmp) );
                continue;
            }
            it->second.push_back( strs[i] );
        }
        for (it=dic.begin(), strs.clear(); it != dic.end(); ++it) {
            for (int i = 0; it->second.size()>1 && i<it->second.size(); ++i) {
                strs.push_back( it->second[i] );
            }
        }
        return strs;
    }
};
