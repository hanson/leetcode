/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int opt;
public:
    int maxPathSumHelper(TreeNode *root) {
        if (root == NULL) {
            return 0;
        }
        int ret = root->val;
        int lsum = maxPathSumHelper(root->left);
        if (lsum > 0) {
            ret += lsum;
        }
        int rsum = maxPathSumHelper(root->right);
        if (rsum > 0) {
            ret += rsum;
        }
        if (ret>opt) {
            opt = ret;
        }
        return root->val + (max(lsum, rsum)>0?max(lsum, rsum):0);
    }
    int maxPathSum(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        opt = INT_MIN;
        maxPathSumHelper(root);
        return opt;
    }
};
