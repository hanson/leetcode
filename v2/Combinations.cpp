class Solution {
private:
    vector<vector<int> >coms;
public:
    void dfs(int n, int k, int st, vector<int> &rc) {
        if (rc.size() == k) {
            vector<int> tmp(rc);
            coms.push_back(tmp);
            return;
        }
        
        for (int i = st; i <= n; ++i) {
            rc.push_back(i);
            dfs(n, k, i+1, rc);
            rc.pop_back();
        }
    }
    
    vector<vector<int> > combine(int n, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        coms.clear();
        vector<int> rc;
        dfs(n, k, 1, rc);
        return coms;
    }
};
