class Solution {
public:
    vector<int> grayCode(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> grays;
        for (int i = 0; i < (1<<n); ++i) {
            grays.push_back(i^(i>>1));
        }
        return grays;
    }
};
