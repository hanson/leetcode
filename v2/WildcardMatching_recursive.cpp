class Solution {
public:
    bool isMatch(const char *s, const char *p) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (s==NULL && p==NULL) { return true;}
        if (s==NULL || p==NULL) { return false;}
        if (*p == '?') {
            if (*s == '\0') { 
                return false;
            }
            return isMatch(s+1, p+1);
        }
        else if (*p == '*') {
            while(*s != '\0')
            {
                if (isMatch(s, p + 1)) { 
                    break;
                }
                s++;
            }
            if (*s != '\0') {
                return true;
            }
            return isMatch(s, p+1);
        }
        else if (*p == *s) {
            if (*s == '\0') {
                return true;
            }
            return isMatch(s+1, p+1);
        }
        return false;
    }
};
