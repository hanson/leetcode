class Solution {
public:
    bool isScramble(string s1, string s2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (s1.length() != s2.length()) {
            return false;
        }
        int len = s1.length();
                if (len == 0) {
            return true;
        }
        if (len == 1) {
            return s1[0] == s2[0];
        }
        bool ans = false;
        for (int i = 1; i < len; ++i) {
            ans = ans || (isScramble(s1.substr(0, i), s2.substr(len-i)) &&
        isScramble(s1.substr(i), s2.substr(0, len-i))) ||
        ((isScramble(s1.substr(0, i), s2.substr(0, i)) &&
        isScramble(s1.substr(i), s2.substr(i))));
        }
        return ans;
    }
};
