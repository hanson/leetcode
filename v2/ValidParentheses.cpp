class Solution {
public:
    bool check(stack<char> &pstack, char ch) {
        if (pstack.empty()) {
            return false;
        }
        return pstack.top() == ch;
    }
    bool isValid(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        stack<char> pstack;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == ')') {
                if (check(pstack, '(') == false) {
                    return false;
                }
                else {
                    pstack.pop();
                }
                continue;
            }
            if (s[i] == '}') {
                if (check(pstack, '{') == false) {
                    return false;
                }
                else {
                    pstack.pop();
                }
                continue;
            }
            if (s[i] == ']') {
                if (check(pstack, '[') == false) {
                    return false;
                }
                else {
                    pstack.pop();
                }
                continue;
            }
            pstack.push(s[i]);
        }
        return pstack.empty();
    }
};
