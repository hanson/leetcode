class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int row = obstacleGrid.size();
        if (row == 0) {
            return 0;
        }
        int col = obstacleGrid[0].size();
        if (col == 0) {
            return 0;
        }
        obstacleGrid[0][0] = obstacleGrid[0][0]==0?1:0;
        for (int r = 1; r < row; ++r) {
            if (obstacleGrid[r][0] == 0) {
                obstacleGrid[r][0] = obstacleGrid[r-1][0];
            }
            else {
                obstacleGrid[r][0] = 0;
            }
        }
        for (int c = 1; c < col; ++c) {
            if (obstacleGrid[0][c] == 0) {
                obstacleGrid[0][c] = obstacleGrid[0][c-1];
            }
            else {
                obstacleGrid[0][c] = 0;
            }
        }
        for (int r = 1; r < row; ++r) {
            for (int c = 1; c < col; ++c) {
                if (obstacleGrid[r][c] == 1) {
                    obstacleGrid[r][c] = 0;
                    continue;
                }
                obstacleGrid[r][c] = obstacleGrid[r-1][c] + obstacleGrid[r][c-1];
            }
        }
        return obstacleGrid[row-1][col-1];
    }
};
