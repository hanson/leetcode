/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *deleteDuplicatesHelp(ListNode *head) {
        while (head!=NULL && head->next!=NULL) {
            if (head->val != head->next->val) {
                break;
            }
            int tmp = head->val;
            while (head!=NULL) {
                if (head->val != tmp) {
                    break;       
                }
                head = head->next;
            }
        }
        return head;
    }
    ListNode *deleteDuplicates(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        head = deleteDuplicatesHelp(head);
        if (head == NULL) {
            return NULL;
        }
        ListNode *pre = head;
        ListNode *thead = head->next;
        while (thead!=NULL && thead->next!=NULL) {
            if (thead->val != thead->next->val) {
                pre = thead;
                thead = thead->next;
                continue;
            }
            int tmp = thead->val;
            while (thead != NULL) {
                if (thead->val != tmp) {
                    break;
                }
                thead = thead->next;
            }
            pre->next = thead;
        }
        return head;
    }
};
