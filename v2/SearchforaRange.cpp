class Solution {
private:
    vector<int> range;
public:
    void genRange(int A[], int low, int high, int t) {
        int mid;
        while (low <= high) {
            mid = low + (high-low)/2;
            if (A[mid] == t) {
                if (range[0]==-1 || mid<range[0]) {
                    range[0] = mid;
                }
                if (range[0]==-1 || mid>range[1]) {
                    range[1] = mid;
                }
                genRange(A, low, mid-1, t);
                genRange(A, mid+1, high, t);
                return;
            }
            else if (A[mid] > t) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
    }
    vector<int> searchRange(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        range.clear();
        range.push_back(-1);
        range.push_back(-1);
        genRange(A, 0, n-1, target);
        return range;
    }
};
