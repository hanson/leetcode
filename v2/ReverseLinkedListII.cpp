/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseBetween(ListNode *head, int m, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode *thead = head;
        ListNode *phead = NULL;
        int cur = 1;
        while (cur < m) {
            ++cur;
            phead = thead;
            thead = thead->next;
        }
        ListNode *ttail = thead;
        ListNode *tnext = thead->next;
        ListNode *tmp;
        while (cur < n) {
            ++cur;
            tmp = tnext->next;
            tnext->next = thead;
            thead = tnext;
            tnext = tmp;
        }
        ttail->next = tnext;
        if (phead == NULL) {
            head = thead;
        }
        else {
            phead->next = thead;
        }
        return head;
    }
};
