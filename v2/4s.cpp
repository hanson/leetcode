#include <iostream>
#include <vector>

using namespace std;
class Solution {
public:
    int bsearch(int low, int high, vector<int> &num, int t) {
        int mid;
        while (low <= high) {
            mid = low + (high-low)/2;
            if (num[mid] == t) {
                return mid;
            }
            else if (num[mid] > t) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return -1;
    }
    vector<vector<int> > fourSum(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> >tri;
        int n = num.size();
        sort(num.begin(), num.end());
        for (int i = 0; i < n; ++i) {
            while (i!=0 && i<n && num[i]==num[i-1]) {
                ++i;
            }
            if (i>=n || num[i] > target) {
                break;
            }
            for (int j = i + 1; j < n; ++j) {
                while (j!=i+1 && j<n && num[j]==num[j-1]) {
                    ++j;
                }
                if (j>=n || num[i]+num[j]>target) {
                    break;
                }
                for (int k = j + 1; k < n; ++k) {
                    while (k!=j+1 && k<n && num[k]==num[k-1]){
                        ++k;
                    }
                    if (k>=n || num[i]+num[j]+num[k]>target) {
                        break;
                    }
                    cout<<num[i]<<" "<<num[j]<<" "<<num[k]<<endl;
                    int t = bsearch(k+1, n-1, num, target-(num[i]+num[j]+num[k]));
                    if (t == -1) {
                        continue;
                    }
                    vector<int> tmp;
                    tmp.push_back(num[i]);
                    tmp.push_back(num[j]);
                    tmp.push_back(num[k]);
                    tmp.push_back(num[t]);
                    tri.push_back(tmp);
                }
            }
        }
        return tri;
    }
};

int main()
{
    Solution sl;
    vector<int> v;
    v.push_back(-1);
    v.push_back(0);
    v.push_back(-5);
    v.push_back(-2);
    v.push_back(-2);
    v.push_back(-4);
    v.push_back(0);
    v.push_back(1);
    v.push_back(-2);
    sl.fourSum(v, -9);
}
