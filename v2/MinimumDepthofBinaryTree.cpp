/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int minDepth(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL) {
            return 0;
        }
        if (root->left==NULL && root->right==NULL) {
            return 1;
        }
        int dep = -1;
        if (root->left != NULL) {
            dep = minDepth(root->left);
        }
        if (root->right != NULL) {
            int tmp = minDepth(root->right);
            dep = dep==-1?tmp:min(dep, tmp);
        }
        return dep+1;
    }
};
