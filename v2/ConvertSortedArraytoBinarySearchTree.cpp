/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *buildTree(int low, int high, vector<int> &num) {
        if (low > high) {
            return NULL;
        }
        TreeNode *curNode;
        int mid = low + (high-low)/2;
        curNode = new TreeNode(num[mid]);
        curNode->left = buildTree(low, mid-1, num);
        curNode->right = buildTree(mid+1, high, num);
        return curNode;
    }
    TreeNode *sortedArrayToBST(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return buildTree(0, num.size()-1, num);
    }
};
