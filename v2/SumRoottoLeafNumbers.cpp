/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int sumHelper(TreeNode *cur, int tsum) {
        if (cur == NULL) {
            return 0;
        }
        tsum *= 10;
        tsum += cur->val;
        if (cur->left == NULL && cur->right == NULL) {
            return tsum;
        }
        return sumHelper(cur->left, tsum) + sumHelper(cur->right, tsum);
    }
    int sumNumbers(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function    
        return sumHelper(root, 0);
    }
};
