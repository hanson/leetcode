class Solution {
public:
    char *strStr(char *haystack, char *needle) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int j = -1, len1 = strlen(haystack), len2 = strlen(needle);
        if (len2 == 0) {
            return haystack;
        }
        int *p = (int*)malloc(sizeof(int*)*len2);
        p[0] = -1;
        for (int i = 1; i < len2; ++i) {
            while (j!=-1 && needle[j+1]!=needle[i]) {
                j = p[j];
            }
            if (needle[j+1] == needle[i]) {
                ++j;
            }
            p[i] = j;
        }
        j = -1;
        for (int i = 0; i < len1; ++i) {
            while (j!=-1 && needle[j+1]!=haystack[i]) {
                j = p[j];
            }
            if (needle[j+1]==haystack[i]) {
                ++j;
            }
            if (j == len2-1) {
                return haystack+(i-len2+1);
            }
        }
        return NULL;
    }
};
