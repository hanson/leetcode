#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
    public:
        int binsearch(const vector<int> &numbers, int t)
        {
            int low = 0;
            int high = numbers.size() - 1;
            while (low <= high)
            {
                int mid = low + (high-low)/2;
                if (numbers[mid] == t)
                {
                    return mid;
                }
                else if (numbers[mid] < t)
                {
                    low = mid + 1;
                }
                else
                {
                    high = mid - 1;
                }
            }

            return  -1;
        }

        vector<int> twoSum(vector<int> &numbers, int target) {
            vector<int> rec;
            for (int i = 0; i < numbers.size(); ++i)
            {
                rec.push_back( numbers[i] );
            }

            sort(numbers.begin(), numbers.end());
            vector<int> rets;
            for (int i = 0; i < numbers.size(); ++i)
            {
                int k = binsearch(numbers, target - numbers[i]);
                if (k == -1)
                {
                    continue;
                }
                
                for (int j = 0; j < rec.size(); ++j)
                {
                    if (rec[j] == numbers[i] || rec[j] == numbers[k])
                    {
                        rets.push_back(j);
                    }
                }
            }

            return rets;
        }
};
int main()
{
    return 0;
}

