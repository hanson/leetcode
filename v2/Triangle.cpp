class Solution {
public:
    int minimumTotal(vector<vector<int> > &triangle) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int row = triangle.size() -1;
        for (int k = row - 1; k >= 0; --k) {
            for (int i = 0; i < triangle[k].size(); ++i) {
                triangle[k][i] += min(triangle[k+1][i], triangle[k+1][i+1]);
            }
        }
        
        return triangle[0][0];
    }
};
