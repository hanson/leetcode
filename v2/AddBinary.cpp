class Solution {
public:
    string addBinary(string a, string b) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int carry = 0, lena = a.length(), lenb = b.length();
        string *p1;
        string *p2;
        if (lena >= lenb) {
            p1 = &a;
            p2 = &b;
        }
        else {
            p1 = &b;
            p2 = &a;
        }
        
        int i1 = (*p1).length()-1, i2 = (*p2).length()-1;
        while (i2 >= 0) {
            int tmp = (*p1)[i1] - '0' + (*p2)[i2] - '0' + carry;
            carry = tmp / 2;
            tmp = tmp % 2;
            (*p1)[i1] = tmp + '0';
            --i2, --i1;
        }
        while (i1 >= 0) {
            int tmp = (*p1)[i1] - '0' + carry;
            carry = tmp / 2;
            tmp = tmp % 2;
            (*p1)[i1] = tmp + '0';
            --i1;
        }
        if (!carry) {
            return (*p1);
        }
        string ret;
        ret += '1';
        for (int i = 0; i < (*p1).length(); ++i) {
            ret += (*p1)[i];
        }
        
        return ret;
    }
};
