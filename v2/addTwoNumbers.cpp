/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int carry = 0, tmp;
        ListNode *retHead = l1;
        ListNode *curNode = retHead;
        ListNode *preNode = curNode;
        while (l2 != NULL || carry > 0) {
            tmp = carry;
            if (l2 != NULL) {
                tmp += l2->val;
                l2 = l2->next;
            }
            if (curNode != NULL) {
                tmp += curNode->val;
                curNode->val = tmp % 10;
            }
            else {
                curNode = new ListNode(tmp % 10);
                if (retHead == NULL) {
                    retHead = curNode;
                }
                preNode->next = curNode;
            }
            carry = tmp / 10;
            preNode = curNode;
            curNode = curNode->next;
        }
        return retHead;
    }
};
