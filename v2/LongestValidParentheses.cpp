class Solution {
public:
    int longestValidParentheses(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        stack<int> ps;
        int ans = 0, tmp = 0, stPoint = 0;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '(') {
                ps.push(i);
            }
            else {
                if (ps.empty()) {
                    stPoint = i+1;
                }
                else {
                    tmp = ps.top();
                    ps.pop();
                    ans = max(ans, i-((ps.empty()||ps.top()<stPoint)?stPoint:ps.top()+1) + 1);
                }
            }
        }
        return ans;
    }
};
