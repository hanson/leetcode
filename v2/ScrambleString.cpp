class Solution {
public:
    bool isScramble(string s1, string s2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (s1.length() != s2.length()) {
            return false;
        }
        int len = s1.length();
        int ***dp;
        dp = (int ***)malloc(sizeof(int**)*len);
        for (int i = 0; i < len; ++i) {
            dp[i] = (int**)malloc(sizeof(int*)*len);
        }
        for (int i = 0; i < len; ++i) {
            for (int j = 0; j < len; ++j) {
                dp[i][j] = (int*)malloc(sizeof(int)*(len+1));
            }
        }
        for (int i = 0; i < len; ++i) {
            for (int j = 0; j < len; ++j) {
                if (s1[i] == s2[j]) {
                    dp[i][j][1] = true;
                }
            }
        }
        for (int tlen = 2; tlen <= len; ++tlen) {
            for (int i = 0; i < len; ++i) {
                for (int j = 0; j < len; ++j) {
                    dp[i][j][tlen] = false;
                    for (int p = 1; p<tlen; ++p) {
                        if (i+p>=len || j+p>=len || j+tlen-p>=len) {
                            continue;
                        }
                        if ((dp[i][j][p]&&dp[i+p][j+p][tlen-p])
                        || (dp[i][j+tlen-p][p]&&dp[i+p][j][tlen-p])) {
                            dp[i][j][tlen] = true;
                            break;
                        }
                    }
                }
            }
        }
        return dp[0][0][len];
    }
};