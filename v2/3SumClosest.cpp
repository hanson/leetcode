class Solution {
public:
    int findc(vector<int> &num, int target, int exclude)
    {
        int ret, start = 0, end = num.size() - 1;
        bool first = true;
        while (1) {
            if (start == exclude) {
                ++start;
            }
            if (end == exclude) {
                --end;
            }
            if (start >= end) {
                break;
            }
            int tmp = num[start] + num[end];
            if (first==true || abs(target-tmp)<abs(target-ret)) {
                ret = tmp;
                first = false;
            }
            if (target > tmp) {
                ++start;
            }
            else if (target < tmp) {
                --end;
            }
            else {
                break;
            }
        }
        return ret;
    }
    int threeSumClosest(vector<int> &num, int target) {
        int ret;
        bool first = true;
        sort(num.begin(), num.end());
        for (int i = 0; i < num.size(); ++i) {
            int tmp = findc(num, target - num[i], i) + num[i];
            if (first==true || abs(tmp-target) < abs(ret-target)) {
                ret = tmp;
                first = false;
            }
        }
        return ret;
    }
};
