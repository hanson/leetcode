class Solution {
public:
    //bad implemt, because it requires O(N) memory, the runtime is O(n^2)
    //I can decrease the memory to O(1)
    void rotate(vector<vector<int> > &matrix) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int len = matrix.size();
        if (len == 0) { return;}
        vector<int> tmp;
        for (int i = 0; i < len/2; ++i) {
            //save right
            tmp.clear();
            for (int r = i; r < len-i; ++r) {
                tmp.push_back(matrix[r][len-1-i]);
            }
            //top -> right
            for (int c=len-1-i; c >= i; --c) {
                matrix[c][len-1-i] = matrix[i][c];
            }
            //left -> top
            for (int r = i; r<=len-1-i; ++r) {
                matrix[i][len-1-r] = matrix[r][i];
            }
            //bottom -> left
            for (int c=i; c<=len-1-i; ++c) {
                matrix[c][i] = matrix[len-1-i][c];
            }
            //right -> bottom
            for (int c=len-1-i, t = 0; c>=i; --c) {
                matrix[len-1-i][c] = tmp[t++];
            }
        }
    }
};
