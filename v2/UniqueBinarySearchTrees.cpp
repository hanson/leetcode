class Solution {
public:
    int numTrees(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int *cnt = (int*)malloc((n+1)*sizeof(int));
        cnt[0] = 1;
        cnt[1] = 1;
        for (int i = 2; i <= n; ++i) {
            cnt[i] = 0;
            for (int k = 0; k < i; ++k) {
                cnt[i] += (cnt[k]*cnt[i-1-k]);
            }
        }
        return cnt[n];
    }
};
