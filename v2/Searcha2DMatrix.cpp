class Solution {
public:
    bool searchMatrix(vector<vector<int> > &matrix, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int rn = matrix.size();
        if (rn == 0) {
            return false;
        }
        int cn = matrix[0].size();
        if (cn == 0) {
            return false;
        }
        int sr = rn - 1;
        int sc = 0;
        while (sr>=0 && sr<rn && sc>=0 && sc<cn) {
            if (matrix[sr][sc] == target) {
                return true;
            }
            else if (matrix[sr][sc] < target) {
                ++sc;
            }
            else {
                --sr;
            }
        }
        return false;
    }
};