class Solution {
public:
    int minDistance(string word1, string word2) {
        int **dp;
        int len1 = word1.length();
        int len2 = word2.length();
        dp = (int **)malloc(sizeof(int*)*(len1+1));
        for (int i = 0; i <= len1; ++i) {
            dp[i] = (int*)malloc(sizeof(int)*(len2+1));
        }
        for (int i = 0; i <= len1; ++i) {
            dp[i][0] = i;
        }
        for (int j = 0; j <= len2; ++j) {
            dp[0][j] = j;
        }
        for (int i = 0; i < len1; ++i) {
            for (int j = 0; j < len2; ++j) {
                if (word1[i] == word2[j]) {
                    dp[i+1][j+1] = dp[i][j];
                    continue;
                }
                dp[i+1][j+1] = min(min(dp[i][j]+1, dp[i+1][j]+1), dp[i][j+1]+1);
            }
        }
        return dp[len1][len2];
    }
};
