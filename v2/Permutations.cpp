class Solution {
private:
    vector<vector<int> > ps;
public:
    void genps(vector<int> &num, int st, vector<int> &rps) {
        if (num.size() == st) {
            vector<int> tmp(rps);
            ps.push_back(tmp);
            return;
        }
        for (int i = st; i < num.size(); ++i) {
            swap(num[i], num[st]);
            rps.push_back(num[st]);
            genps(num, st+1, rps);
            swap(num[i], num[st]);
            rps.pop_back();
        }
    }
    vector<vector<int> > permute(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ps.clear();
        vector<int> rps;
        genps(num, 0, rps);
        return ps;
    }
};
