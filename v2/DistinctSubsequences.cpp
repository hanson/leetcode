class Solution {
public:
    int numDistinct(string S, string T) {
        //dp[i][j] = dp[i-1][j] if (S[i] != T[j])
        //dp[i][j] = dp[i-1][j-1] + dp[i-1][j] if (S[i] == T[j])
        //init:
        //dp[i][0] = 0;
        //dp[0][j] = 0;
        int len1 = S.length(), len2 = T.length();
        int **dp;
        dp = (int**)malloc(sizeof(int)*(len1+1));
        for (int i = 0; i <= len1; ++i) {
            dp[i] = (int*)malloc(sizeof(int)*(len2+1));
        }
        for (int j = 0; j <= len2; ++j) {
            dp[0][j] = 0;
        }
        for (int i = 0; i <= len1; ++i) {
            dp[i][0] = 1;
        }
        for (int i = 1; i <= len1; ++i) {
            for (int j = 1; j <= len2; ++j) {
                if (S[i-1] == T[j-1]) {
                    dp[i][j] = dp[i-1][j-1]+dp[i-1][j];
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[len1][len2];
    }
};