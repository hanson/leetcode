class Solution {
public:
    int bsearch(int A[], int low, int high, int t) {
        int mid;
        while (low <= high) {
            mid = low + (high-low)/2;
            if (A[mid] == t) {
                return mid;
            }
            else if (A[mid] < t) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        return -1;
    }
    int search(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int low = 0, high = n-1, mid;
        while (low < high) {
            mid = low + (high-low)/2;
            if (A[mid]>A[high]) {
                low = mid + 1;
            }
            else {
                high = mid;
            }
        }
        int ret, pivot = low;
        return (ret = bsearch(A, 0, pivot-1, target))==-1?bsearch(A, pivot, n-1, target):ret;
    }
};
