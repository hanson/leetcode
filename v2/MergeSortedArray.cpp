class Solution {
public:
    void merge(int A[], int m, int B[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int tm = m - 1;
        for (int i = 0; i < n; ++i) {
            int tmp = B[i];
            while (tm >= 0 && A[tm] > tmp) {
                A[tm + 1] = A[tm];
                --tm;
            }
            A[tm+1] = tmp;
            ++m;
            tm = m - 1; 
        }
    }
};
