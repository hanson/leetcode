class Solution {
public:
    vector<vector<int> > subsets(vector<int> &S) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > sets;
        sort(S.begin(), S.end());
        int len = S.size();
        for (int i = 0; i < (1<<len); ++i) {
            vector<int> tmp;
            for (int j = 0; j < len; ++j) {
                if ((i&(1<<j)) == 0) {
                    continue;
                }
                tmp.push_back(S[j]);
            }
            sets.push_back( tmp );
        }
        return sets;
    }
};
