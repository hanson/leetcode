class Solution {
public:
    bool isValid(int digit) {
        return (digit>=1 && digit<=26);
    }
    int numDecodings(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int len = s.length();
        int *dp = (int*)malloc(sizeof(int)*len+1);
        dp[len] = 0;
        for (int i = len-1; i >= 0; --i) {
            dp[i] = 0;
            int tmp = s[i] - '0';
            if (i+1<=len-1 && s[i]!='0' && isValid(tmp*10+s[i+1]-'0')) {
                if (i == len-2) {
                    dp[i] = 1;
                }
                dp[i] += dp[i+2];
            }
            if (isValid( tmp )) {
                if (i == len - 1) {
                    dp[i] = 1;
                }
                dp[i] += dp[i+1];
            }
        }
        return dp[0];
    }
};
