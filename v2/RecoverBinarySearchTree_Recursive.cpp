/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *f1;
    TreeNode *f2; 
    TreeNode *p;
    void inOrder(TreeNode *root) {
        if (root == NULL) {
            return;
        }
        inOrder(root->left);
        if ((p!=NULL) && (p->val > root->val)) {
            if (f1==NULL) {
                f1 = p;
            }
            f2 = root;
        }
        p = root;
        inOrder(root->right);
    }
    void recoverTree(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        f1 = f2 = p = NULL;
        inOrder(root);
        swap(f1->val, f2->val);            
    }
};
