/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isSymmetricHelper(TreeNode *cur1, TreeNode *cur2) {
        if (cur1==NULL && cur2==NULL) {
            return true;
        }
        if (cur1==NULL ^ cur2==NULL) {
            return false;
        }
        if (cur1->val != cur2->val) {
            return false;
        }
        return isSymmetricHelper(cur1->left, cur2->right) && isSymmetricHelper(cur1->right, cur2->left);
    }
    bool isSymmetric(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return isSymmetricHelper(root, root);
    }
};
