/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *swapPairs(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode *thead = head;
        while (thead!=NULL && thead->next!=NULL) {
            swap(thead->val, thead->next->val);
            thead = thead->next->next;
        }
        return head;
    }
};
