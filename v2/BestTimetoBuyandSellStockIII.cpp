class Solution {
public:
    int maxProfit(vector<int> &prices) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (prices.size() <= 0) {
            return 0;
        }
        int *p1 = (int*)malloc(sizeof(int)*prices.size());
        int *p2 = (int*)malloc(sizeof(int)*prices.size());
        int sentinel;
        sentinel = prices[0], p1[0] = 0;
        for (int i = 1; i < prices.size(); ++i) {
            if (prices[i] < sentinel) {
                sentinel = prices[i];
            }
            p1[i] = max(p1[i-1], prices[i] - sentinel);
        }
        sentinel = prices[prices.size()-1], p2[prices.size()-1] = 0;
        for (int i = prices.size()-1-1; i >= 0; --i) {
            if (prices[i] > sentinel) {
                sentinel = prices[i];
            }
            p2[i] = max(p2[i+1], sentinel - prices[i]);
        }
        int ans = 0;
        for (int i = 0; i < prices.size(); ++i) {
            ans = max(ans, p1[i]+p2[i]);
        }
        return ans;
    }
};
