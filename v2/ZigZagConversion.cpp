class Solution
{
    public:
        string convert(string s, int nRows) {
            vector<string> zigzags;
            for (int i = 0; i < nRows; ++i) {
                zigzags.push_back("");
            }
            int len = s.length(), index = 0;
            while(index < len) {
                for (int i = 0; i<nRows&&index<len; ++i) {
                    zigzags[i] += s[index++];
                }
                for (int k = nRows-2; k>0 && index<len; --k) {
                    zigzags[k] += s[index++];
                }
            }
            for (int i = 1; i < nRows; ++i) {
                zigzags[0].append( zigzags[i] );
            }
            return zigzags[0];
        }
};
