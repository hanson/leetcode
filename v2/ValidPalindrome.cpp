class Solution {
private:
    int limit;
public:
    bool isvalid(int p) {
        return (p>=0 && p<=limit);
    }
    bool isalph(char ch) {
        if ((ch>='a'&&ch<='z') || (ch>='A'&&ch<='Z') || (ch>='0'&&ch<='9')) {
            return true;
        }
        return false;
    }
    char tolowerCase(char ch) {
        if ((ch>='a'&&ch<='z') || (ch>='0'&&ch<='9')) {
            return ch;
        }
        return ch-'A'+'a';
    }
    bool isPalindrome(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int start = 0, end = s.length() - 1;
        int limit = end;
        while (true) {
            while (start<=end && !isalph(s[start])) {
                ++start;
            }
            while (start<=end && !isalph(s[end])) {
                --end;
            }
            if (start > end) {
                break;
            }
            if (tolowerCase(s[start]) != tolowerCase(s[end])) {
                return false;
            }
            ++start, --end;
        }
        return true;
    }
};
