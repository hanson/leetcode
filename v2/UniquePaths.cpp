class Solution {
public:
    int uniquePaths(int m, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int **dp;
        dp = (int**)malloc(sizeof(int*)*(m+2));
        for (int i = 0; i <= m; ++i) {
            dp[i] = (int*)malloc(sizeof(int)*(n+2));
        }
        dp[0][0] = 1;
        for (int i = 0; i <= m; ++i) {
            dp[i][1] = 1;
        }
        for (int j = 0; j <= n; ++j) {
            dp[1][j] = 1;
        }
        for (int i = 2; i <= m; ++i) {
            for (int j = 2; j <= n; ++j) {
                dp[i][j] = dp[i-1][j]+dp[i][j-1];
            }
        }
        return dp[m][n];
    }
};
