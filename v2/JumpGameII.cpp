class Solution {
public:
    int jump(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int *dp = (int*)malloc(sizeof(int) * n), jump = 0;
        for (int i = 0; i < n; ++i) {
            if (A[i] > jump) {
                jump = A[i];
            }
            A[i]=jump, --jump, dp[i]=-1;
        }
        dp[0] = 0;
        for (int i = 1; i < n; ++i) {
            for (int j = 0; j < i; ++j) {
                if (j+A[j]>=i && (dp[i]==-1 || dp[i]>(dp[j]+1))) {
                    dp[i] = dp[j] + 1;
                    break;
                }
            }
        }
        return dp[n-1];
    }
};
