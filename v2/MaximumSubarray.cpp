class Solution {
public:
    int maxSubArray(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int ans = A[0], tmp = 0;
        for (int i = 0; i < n; ++i) {
            if (tmp < 0) {
                tmp = 0;
            }
            tmp += A[i];
            if (tmp > ans) {
                ans = tmp;
            }
        }
        return ans;
    }
};
