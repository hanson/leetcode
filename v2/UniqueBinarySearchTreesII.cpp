/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<TreeNode *> generateTreesHelper(int left, int right) {
        vector<TreeNode *> ret;
        if (right < left) {
            ret.push_back(NULL);
            return ret;
        }
        TreeNode *curNode;
        for (int cur = left; cur<=right; ++cur) {
            vector<TreeNode *> lhc = generateTreesHelper(left, cur-1);
            vector<TreeNode *> rhc = generateTreesHelper(cur+1, right);
            for (int lhs = 0; lhs < lhc.size(); ++lhs) {
                for (int rhs = 0; rhs < rhc.size(); ++rhs) {
                    curNode = new TreeNode(cur);
                    curNode->left = lhc[lhs];
                    curNode->right = rhc[rhs];
                    ret.push_back(curNode);
                }
            }
        }
        return ret;
    }
    vector<TreeNode *> generateTrees(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return generateTreesHelper(1, n);
    }
};
