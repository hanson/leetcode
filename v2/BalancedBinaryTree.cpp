/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool flag;
public:
    int isBalancedHelper(TreeNode *cur) {
        if (cur == NULL) {
            return 0;
        }
        if (flag == false) {
            return 0;
        }
        int lheight = isBalancedHelper(cur->left);
        int rheight = isBalancedHelper(cur->right);
        if (abs(lheight-rheight) > 1) {
            flag = false;
        }
        return max(lheight, rheight)+1;
    }
    bool isBalanced(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        flag = true;
        if (root == NULL) {
            return flag;
        }
        int lheight = isBalancedHelper(root->left);
        int rheight = isBalancedHelper(root->right);
        if (abs(lheight-rheight) > 1) {
            flag = false;
        }
        return flag;
    }
};
