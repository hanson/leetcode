class Solution {
private:
    int ans;
public:
    int getLaA(stack<int> &hs, int h) {
        int tmp, width = 1;
        while (!hs.empty() && h<hs.top()) {
            tmp = width*hs.top();
            hs.pop();
            ++width;
            ans = max(tmp, ans);
        }
        return width;
    }
    int largestRectangleArea(vector<int> &height) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        stack<int> hs;
        ans = 0;
        int width = 1;
        for (int i = 0; i < height.size(); ++i) {
            if (hs.empty() || height[i] >= hs.top()) {
                hs.push(height[i]);
                continue;
            }
            width = getLaA(hs, height[i]);
            while (width) {
                --width;
                hs.push(height[i]);
            }
        }
        getLaA(hs, -1);
        return ans;
    }
};
