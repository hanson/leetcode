class Solution {
public:
    int sqrt(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        //x(n+1)=x(n)－f(x(n))/f'(x(n))
        double ans = 1;
        while ( abs(ans*ans - x) > 0.1)
        {
            ans = (ans + x/ans)/2;
        }
        return (int)ans;
    }
};
