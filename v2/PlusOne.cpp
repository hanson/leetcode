class Solution {
public:
    vector<int> plusOne(vector<int> &digits) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int carry = 1;
        int num = digits.size();
        for (int i = num-1; i >= 0; --i) {
            digits[i] += carry;
            carry = digits[i] / 10;
            digits[i] = digits[i] % 10;
        }
        if (carry == 0) {
            return digits;
        }
        vector<int> ret;
        ret.push_back(1);
        for (int i = 0; i < num; ++i) {
            ret.push_back( digits[i] );
        }
        return ret;
    }
};
