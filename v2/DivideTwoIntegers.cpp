class Solution {
public:
    int divide(long long dividend, long long divisor) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int neg = 1 - 2*(int)(dividend<0 ^ divisor<0);
        dividend = abs(dividend);
        long long ans=0;
        while (dividend > 0) {
            long long tmp = 1;
            long long tr = abs(divisor);
            while (tr < dividend) {
                tr <<= 1;
                tmp <<= 1;
            }
            if (tr != dividend) {
                tmp >>= 1;
                tr >>= 1;
            }
            ans += tmp;
            dividend -= tr;
        }
        return neg*ans;
    }
};
