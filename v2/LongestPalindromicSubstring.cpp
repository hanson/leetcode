class Solution {
public:
    string longestPalindrome(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int len = s.length();
        if (len <= 1) {return s;}
        int dp[1005][1005];
        int maxlen = 1, lhs = 0, rhs = 0;
        for (int tl = 1; tl < len; ++tl) {
            for (int i = 0; i+tl< len; ++i) {
                dp[i][i+tl] = false;
                if (s[i]==s[i+tl] && (dp[i+1][i+tl-1] || tl<=2)) {
                    dp[i][i+tl] = true;
                    if (tl+1 > maxlen) {
                        maxlen = tl+1, lhs = i, rhs = i+tl;
                    }
                }
            }
        }
        return s.substr(lhs, (rhs-lhs+1));
    }
};
