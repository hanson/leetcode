/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> ret;
        if (root == NULL) {
            return ret;
        }
        stack<TreeNode*> ts;
        ts.push(root);
        TreeNode *thead, *tmp;
        while (!ts.empty()) {
            thead = ts.top();
            while (thead->left != NULL) {
                tmp = thead->left;
                thead->left = NULL;
                thead = tmp;
                ts.push(thead);
            }
            ret.push_back(thead->val);
            ts.pop();
            if (thead->right != NULL) {
                ts.push(thead->right);
                thead->right = NULL;
            }
        }
        return ret;
    }
};
