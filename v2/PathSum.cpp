/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool isExist;
public:
    void checkSum(TreeNode *curNode, int curVal, int sum) {
        if (isExist) {
            return;
        }
        if (curNode == NULL) {
            return;
        }
        int tmp = curVal+curNode->val;
        if (curNode->left==NULL && curNode->right==NULL) {
            isExist = (tmp == sum);
            return;
        }
        checkSum(curNode->left, tmp, sum);
        checkSum(curNode->right, tmp, sum);
    }
    bool hasPathSum(TreeNode *root, int sum) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        isExist = false;
        checkSum(root, 0, sum);
        return isExist;
    }
};
