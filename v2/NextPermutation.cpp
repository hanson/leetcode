class Solution {
public:
    void nextPermutation(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int len = num.size();
        bool flag = false;
        for (int i = len-1; i >= 0; --i) {
            int ch = -1;
            for (int j = i+1; j < len; ++j) {
                if (num[i]<num[j] && (ch==-1 || num[ch]>num[j])) {
                    ch = j;
                }
            }
            if (ch != -1) {
                flag = true;
                swap(num[i], num[ch]);
                sort(num.begin()+i+1, num.end());
                break;
            }
        }
        if (!flag) {
            sort(num.begin(), num.end());
        }
    }
};
