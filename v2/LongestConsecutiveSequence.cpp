class Solution {
public:
    int longestConsecutive(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        map<int, int> preRec;
        map<int, int> succRec;
        for (int i = 0; i < num.size(); ++i) {
            preRec.insert(pair<int, int>(num[i], 1));
            succRec.insert(pair<int, int>(num[i], 1));
        }
        int ret = 0;
        map<int, int>::iterator preIt;
        map<int, int>::iterator succIt;
        for (preIt = preRec.begin(), succIt = succRec.begin(); preIt != preRec.end(); ++preIt, ++succIt) {
            int succ = preIt->first + 1;
            int pre = preIt->first - 1;
            map<int, int>::iterator tmpit;
            if ((tmpit=preRec.find(pre)) != preRec.end()) {
                preIt->second = tmpit->second+1;
            }
            if ((tmpit=succRec.find(succ)) != succRec.end()) {
                succIt->second = tmpit->second+1;
            }
            ret = max(max(preIt->second, succIt->second), ret);
        }
        return ret;
    }
};
