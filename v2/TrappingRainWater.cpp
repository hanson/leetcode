class Solution {
public:
    int trap(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int pVal = INT_MIN;
        int pIndex = -1;
        for (int i = 0; i < n; ++i) {
            if (A[i] > pVal) {
                pVal = A[i];
                pIndex = i;
            }
        }
        int ans = 0;
        int tmp = 0;
        for (int i = 0; i <= pIndex; ++i) {
            if (A[i] > A[tmp]) {
                tmp = i;
            }
            else {
                ans += (A[tmp]-A[i]);
            }
        }
        tmp = n-1;
        for (int i = n-1; i >= pIndex; --i) {
            if (A[i] > A[tmp]) {
                tmp = i;
            }
            else {
                ans += (A[tmp] - A[i]);
            }
        }
        return ans;
    }
};
