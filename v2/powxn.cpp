class Solution {
public:
    double pow(double x, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (n < 0) {
            x = 1/x;
            n = -n;
        }
        if (x==1.0) {
            return x;
        }
        else if (x == 0.0) {
            return n==0?1:x;
        }
        else if (x == -1) {
            return n&1?x:-x;
        }
        double ans = 1;
        while (n) {
            if (n&1) {
                ans *= x;
            }
            x *= x;
            n = n>>1;
        }
        return ans;
    }
};