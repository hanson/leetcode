/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *rotateRight(ListNode *head, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (head == NULL) {
            return head;
        }
        ListNode *thead = head;
        int cnt = 0;
        while (thead != NULL) {
            ++cnt;
            thead = thead->next;
        }
        k = k % cnt;
        if (k == 0) {
            return head;
        }
        thead = head;
        int cur = 1;
        k = cnt - k;
        ListNode *prec = NULL;
        while (thead->next != NULL) {
            if (cur == k) {
                prec = thead;
            }
            thead = thead->next;
            ++cur;
        }
        thead->next = head;
        head = prec->next;
        prec->next = NULL;
        return head;
    }
};
