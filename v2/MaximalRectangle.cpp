class Solution {
public:
    int maximalRectangle(vector<vector<char> > &matrix) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int ans = 0, row = matrix.size(), tmp;
        if (row <= 0) {
            return ans;
        }
        stack<int> ms;
        int col = matrix[0].size();
        for (int r = 0; r < row; ++r) {
            for (int c = 0; c < col; ++c) {
                matrix[r][c] -= '0';
            }
            for (int c = 0; r>0 && c<col; ++c) {
                if (matrix[r][c] == 0) {
                    continue;
                }
                matrix[r][c] += matrix[r-1][c];
            }
            for (int c = 0; c < col; ++c) {
                if (ms.empty() || matrix[r][c] >= ms.top()) {
                    ms.push( matrix[r][c]);
                    continue;
                }
                tmp = 1;
                while (!ms.empty() && matrix[r][c] < ms.top()) {
                    ans = max(ans, (ms.top()*(tmp++)));
                    ms.pop();
                }
                while (tmp--) {
                    ms.push(matrix[r][c]);
                }
            }
            tmp = 0;
            while (!ms.empty()) {
                ++tmp;
                ans = max(ans, (ms.top()*tmp));
                ms.pop();
            }
        }
        return ans;
    }
};
