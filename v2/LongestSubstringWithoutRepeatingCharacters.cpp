class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int chmap[256];
        for (int i = 0; i < 256; ++i) {
            chmap[i] = 0;
        }
        int st=0, end=0, ans=0, len = s.length();
        while (end < len) {
            if (chmap[ s[end] ] == 0) {
                ++chmap[ s[end] ];
            }
            else {
                ans = max(ans, end-st);
                for (; s[st]!=s[end]; ++st){
                    --chmap[ s[st] ];
                }
                ++st;
            }
            ++end;
        }
        return max(ans, len-st);
    }
};
