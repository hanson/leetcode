class Solution {
public:
    //O(n*log(n))
    string longestPalindrome(string s) {
        int len = s.length(), max = 0, recl = 0, recr = -1;
        //会溢出
        int *hash = (int*)malloc(sizeof(int)*(len+1));
        int *r_hash = (int*)malloc(sizeof(int)*(len+1));
        hash[0]=0, r_hash[0]=0;
        for (int i = 0; i < len; ++i) {
            hash[i+1] = hash[i]*256 + s[i];
            r_hash[len-i] = r_hash[ (len+1-i)%(len+1) ]*256 + s[len-1-i];
        }
        for (int i = 0; i < len; ++i) {
            int mid, left = 0, right = min(i-0, len-1-i), tmpl, tmpr;
            //odd
            while (left <= right) {
                mid = left + (right-left)/2;
                tmpl = hash[i+1] - hash[i-mid]*pow(256, mid+1);
                tmpr = r_hash[i+1] - r_hash[(i+mid+1+1)%(len+1)]*pow(256, mid+1);
                if (tmpl == tmpr) {
                    left = mid + 1;
                    if (mid*2+1 > max) {
                        max = mid*2+1, recl = i-mid, recr = i+mid;
                    }
                }
                else {
                    right = mid - 1;
                }
            }
            //even
            if (i+1<len && s[i]==s[i+1]) {
                left = 0, right = min(i-0, len-1-i-1);
                while (left <= right) {
                    mid = left + (right-left)/2;
                    tmpl = hash[i+1] - hash[i-mid]*pow(256, mid+1);
                    tmpr = r_hash[i+1+1] - r_hash[(i+1+mid+1+1)%(len+1)]*pow(256, mid+1);
                    if (tmpl == tmpr) {
                        left = mid + 1;
                        if (mid*2+2 > max) {
                            max = mid*2+2, recl = i-mid, recr = i+1+mid;
                        }
                    }
                    else {
                        right = mid - 1;
                    }
                }
            }
        }
        return s.substr(recl, recr-recl+1);
    }
};
