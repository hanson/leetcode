/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void recoverTree(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        TreeNode *f1=NULL, *f2=NULL, *p=NULL, *cur = root;
        while (cur) {
            if (cur->left == NULL) {
                if (p!=NULL && p->val>cur->val) {
                    if (f1 == NULL) {
                        f1 = p;
                    }
                    f2 = cur;
                }
                p = cur;
                cur = cur->right;
            }
            else {
                TreeNode *tmp = cur->left;
                while (tmp->right!=NULL && tmp->right!=cur) {
                    tmp = tmp->right;
                }
                if (tmp->right == NULL) {
                    tmp->right = cur;
                    cur = cur->left;
                }
                else {
                    tmp->right = NULL;
                    if (p!=NULL && p->val>cur->val) {
                        if (f1 == NULL) {
                            f1 = p;
                        }
                        f2 = cur;
                    }
                    p = cur;
                    cur = cur->right;
                }
            }
        }
        swap(f1->val, f2->val);
    }
};
