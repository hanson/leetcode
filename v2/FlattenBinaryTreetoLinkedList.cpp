/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL) {
            return;
        }
        stack<TreeNode*> ts;
        ts.push(root);
        TreeNode *tr = NULL;
        TreeNode *tmp;
        while(!ts.empty()) {
            tmp = ts.top();
            ts.pop();
           if (tmp->right != NULL) {
                ts.push(tmp->right);
                tmp->right = NULL;
            }
            if (tmp->left != NULL) {
                ts.push(tmp->left);
                tmp->left = NULL;
            }
            if (tr == NULL) {
                root = tmp;
                tr = root;
            }
            else {
                tr->right = tmp;
                tr = tr->right;
            }
        }
    }
};
