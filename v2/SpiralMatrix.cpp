class Solution {
private:
    int dr[4] = {0, 1, 0, -1};
    int dc[4] = {1, 0, -1, 0};
    int row_start;
    int row_end;
    int col_start;
    int col_end;
public:
    bool isValid(int cr, int cc) {
        return cr>=row_start && cr<=row_end && cc>=col_start && cc<=col_end;
    }
    vector<int> spiralOrder(vector<vector<int> > &matrix) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> ret;
        int row = matrix.size();
        if (row == 0) {
            return ret;
        }
        int col = matrix[0].size(), cur_row = 0, cur_col = -1, dir = 0;
        row_start = 0, row_end = row - 1, col_start = 0, col_end = col-1;
        while (row_start<=row_end && col_start<=col_end) {
            while (isValid(cur_row+dr[dir], cur_col+dc[dir])) {
                cur_row += dr[dir];
                cur_col += dc[dir];
                ret.push_back(matrix[cur_row][cur_col]);
            }
            if (dir == 0) {
                ++row_start;
            }
            else if (dir == 1) {
                --col_end;
            }
            else if (dir == 2) {
                --row_end;
            }
            else {
                ++col_start;
            }
            dir = (dir+1)%4;
        }
        return ret;
    }
};
