class Solution {
private:
    vector<string> pars;
public:
    void generateParenthesisHelper(int cur, int n, stack<int> &psta, string &par) {
        if (cur>n && psta.empty()) {
            pars.push_back(par);
        }
        if (cur <= n) {
            psta.push(1);
            par += '(';
            generateParenthesisHelper(cur+1, n, psta, par);
            psta.pop();
            par = par.erase(par.length()-1, 1);
        }
        if (!psta.empty()) {
            psta.pop();
            par += ')';
            generateParenthesisHelper(cur, n, psta, par);
            psta.push(1);
            par = par.erase(par.length()-1, 1);
        }
    }
    vector<string> generateParenthesis(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        pars.clear();
        stack<int> psta;
        string par;
        generateParenthesisHelper(1, n, psta, par);
        return pars;
    }
};
