class Solution {
public:
    class trieNode{
    public:
        char chVal;
        int chCnt;
        trieNode* ch[26];
        trieNode(char tc):chVal(tc), chCnt(0){
            for (int i = 0; i < 26; ++i) { ch[i] = NULL;}
        }
    };
    string longestCommonPrefix(vector<string> &strs) {
        trieNode *root = new trieNode('K'), *curNode;
        int minLen = INT_MAX, tmp;
        for (int i = 0; i < strs.size(); ++i) {
            curNode = root;
            if ((tmp=strs[i].length()) == 0) {return "";}
            minLen = min(minLen, tmp);
            for (int t = 0; t<strs[i].length(); ++t) {
                if (curNode->ch[strs[i][t] - 'a'] == NULL) {
                    curNode->ch[strs[i][t] - 'a'] = new trieNode(strs[i][t]);
                    ++curNode->chCnt;
                }
                curNode = curNode->ch[strs[i][t] - 'a'];                    
            }
        }
        string ret; ++minLen;
        while (root != NULL && minLen--) {
            if (root->chVal != 'K') { ret += root->chVal;}
            if (root->chCnt != 1) { break;}
            for (int i = 0; i < 26; ++i) {
                if (root->ch[i] != NULL) {
                    root = root->ch[i];
                    break;
                }
            }
        }
        return ret;
    }
};
