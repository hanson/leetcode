/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isValidBSTHelper(TreeNode *curNode, int leftMax, int rightMin) {
        if (curNode == NULL) {
            return true;
        }
        if (curNode->val <= leftMax || curNode->val >= rightMin) {
            return false;
        }
        return isValidBSTHelper(curNode->left, leftMax, curNode->val) && isValidBSTHelper(curNode->right, curNode->val, rightMin);
    }
    bool isValidBST(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return isValidBSTHelper(root, INT_MIN/*leftMax*/, INT_MAX/*rightMin*/);
    }
};
