/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
private:
    class queElem {
        public:
        TreeLinkNode *tn;
        int level;
        queElem(TreeLinkNode *ttn, int tl):tn(ttn), level(tl){}
    };
public:
    void connect(TreeLinkNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        queue<queElem*> que;
        que.push(new queElem(root, 0));
        queElem *cur, *pre = NULL;
        while (!que.empty()) {
            cur = que.front();
            que.pop();
            if (cur->tn == NULL) {
                continue;
            }
            if (pre!=NULL && pre->level==cur->level) {
                pre->tn->next = cur->tn;
            }
            pre = cur;
            que.push(new queElem(cur->tn->left, cur->level+1));
            que.push(new queElem(cur->tn->right, cur->level+1));
        }
    }
};
