class Solution {
public:
    bool canJump(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int cur = 0;
        for (int i = 0; i < n-1; ++i, --cur) {
            cur = max(cur, A[i]);
            if (cur <= 0) {
                return false;
            }
        }
        return true;
    }
};
