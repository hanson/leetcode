#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        bool isMatch(const char *s, const char *p) {
            const char *sIndex = s, *pIndex = p, *starIndex = NULL;
            while (*sIndex != '\0') {
               if (*pIndex == '*') {
                   s = sIndex;
                   while (*pIndex == '*') {
                    ++pIndex;
                   }
                   starIndex = pIndex-1;
               }
               else if (*pIndex == '\0') {
                    if (starIndex == NULL) {
                        return false;
                    }
                    ++s, sIndex = s, pIndex = starIndex + 1;
               }
               else if (*pIndex=='?' || *pIndex==*sIndex) {
                    ++pIndex, ++sIndex;
               }
               else if (*pIndex != *sIndex) {
                   ++s, sIndex = s;
                   if (starIndex == NULL) {
                       return false;
                   }
                   pIndex = starIndex + 1;
               }
            }
            while (*pIndex == '*') {
                ++pIndex;
            }
            return (*pIndex=='\0');
        }
};
