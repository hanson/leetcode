/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    class queElem{
        public:
            TreeNode* tn;
            int level;
            queElem(int tl, TreeNode *ttn):level(tl), tn(ttn){
            }
    };
    vector<vector<int> > levelOrder(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> >ret;
        if (root == NULL) {
            return ret;
        }
        queElem *front;
        queue<queElem*> que;
        que.push(new queElem(0, root));
        while (!que.empty()) {
            front = que.front();
            que.pop();
            if (front->level >= ret.size()) {
                vector<int> tmp;
                tmp.push_back(front->tn->val);
                ret.push_back(tmp);
            }
            else {
                ret[front->level].push_back(front->tn->val);
            }
            if (front->tn->left) {
                que.push(new queElem(front->level+1, front->tn->left));
            }
            if (front->tn->right) {
                 que.push(new queElem(front->level+1, front->tn->right));   
            }
        }
        return ret;
    }
};
