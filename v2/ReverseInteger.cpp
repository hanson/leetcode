class Solution {
public:
    int reverse(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int flag = 1;
        if (x < 0) {
            flag = -1;
            x = -x;
        }
        int val = 0;
        while (x > 0) {
            val *= 10;
            val += (x % 10);
            x /= 10;
        }
        return val*flag;
    }
};
