/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    struct cmp  
    {  
        bool operator()(const Interval& lhs, const Interval& rhs) const  
        {  
            return lhs.start < rhs.start;  
        }  
    }; 
    vector<Interval> merge(vector<Interval> &intervals) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<Interval> ret;
        if (intervals.size() == 0) {return ret;}
        sort(intervals.begin(), intervals.end(), cmp());
        Interval tmp = intervals[0];
        for (int i = 0; i < intervals.size(); ++i) {
            if (tmp.end >= intervals[i].start) {
                tmp.end = max(tmp.end, intervals[i].end);
            }
            else {
                ret.push_back(tmp);
                tmp = intervals[i];
            }
        }
        ret.push_back(tmp);
        return ret;
    }
};
