/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *partition(ListNode *head, int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode *retHead = new ListNode(INT_MAX);
        retHead->next = head;
        ListNode *preNode = retHead, *curNode = head, *pNode = retHead;
        while (pNode->next!=NULL && pNode->next->val<x) {
            pNode = pNode->next;
        }
        preNode = pNode;
        curNode = pNode->next;
        while (curNode != NULL) {
            if (curNode->val < x) {
                preNode->next = curNode->next;
                curNode->next = pNode->next;
                pNode->next = curNode;
                pNode = curNode;
                curNode = preNode->next;
                continue;
            }
            preNode = curNode;
            curNode = curNode->next;
        }
        return retHead->next;
    }
};
