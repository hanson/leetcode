class Solution {
public:
    int findKth(int A[], int al, int ar, int B[], int bl, int br, int k) {
        int ca = ar-al+1, cb = br-bl+1;
        if (ca > cb) {
            return findKth(B, bl, br, A, al, ar, k);
        }
        if (ca <= 0) {
            return B[bl+k-1];
        }
        if (k == 1) {
            return min(A[al], B[bl]);
        }
        int ak = min(k>>1, ca);
        int bk = k - ak;
        ak = al+ak-1, bk = bl+bk-1;
        if (A[ak] >= B[bk]) {
            return findKth(A, al, ak, B, bk+1, br, k-(bk-bl+1));
        }
        return findKth(A, ak+1, ar, B, bl, bk, k-(ak-al+1));
    }
    double findMedianSortedArrays(int A[], int m, int B[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if ((m+n)&1) {
            return findKth(A, 0, m-1, B, 0, n-1, (m+n)/2+1);
        }
        return (findKth(A, 0, m-1, B, 0, n-1, (m+n)/2)+findKth(A, 0, m-1, B, 0, n-1, (m+n)/2+1))*1.0/2;
    }
};
