class Solution {
public:
    //dp[i][j]: s[i..len(s)] p[j..len(p)] is match
    //if p[j+1] != '*' if (s[i]==p[j]) dp[i][j] = dp[i+1][j+1] else false
    //if p[j+1] == '*' extract * till it match
    bool isMatch(const char *s, const char *p) {
        if (*p == '\0') { return *s=='\0';}
        if (*(p+1) != '*') { return (*p==*s || (*p=='.' && *s!='\0')) && isMatch(s+1, p+1);}
        while (*s==*p || (*p=='.' && *s!='\0')) {
            if (isMatch(s++, p+2)) { return true;}
        }
        return isMatch(s, p+2);
    }
};