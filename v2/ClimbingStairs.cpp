class Solution {
public:
    int climbStairs(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int f[n];
        f[0] = f[1] = 1;
        f[2] = 2;
        for (int i = 3; i <= n; ++i) {
            f[i] = f[i-2] + f[i-1];
        }
        return f[n];
    }
};
