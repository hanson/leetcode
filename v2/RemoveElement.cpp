class Solution {
public:
    int removeElement(int A[], int n, int elem) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int start = 0;
        int end = n - 1;
        while (start <= end) {
            if (A[start] != elem) {
                ++start;
                continue;
            }
            swap(A[start], A[end]);
            --end;
        }
        return end + 1;
    }
};
