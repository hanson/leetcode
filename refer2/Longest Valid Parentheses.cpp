class Solution {
public:
    int longestValidParentheses(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        s=" "+s;
        int i,j,k,n=s.length(),ans=0;
        vector<int> f(n);
        for (f[0]=0,i=1;i<n;ans=max(ans,f[i++]))
        	if (s[i]=='(') f[i]=0;
        	else if (s[i-1]=='(') f[i]=2+f[i-2];
        	else if (s[i-1-f[i-1]]=='(') f[i]=2+f[i-1]+f[i-1-f[i-1]-1];
        return ans;
    }
};
