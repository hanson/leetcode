class Solution {
public:
    bool isValid(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int a,b,c,i;
        stack<int> stack;
        for (i=a=b=c=0;i<s.length();i++)
        {
			switch (s[i])
			{
				case '(':	stack.push(0);
							break;
				case ')':	if (stack.empty() || stack.top()!=0) return false;
							stack.pop(); break;
				case '[':	stack.push(1);
							break;
				case ']':	if (stack.empty() || stack.top()!=1) return false;
							stack.pop(); break;
				case '{':	stack.push(2);
							break;
				case '}':	if (stack.empty() || stack.top()!=2) return false;
							stack.pop(); break;
			}
		}
		if (!stack.empty()) return false;
		return true;
    }
};
