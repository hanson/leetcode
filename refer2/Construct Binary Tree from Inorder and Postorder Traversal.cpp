/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	TreeNode* dfs(int x,int y,int l,int r,vector<int> &inorder, vector<int> &postorder){
		if (x>y) return NULL;
		int k;
		for (k=x;k<=y && inorder[k]!=postorder[r];k++);
		TreeNode* node=new TreeNode(postorder[r]);
		node->left=dfs(x,k-1,l,l+k-x-1,inorder,postorder);
		node->right=dfs(k+1,y,l+k-x,r-1,inorder,postorder);
		return node;
	}
    TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return dfs(0,inorder.size()-1,0,postorder.size()-1,inorder,postorder);
    }
};
