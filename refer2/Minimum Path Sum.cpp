class Solution {
public:
    int minPathSum(vector<vector<int> > &grid) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > f(grid);
        if (grid.size()==0) return 0;
    	int m=f.size(),n=f[0].size();
    	for (int i=1;i<m;i++) f[i][0]+=f[i-1][0];
    	for (int j=1;j<n;j++) f[0][j]+=f[0][j-1];
    	for (int i=1;i<m;i++) for (int j=1;j<n;j++)
    		f[i][j]+=min(f[i-1][j],f[i][j-1]);
    	return f[m-1][n-1];
    }
};
