class Solution {
public:
    vector<int> getRow(int rowIndex) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (rowIndex==0) return vector<int>(1,1);
        vector<vector<int> > f(2);
        int u,v,i,j;
        f[0].push_back(1);
        for (i=1;i<=rowIndex;i++)
        {
			u=i & 1; v=1-u;
			f[u].resize(i+1);
			f[u][0]=f[v][0]; f[u][i]=f[v][i-1];
			for (j=1;j<i;j++) f[u][j]=f[v][j-1]+f[v][j];
		}
		return f[u];
    }
};
