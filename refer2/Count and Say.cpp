class Solution {
public:
    string countAndSay(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        string a="1",b;
        int i,j,k;
        for (i=2;i<=n;i++,a=b)
			for (b="",j=0;j<a.length();j=k+1)
			{
				for (k=j;k+1<a.length() && a[j]==a[k+1];k++);
				b+=to_string(k-j+1)+a[j];
			}
		return a;
    }
};
