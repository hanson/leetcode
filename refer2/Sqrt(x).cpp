class Solution {
public:
    int sqrt(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        double ans=x;
        while (fabs(ans*ans-x)>1e-6)
			ans=(ans+x/ans)/2;
		return (int)(ans+1e-6);
    }
};
