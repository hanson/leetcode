class Solution {
public:
    void nextPermutation(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n=num.size(),i,j,k,x,y;
        for (i=n-2;i>=0 && num[i]>=num[i+1];i--);
        if (i<0)
        {
			reverse(num.begin(),num.end());
			return;
		}
		for (j=i+1;j+1<n && num[j+1]>num[i];j++);
		swap(num[i],num[j]);
		for (j=i+1;j<n-(j-i);j++) swap(num[j],num[n-(j-i)]);
    }
};
