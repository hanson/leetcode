/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,n=intervals.size();
        vector<Interval> ret;
        for (i=0;i<n && intervals[i].end<newInterval.start;i++) ret.push_back(intervals[i]);
        if (i==n) { ret.push_back(newInterval); return ret;}
        for (j=i;j<n && intervals[j].start<=newInterval.end;j++);
    	if (i==j) ret.push_back(newInterval);
    	else ret.push_back(Interval(min(newInterval.start,intervals[i].start),max(newInterval.end,intervals[j-1].end)));
    	for (;j<n;j++) ret.push_back(intervals[j]);
    	return ret;
    }
};
