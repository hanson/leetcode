class Solution {
public:
	struct Node{
		vector<string> pre;
		int step;
	};	
    int ladderLength(string start, string end, unordered_set<string> &dict) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,n=dict.size();
        bool flag=false;
        queue<string> q;
        unordered_map<string,Node> visit;
        
        q.push(end);
        Node tmpNode;
        tmpNode.step=1;
        visit[end]=tmpNode;
        if (dict.find(start)==dict.end()) dict.insert(start);
        while (!q.empty())
        {
			string str=q.front(); q.pop();
			int step=visit[str].step;
			for (i=0;i<str.length();i++)
			{
				string tmp=str;
				for (char ch='a';ch<='z';ch++) if (str[i]!=ch)
				{
					tmp[i]=ch;
					if (tmp==start) return step+1;
					if (dict.find(tmp)!=dict.end())
					{
						unordered_map<string,Node>::iterator it=visit.find(tmp);
						if (it==visit.end())
						{
							if (!flag || tmp==start)
							{
								tmpNode.pre.assign(1,str);
								tmpNode.step=step+1;
								visit[tmp]=tmpNode;
								if (tmp!=start) q.push(tmp);
							}
							continue;
						}
						if (it->second.step<step+1) continue;
						it->second.pre.push_back(str);
					}
				}
			}
		}
		return 0;
    }
};



