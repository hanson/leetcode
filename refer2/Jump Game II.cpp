class Solution {
public:
    int jump(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> f(n,1<<30);
        int i,j,k,next,bound,step;
        for (bound=0,step=0,next=0,i=0;i<n && i<=next;i++)
        {
			if (i>bound)
			{
				bound=next;
				step++;
			}
			next=max(next,i+A[i]);
		}
        if (i==n) return step;
        return -1;
    }
};
