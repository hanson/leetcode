class Solution {
public:
    int lengthOfLastWord(const char *s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        string t(s);
        while (t!="" && t.rfind(' ')==t.length()-1) t.erase(t.length()-1);
		if (t=="") return 0;
        int k;
        if ((k=t.rfind(' '))==string::npos) return t.length();
        return t.length()-k-1;
    }
};
