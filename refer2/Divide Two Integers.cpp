class Solution {
public:
    int divide(int dividend, int divisor) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        long long i,j,k,sign,b=dividend,a=divisor,ori,ans=0;
        if (a==0) return 0;
        sign=(a<0) ^ (b<0);
        a=abs(a); b=abs(b); ori=b;
        for (k=0;a<=b;k++) b=b>>1;
        for (i=k-1;i>=0;i--)
        {
    		b= (b<<1) | ((ori & 1LL<<i)!=0);
			ans=(ans<<1)|(b>=a);
			if (b>=a) b-=a;
		}
		return sign?-ans:ans;
    }
};
