class Solution {
public:
	void addAnswer(vector<vector<int> > &a,int x,int y,int z,int t){
		vector<int> tmp;
		tmp.push_back(x); tmp.push_back(y);
		tmp.push_back(z); tmp.push_back(t);
		a.push_back(tmp);
	}
    vector<vector<int> > fourSum(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n=num.size(),i,j,k,t,x,y;
        vector<vector<int> > ret;
        sort(num.begin(),num.end());
        for (t=0;t+3<n;t++)
        {
	        for (i=t+1;i+2<n;i++)
	        {
				for (j=i+1,k=n-1;j<k;)
				{
					if (num[j]+num[k]+num[i]+num[t]==target) addAnswer(ret,num[t],num[i],num[j],num[k]);
					for (x=j;x+1<=k && num[x]==num[x+1];x++);
					for (y=k;y-1>=j && num[y]==num[y-1];y--);
					if (abs(num[t]+num[i]+num[x+1]+num[k]-target)<
						abs(num[t]+num[i]+num[j]+num[y-1]-target))
						j=x+1;
					else
						k=y-1;
				}
				while (i+2<n && num[i+1]==num[i]) i++;
			}
			while (t+3<n && num[t+1]==num[t]) t++;
		}
		return ret;
    }
};
