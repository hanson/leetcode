class Solution {
public:
    bool isScramble(string s1, string s2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n=s1.length();
        string s3=s1,s4=s2;
        sort(s3.begin(),s3.end());
        sort(s4.begin(),s4.end());
        if (s3!=s4) return false;
        if (s1==s2) return true;
        for (int i=0;i<s1.length()-1;i++)
        {
			if (isScramble(s1.substr(0,i+1),s2.substr(0,i+1)) && 
				isScramble(s1.substr(i+1,n-i-1),s2.substr(i+1,n-i-1)) ||
				isScramble(s1.substr(0,i+1),s2.substr(n-i-1,i+1)) && 
				isScramble(s1.substr(i+1,n-i-1),s2.substr(0,n-i-1)))
				return true;
		}
		return false;		
    }
};
