class Solution {
public:
    string multiply(string num1, string num2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int m=num1.length(),n=num2.length();
        vector<int> a(m),b(n),c(m+n);
        int i,j,k,x;
        for (i=0;i<m;i++) a[i]=num1[m-i-1]-'0';
        for (j=0;j<n;j++) b[j]=num2[n-j-1]-'0';
		for (i=0;i<m;i++) for (j=0;j<n;j++)
			c[i+j]+=a[i]*b[j];
		for (i=0;i<m+n-1;i++)
			c[i+1]+=c[i]/10,c[i]%=10;
		for (i=m+n-1;c[i]==0 && i>0;i--);
		string ret;
		for (;i>=0;i--) ret+=(char)(c[i]+'0');
		return ret;
    }
};
