class Solution {
public:
	int getBit(int x,int idx){return (1<<idx)&x;}
    vector<int> grayCode(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> ret;
        for (int i=0;i<1<<n;i++)
        {
			int j,k=i;
			for (j=0;j<n;j++) k^=getBit(k>>1,j);
			ret.push_back(k);
		}
		return ret;
    }
};
