class Solution {
public:
	int findKth(int A[], int m, int B[], int n, int k){
		if (m==0) return B[k-1];
		if (n==0) return A[k-1];
		int mm=m/2,mn=n/2;
		if (k>mm+mn+1)
			if (A[mm]<B[mn]) return findKth(A+mm+1,m-mm-1,B,n,k-mm-1);
			else return findKth(A,m,B+mn+1,n-mn-1,k-mn-1);
		else if (k<=mm+mn+1)
			if (A[mm]<B[mn]) return findKth(A,m,B,mn,k);
			else return findKth(A,mm,B,n,k);
	}
    double findMedianSortedArrays(int A[], int m, int B[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (m+n & 1) return findKth(A,m,B,n,(m+n+1)/2);
        return (double)(findKth(A,m,B,n,(m+n)/2)+findKth(A,m,B,n,(m+n)/2+1))/2;
    }
};
