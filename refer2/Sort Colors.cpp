class Solution {
public:
    void sortColors(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int x,y,z;
        for (x=0,y=z=n-1;x<=y;)
        	if (A[x]==0) x++;
        	else if (A[x]==1) swap(A[x],A[y--]);
        	else swap(A[x],A[z--]),y=min(y,z);
    }
};
