class Solution {
public:
    int maxProfit(vector<int> &prices) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,n=prices.size(),ret=0;
        if (n==0) return 0;
        for (j=prices[0],i=1;i<n;i++)
        	if (prices[i]<prices[i-1])
        	{
				ret+=prices[i-1]-j;
				j=prices[i];
			}
		return ret+prices[n-1]-j;
    }
};
