class Solution {
public:
    string longestCommonPrefix(vector<string> &strs) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (strs.size()==0) return "";
        for (int j=0;;j++)
        {
			if (j==strs[0].length()) return strs[0].substr(0,j);
			char ch=strs[0][j];
	        for (int i=1;i<strs.size();i++)
	        	if (j==strs[i].length() || strs[i][j]!=ch) return strs[i].substr(0,j);
		}
    }
};
