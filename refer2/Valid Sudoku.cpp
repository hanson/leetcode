class Solution {
public:
    bool isValidSudoku(vector<vector<char> > &board) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<bool> > row,col,squ;
        for (int i=0;i<9;i++)
        {
			row.push_back(vector<bool>(9,false));
			col.push_back(vector<bool>(9,false));
			squ.push_back(vector<bool>(9,false));
		}
		for (int x=0;x<9;x++) for (int y=0;y<9;y++) if (board[x][y]!='.')
		{
			int k=x/3*3+y/3, i=board[x][y]-'1';
			if (row[x][i] || col[y][i] || squ[k][i]) return false;
			row[x][i]=col[y][i]=squ[k][i]=true;
		}
    }
};
