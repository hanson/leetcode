class Solution {
public:
    int maxArea(vector<int> &height) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n=height.size(),i,j,k,ans=0;
        for (i=0,j=n-1;i<j;)
        {
			ans=max(ans,(j-i)*min(height[i],height[j]));
			if (height[i]<height[j])
				for (k=height[i],++i;i<=j && height[i]==k;i++);
			else
				for (k=height[j],--j;i<=j && height[j]==k;j--);
		}
		return ans;
    }
};
