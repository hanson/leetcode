/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	vector<int> a;
	TreeNode *dfs(int x,int y){
		if (x>y) return NULL;
		int mid=(x+y)>>1;
		TreeNode* node=new TreeNode(a[mid]);
		node->left=dfs(x,mid-1);
		node->right=dfs(mid+1,y);
		return node;
	}
    TreeNode *sortedArrayToBST(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        a=num;
        return dfs(0,a.size()-1);
    }
};
