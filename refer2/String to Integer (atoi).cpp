class Solution {
public:
    int atoi(const char *str) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,sign,ans;
        
		while (*str==' ') str++;
        const char *x=str;
		if (*x=='+' || *x=='-') x++;
		while (*x=='0') x++;
		if (!isdigit(*x)) return 0;
		if (*str=='-') ans=-1*(*x-'0'),sign=-1;
		else ans=*x-'0',sign=1;
		while (isdigit(*++x))
		{
			if (sign==1 && (INT_MAX-(*x-'0'))/10<ans)	return INT_MAX;
			if (sign==-1 && (INT_MIN+(*x-'0'))/10>ans)	return INT_MIN;
			ans=ans*10+sign*(*x-'0');
		}
		return ans;
    }
};
