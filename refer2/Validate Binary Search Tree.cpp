/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	bool dfs(TreeNode *x,int &pre){
		if (x==NULL) return true;
		if (!dfs(x->left,pre)) return false;
		if (pre>=x->val) return false;
		pre=x->val;
		return dfs(x->right,pre);
	}
    bool isValidBST(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int pre=-(1<<30);
        dfs(root,pre);
    }
};
