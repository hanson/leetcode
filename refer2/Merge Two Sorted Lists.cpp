/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode *ret,*x,*y,*z,p(0);
        z=ret=&p;
        for (x=l1,y=l2;x!=NULL || y!=NULL;)
        	if (y==NULL || x!=NULL && x->val<y->val)
        	{
				z->next=x;
				x=x->next;
				z=z->next;
			}else
			{
				z->next=y;
				y=y->next;
				z=z->next;
			}
		return ret->next;
    }
};
