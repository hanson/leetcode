/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	TreeNode* dfs(TreeNode* x){
		if (x==NULL) return NULL;
		TreeNode *a=dfs(x->left), *b=dfs(x->right);
		if (a!=NULL) 
		{
			a->right=x->right;
			x->right=x->left;
			x->left=NULL;
			if (b==NULL) return a;
		}
		if (b==NULL) return x;
		return b;
	}
    void flatten(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        dfs(root);
    }
};
