/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *swapPairs(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode tmp(0),*x=&tmp,*y,*z,*p;
        x->next=head; head=x;
        while (x->next!=NULL && x->next->next!=NULL)
        {
			y=x->next; z=y->next; p=z->next;
			x->next=z; z->next=y; y->next=p;
			x=y;
		}
		return head->next;
    }
};
