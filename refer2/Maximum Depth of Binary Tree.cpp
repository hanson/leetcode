/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	int ans;
	void dfs(TreeNode *x,int depth){
		if (x==NULL) return;
		if (x->left==NULL && x->right==NULL)
			ans=max(ans,depth);
		dfs(x->left,depth+1);
		dfs(x->right,depth+1);
	}
    int maxDepth(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ans=0; dfs(root,1);
        return ans;
    }
};
