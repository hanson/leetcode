class Solution {
public:
    string minWindow(string S, string T) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int m=S.length(),n=T.length();
        if (m==0 || n==0) return "";
        unordered_map<char,int> map;
        unordered_map<char,int>::iterator it;
        int i,j,k;
        for (i=0;i<n;i++)
        {
			it=map.find(T[i]);
			if (it==map.end()) map[T[i]]=1;
			else it->second++;
		}
		for (k=0,j=0;j<m;j++)
		{
			it=map.find(S[j]);
			if (it!=map.end())
			{
				it->second--;
				if (it->second>=0) k++;
			}
			if (k==n) break;
		}
		if (k!=n) return "";
		string ans=S;
		for (i=0;;i++)
		{
			for (;i<m;i++)
			{
				it=map.find(S[i]);
				if (it!=map.end())
				{
					it->second++;
				 	if (it->second==1) break;
				}
			}
			if (j-i+1<ans.length()) ans=S.substr(i,j-i+1);
			for (++j;j<m;j++)
			{
				it=map.find(S[j]);
				if (it!=map.end()) it->second--;
				if (S[i]==S[j]) break;
			}
			if (j>=m) break;
		}
		return ans;
    }
};
