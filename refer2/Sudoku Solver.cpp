class Solution {
public:
	vector<vector<bool> > row,col,squ;
	bool dfs(int x,int y,vector<vector<char> > &board){
		if (y==9) x++,y=0;
		if (x==9) return true;
		if (board[x][y]=='.')
			for (int i=0;i<9;i++)
			{
				int k=x/3*3+y/3;
				if (row[x][i] || col[y][i] || squ[k][i]) continue;
				row[x][i]=col[y][i]=squ[k][i]=true; board[x][y]=(char)('1'+i);
				if (dfs(x,y+1,board)) return true;
				row[x][i]=col[y][i]=squ[k][i]=false; board[x][y]='.';
			}
		else
			dfs(x,y+1,board);
	}
			
    void solveSudoku(vector<vector<char> > &board) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        row.clear(); col.clear(); squ.clear();
        for (int i=0;i<9;i++)
        {
			row.push_back(vector<bool>(9,false));
			col.push_back(vector<bool>(9,false));
			squ.push_back(vector<bool>(9,false));
		}
		for (int x=0;x<9;x++) for (int y=0;y<9;y++) if (board[x][y]!='.')
		{
			int k=x/3*3+y/3, i=board[x][y]-'1';
			row[x][i]=col[y][i]=squ[k][i]=true;
		}
        dfs(0,0,board);
    }
};
