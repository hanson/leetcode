class Solution {
public:
    vector<string> anagrams(vector<string> &strs) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<pair<string,int> > a;
        int n=strs.size(),i,j,k;
        vector<string> ret;
        for (i=0;i<n;i++)
		{
			string tmp=strs[i];
			sort(tmp.begin(),tmp.end());
			a.push_back(make_pair(tmp,i));
		}
        sort(a.begin(),a.end());
        for (i=0;i<n;i=j+1)
        {
			for (j=i;j+1<n && a[j+1].first==a[i].first;j++);
			if (i==j) continue;
			for (k=i;k<=j;k++) ret.push_back(strs[a[k].second]);
		}
		return ret;
    }
};
