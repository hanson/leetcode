class Solution {
public:
    vector<vector<int> > combine(int n, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > ret,a;
        if (n<k || k<0) return ret;
        if (n==0)
        {
			ret.push_back(vector<int>());
			return ret;
		}
		ret=combine(n-1,k); a=combine(n-1,k-1);
        for (int i=0;i<a.size();i++)
        {
			a[i].push_back(n);
			ret.push_back(a[i]);
		}
    }
};
