/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> ret;
        if (root==NULL) return ret;
        stack<pair<TreeNode*,int> > stack;
        stack.push(make_pair(root,0));
        while (!stack.empty())
        {
			TreeNode *x=stack.top().first;
			int idx=stack.top().second;
			if (idx==0 && x->left!=NULL)
			{
				stack.top().second=1;
				stack.push(make_pair(x->left,0));
			}else
			{
				ret.push_back(x->val);
				stack.pop();
				if (x->right!=NULL) stack.push(make_pair(x->right,0));
			}
		}
		return ret;
    }
};
