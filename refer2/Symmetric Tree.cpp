/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	void dfs(TreeNode *x,int depth,vector<vector<int> > &a){
		if (depth>=a.size()) a.resize(depth+1);
		if (x==NULL)
		{
			a[depth].push_back(1<<30);
			return;
		}
		a[depth].push_back(x->val);
		dfs(x->left,depth+1,a);
		dfs(x->right,depth+1,a);
	}
    bool isSymmetric(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > a,b;
        if (root==NULL) return true;
        dfs(root->left,0,a); dfs(root->right,0,b);
        if (a.size()!=b.size()) return false;
        for (int i=0;i<a.size();i++)
        {
			reverse(b[i].begin(),b[i].end());
			if (!(a[i]==b[i])) return false;
		}
		return true;
    }
};
