class Solution {
public:
	vector<vector<string> > ans;
	vector<int> now;
	void dfs(int x,int n,int left,int right,int down){
		if (x==n)
		{
			vector<string> tmp;
			for (int i=0;i<n;i++)
			{
				string s(n,'.');
				if (now[i]>=0) s[now[i]]='Q';
				tmp.push_back(s);
			}
			ans.push_back(tmp);
			return;
		}
		for (int i=0;i<n;i++)
		{
			if ((left | right | down) & 1<<i) continue;
			now.push_back(i);
			dfs(x+1,n,(left|1<<i)<<1,(right|1<<i)>>1,(down|1<<i));
			now.pop_back();
		}
	}
    vector<vector<string> > solveNQueens(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ans.clear();
        dfs(0,n,0,0,0);
        return ans;
    }
};
