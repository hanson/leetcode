class Solution {
public:
    vector<string> fullJustify(vector<string> &words, int L) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n=words.size();
        int i,j,k,t,x,y;
        vector<string> ret;
        for (i=0;i<n;i=j+1)
        {
			for (k=words[i].length(),j=i;j+1<n && k+1+words[j+1].length()<=L;k+=1+words[++j].length());
			string s;
			if (j+1>=n)
			{
				for (s=words[i],t=i+1;t<=j;t++) s+=" "+words[t];
				s.append(L-k,' ');
			}else if (i==j)
			{
				s=words[i];
				s.append(L-k,' ');
			}else
			{
				x=1+(L-k)/(j-i); y=(L-k)%(j-i);
				for (s=words[i],t=i+1;t<=j;t++,y--)
					s+=string(x+(y>0),' ')+words[t];
			}
			ret.push_back(s);
		}
		return ret;
    }
};
