class Solution {
public:
    bool searchMatrix(vector<vector<int> > &matrix, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int m=matrix.size();
        if (m==0) return false;
        int n=matrix[0].size();
        int l,r,mid,k;
        for (l=0,r=m-1;l<=r;)
        {
			mid=(l+r)>>1;
			if (target<matrix[mid][0]) r=mid-1;
			else l=mid+1;
		}
		if (r<0) return false;
		for (k=r,l=0,r=n-1;l<=r;)
        {
			mid=(l+r)>>1;
			if (target<matrix[k][mid]) r=mid-1;
			else l=mid+1;
		}
        if (r<0) return false;
        return matrix[k][r]==target;
    }
};
