class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > f(obstacleGrid);
        if (f.size()==0) return 0;
    	int m=f.size(),n=f[0].size(),i,j,k;
    	f[0][0]=!obstacleGrid[0][0];
    	for (i=1;i<m;i++) f[i][0]=(f[i-1][0]&&!obstacleGrid[i][0]);
    	for (j=1;j<n;j++) f[0][j]=(f[0][j-1]&&!obstacleGrid[0][j]);
    	for (i=1;i<m;i++) for (j=1;j<n;j++)
    		if (obstacleGrid[i][j]) f[i][j]=0;
			else f[i][j]=f[i-1][j]+f[i][j-1];
    	return f[m-1][n-1];
    }
};
