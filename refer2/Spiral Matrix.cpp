class Solution {
public:
    vector<int> spiralOrder(vector<vector<int> > &matrix) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> ret;
        int m=matrix.size();
        if (m==0) return  ret;
		int n=matrix[0].size();
		const int move[4][2]={{0,1},{1,0},{0,-1},{-1,0}};
		int i,j,k,len,dir,t,x,y;
		for (j=0;j<n-1;j++) ret.push_back(matrix[0][j]);
		for (dir=1,i=0,j=n-1,k=1;ret.size()+1<m*n;k+=(++dir&1))
				for (dir%=4,x=0;x<((dir&1)?m-k:n-k);x++,i+=move[dir][0],j+=move[dir][1])
					ret.push_back(matrix[i][j]);
		ret.push_back(matrix[i][j]);
		return ret;
    }
};
