/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int> > levelOrderBottom(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k;
        vector<vector<int> > ret;
        if (root==NULL) return ret;
        vector<pair<TreeNode *,int> > q;
        q.push_back(make_pair(root,0));
        for (i=0;i<q.size();i++)
        {
			TreeNode* x=q[i].first;
			int depth=q[i].second;
			if (x->left!=NULL) q.push_back(make_pair(x->left,depth+1));
			if (x->right!=NULL) q.push_back(make_pair(x->right,depth+1));
		}
		for (i=q.size()-1;i>=0;i=j-1)
		{
			for (j=i;j>0 && q[j-1].second==q[i].second;j--);
			vector<int> b;
			for (k=j;k<=i;k++) b.push_back(q[k].first->val);
			ret.push_back(b);
		}
		return ret;
    }
};
