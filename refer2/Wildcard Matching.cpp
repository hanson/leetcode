class Solution {
public:
    bool isMatch(const char *s, const char *p) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        const char *ps,*pp;
        for (ps=pp=NULL;*s;)
        {
			if (*p=='?' || *s==*p) ++s,++p;
			else if (*p=='*')
			{
				while (*p=='*') ++p;
				if (*p==0) return true;
				pp=p; ps=s;
			}else if (*p!=*s && pp!=NULL)
			{
				s=++ps;
				p=pp;
			}else
				return false;
		}
        while (*p)
		{
			if (*p!='*') return false;
			p++;
		}
		return true;
    }
};
