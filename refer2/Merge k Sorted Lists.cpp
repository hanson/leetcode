/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *mergeKLists(vector<ListNode *> &lists) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,n=lists.size();
        ListNode tmp(0),*x=&tmp,*head=x;
        priority_queue<pair<int,int> > heap;
        for (i=0;i<n;i++) if (lists[i]!=NULL)
			heap.push(make_pair(-lists[i]->val,i));
        while (!heap.empty())
        {
			i=heap.top().second; heap.pop();
			x->next=lists[i]; x=x->next;
			lists[i]=x->next;
			if (x->next!=NULL) heap.push(make_pair(-x->next->val,i));
		}
		return head->next;
    }
};
