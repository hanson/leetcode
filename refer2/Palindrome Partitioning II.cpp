class Solution {
public:
    int minCut(string s){
		// Start typing your C/C++ solution below
		// DO NOT write int main() function
		s="$"+s;
		int i,j,k,n=s.length();
		vector<vector<int> > g(n);
		for (i=0;i<n;i++)
		{
			for (j=0;i-j>=0 && i+j<n && s[i-j]==s[i+j];j++)
				g[i+j].push_back(i-j-1);
			for (j=0;i-j>=0 && i+j+1<n && s[i-j]==s[i+j+1];j++)
				g[i+j+1].push_back(i-j-1);
		}
		vector<int> f(n,1<<30);
		f[0]=0;
		for (i=0;i<n;i++) for (j=0;j<g[i].size();j++)
			f[i]=min(f[i],f[g[i][j]]+1);
		return f[n-1]-1;
    }
};