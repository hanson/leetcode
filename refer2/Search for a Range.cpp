class Solution {
public:
    vector<int> searchRange(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int l=0,r=n-1,mid;
        while (l<=r)
        {
			mid=(l+r)>>1;
			if (target<=A[mid]) r=mid-1;
			else l=mid+1;
		}
		if (A[l]!=target) return vector<int>(2,-1);
		int k=l; l++; r=n-1;
		while (l<=r)
        {
			mid=(l+r)>>1;
			if (target<A[mid]) r=mid-1;
			else l=mid+1;
		}
		vector<int> ret;
		ret.push_back(k); ret.push_back(r);
		return ret;
    }
};
