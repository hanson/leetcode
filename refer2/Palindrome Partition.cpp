class Solution {
public:
	vector<vector<string> > ret;
	vector<string> tmpAns;
	vector<vector<int> > g;
	int n;
	string t;
	void dfs(int x){
		if (x>=n)
		{
			ret.push_back(tmpAns);
			return;
		}
		for (int j=0;j<g[x].size();j++)
		{
			tmpAns.push_back(t.substr(x,g[x][j]-x));
			dfs(g[x][j]);
			tmpAns.pop_back();
		}
	}
			
    vector<vector<string> > partition(string s) {
		// Start typing your C/C++ solution below
		// DO NOT write int main() function
		int i,j,k;
		n=s.length(); t=s;
		g.clear(); g.resize(n);
		for (i=0;i<n;i++)
		{
			for (j=0;i-j>=0 && i+j<n && s[i-j]==s[i+j];j++)
				g[i-j].push_back(i+j+1);
			for (j=0;i-j>=0 && i+j+1<n && s[i-j]==s[i+j+1];j++)
				g[i-j].push_back(i+j+2);
		}
		ret.clear(); tmpAns.clear();
		dfs(0);
		return ret;
    }
};
