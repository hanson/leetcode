/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	vector<TreeNode *> dfs(int x,int y){
		vector<TreeNode*> ret;
		if (x>y)
		{
			ret.push_back(NULL);
			return ret;
		}
		for (int i=x;i<=y;i++)
		{
			vector<TreeNode*> a=dfs(x,i-1),b=dfs(i+1,y);
			for (int j=0;j<a.size();j++)
				for (int k=0;k<b.size();k++)
				{
					TreeNode* tmp=new TreeNode(i);
					ret.push_back(tmp);
					tmp->left=a[j]; tmp->right=b[k];
				}
		}
		return ret;
	}
    vector<TreeNode *> generateTrees(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return dfs(1,n);
    }
};
