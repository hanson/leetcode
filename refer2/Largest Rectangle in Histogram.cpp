class Solution {
public:
    int largestRectangleArea(vector<int> &height) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        stack<pair<int,int> > s;
        height.push_back(0);
        int i,j,k,ans=0,n=height.size();
        for (j=0;j<n;j++)
        {
			k=j;
			while (!s.empty() && s.top().second>height[j])
					k=s.top().first,ans=max(ans,(j-k)*s.top().second),s.pop();
			if (!s.empty() && s.top().second==height[j]);
			else s.push(make_pair(k,height[j]));
		}
		return ans;
    }
};
