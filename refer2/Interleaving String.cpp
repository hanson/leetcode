class Solution {
public:
    bool isInterleave(string s1, string s2, string s3) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,m=s1.length(),n=s2.length();
        if (s3.length()!=m+n) return false;
        if (s1==s3 || s2==s3) return true;
        if (m==0 || n==0) return false;
        vector<vector<bool> > f(2);
        int u,v;
        bool flag=true;
        f[0].assign(n,false); f[1].assign(n,false);
        for (j=0;j<n && s2[j]==s3[j];j++) f[1][j]=true;
        for (i=0;i<m;i++)
        {
			u=i&1; v=1-u; flag=flag && (s1[i]==s3[i]);
			f[u][0]= (flag&&s2[0]==s3[i+1]) || (f[v][0]&&s1[i]==s3[i+1]);
			for (j=1;j<n;j++) f[u][j]=(f[u][j-1]&&s2[j]==s3[i+j+1]) || (f[v][j]&&s1[i]==s3[i+j+1]);
		}
		return f[u][n-1];
    }
};
