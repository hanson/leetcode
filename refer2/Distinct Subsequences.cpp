class Solution {
public:
    int numDistinct(string S, string T) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        S='@'+S; T='@'+T;
        int m=S.length(),n=T.length();
        //if (T=="") return 1;
        //if (S=="") return 0;
        int i,j,k,u,v;
        vector<vector<int> > f(2);
        f[0].resize(n); f[1].resize(n);
        for (f[0][0]=f[1][0]=1,i=1;i<m;i++)
        {
			u=i&1; v=1-u;
			for (j=1;j<n;j++)
				if (S[i]==T[j]) f[u][j]=f[v][j-1]+f[v][j];
				else f[u][j]=f[v][j];
		}
		return f[u][n-1];
    }
};
