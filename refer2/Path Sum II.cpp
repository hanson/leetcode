/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int> > pathSum(TreeNode *root, int sum) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k;
        vector<vector<int> > ret;
        if (root==NULL) return ret;
        vector<pair<TreeNode *,pair<int,int> > > q;
        q.push_back(make_pair(root,make_pair(root->val,-1)));
        for (i=0;i<q.size();i++)
        {
			TreeNode* x=q[i].first;
			int curSum=q[i].second.first;
			if (x->left==NULL && x->right==NULL && curSum==sum)
			{
				vector<int> now,sro;
				for (j=i;j>=0;j=q[j].second.second)
					now.push_back(q[j].first->val);
				sro.assign(now.rbegin(),now.rend());
				ret.push_back(sro);
			}
			if (x->left!=NULL) q.push_back(make_pair(x->left,make_pair(curSum+x->left->val,i)));
			if (x->right!=NULL) q.push_back(make_pair(x->right,make_pair(curSum+x->right->val,i)));
		}
    }
};
