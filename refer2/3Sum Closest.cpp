class Solution {
public:
    int threeSumClosest(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n=num.size(),i,j,k,x,y,ans=1<<30,ret;
        sort(num.begin(),num.end());
        for (i=0;i+2<n;i++)
        {
			for (j=i+1,k=n-1;j<k;)
			{
				if (ans>abs(num[j]+num[k]+num[i]-target))
					ret=num[j]+num[k]+num[i],ans=abs(ret-target);
				for (x=j;x+1<=k && num[x]==num[x+1];x++);
				for (y=k;y-1>=j && num[y]==num[y-1];y--);
				if (abs(num[i]+num[x+1]+num[k]-target)<abs(num[i]+num[j]+num[y-1]-target))
					j=x+1;
				else
					k=y-1;
			}
			while (i+2<n && num[i+1]==num[i]) i++;
		}
		return ret;
    }
};
