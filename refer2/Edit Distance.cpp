class Solution {
public:
    int minDistance(string word1, string word2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        word1=" "+word1; word2=" "+word2;
        int m=word1.size(),n=word2.size();
        vector<vector<int> > f(m);
        int i,j,k;
        for (i=0;i<m;i++) f[i].resize(n);
        for (j=0;j<n;j++) f[0][j]=j;
        for (i=1;i<m;i++) for (f[i][0]=i,j=1;j<n;j++)
        	f[i][j]=min( f[i-1][j-1]+(word1[i]!=word2[j]) , min(f[i-1][j],f[i][j-1])+1);
        return f[m-1][n-1];
    }
};
