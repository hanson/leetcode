/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
	vector<TreeLinkNode*> rightNode;
	void dfs(TreeLinkNode *x,int depth){
		if (x==NULL) return;
        dfs(x->right,depth+1);
        dfs(x->left,depth+1);
        if (depth+1>rightNode.size()) rightNode.resize(depth+1,NULL);
        x->next=rightNode[depth];
        rightNode[depth]=x;
	}
	
    void connect(TreeLinkNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        rightNode.clear();
        dfs(root,0);
        return;
    }
};
