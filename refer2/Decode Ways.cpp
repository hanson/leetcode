class Solution {
public:
	vector<int> f;
	int dfs(string s){
		if (s=="") return 1;
		if (f[s.length()]>=0) return f[s.length()];
        int a,tmp;
        a= (s[0]=='0'? 0:dfs(s.substr(1,s.length()-1)));
        if (s.length()>1)
        {
	        tmp=atoi(s.substr(0,2).c_str());
	        if (tmp>=10 && tmp<=26) a+=dfs(s.substr(2,s.length()-2));
		}
		return f[s.length()]=a;
	}
    int numDecodings(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        f.assign(s.length()+1,-1);
        return s==""?0:dfs(s);
    }
};
