/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	void dfs(TreeNode *x,TreeNode *&a,TreeNode *&b,TreeNode *&pre){
		if (x==NULL) return;
		int k=x->val;
		dfs(x->left,a,b,pre);
		if (pre && pre->val>k)
		{
			b=x;
			if (a==NULL) a=pre;
		}
		pre=x;
		dfs(x->right,a,b,pre);
	}
    void recoverTree(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        TreeNode *a=NULL,*b=NULL,*pre=NULL;
        dfs(root,a,b,pre);
        if (a && b) swap(a->val,b->val);
    }
};
