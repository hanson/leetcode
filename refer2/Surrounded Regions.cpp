class Solution {
public:
	int move[4][2];
	int m,n;
	vector<vector<bool> > v;
	void work(int i,int j,vector<vector<char> > &board){
		int k,x,y,xx,yy;
		if (board[i][j]=='X' || v[i][j]) return;
		queue<pair<int,int> > q;
		q.push(make_pair(i,j));
		v[i][j]=true;
		while (!q.empty())
		{
			x=q.front().first; y=q.front().second;
			q.pop();
			for (k=0;k<4;k++)
			{
				xx=x+move[k][0];
				yy=y+move[k][1];
				if (xx<0 || yy<0 || xx>=m || yy>=n) continue;
				if (board[xx][yy]=='X' || v[xx][yy]) continue;
				q.push(make_pair(xx,yy)); v[xx][yy]=true;
			}
		}
	}
    void solve(vector<vector<char> > &board) {
		// Start typing your C/C++ solution below
		// DO NOT write int main() function
		int i,j,k;
		move[0][0]=-1; move[0][1]=0;
		move[1][0]=0; move[1][1]=-1;
		move[2][0]=0; move[2][1]=1;
		move[3][0]=1; move[3][1]=0;
		m=board.size(); if (m>0) n=board[0].size(); else n=0;
		v.clear(); v.resize(m);
		for (i=0;i<m;i++) v[i].resize(n);
		for (i=0;i<m;i++) work(i,0,board),work(i,n-1,board);
		for (j=0;j<n;j++) work(0,j,board),work(m-1,j,board);
		for (i=0;i<m;i++) for (j=0;j<n;j++)
			if (board[i][j]=='O' && !v[i][j]) board[i][j]='X';
		return;
    }
};