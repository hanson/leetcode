/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode tmp(0),*x=&tmp,*y,*z;
        x->next=head; head=x;
        int i;
        for (y=x,i=0;i<n;i++,y=y->next);
        while (y->next!=NULL)
        {
			y=y->next; x=x->next;
		}
        x->next=x->next->next;
        return head->next;
    }
};
