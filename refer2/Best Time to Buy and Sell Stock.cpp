class Solution {
public:
    int maxProfit(vector<int> &prices) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,n=prices.size(),ret=0;
        if (n<=1) return 0;
        for (j=prices[0],i=1;i<n;i++)
        	if (prices[i]>j) ret=max(ret,prices[i]-j);
        	else j=prices[i];
		return ret;
    }
};
