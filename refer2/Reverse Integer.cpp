class Solution {
public:
    int reverse(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,sign;
        if (x==0) return 0;
        while (x%10==0) x/=10;
        k=x%10; x/=10;
        while (x!=0)
        	k=k*10+x%10,x/=10;
        return k;
    }
};
