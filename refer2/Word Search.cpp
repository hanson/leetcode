class Solution {
public:
    int m,n;
	bool dfs(int x,int y,int idx,vector<vector<char> > &board,vector<vector<bool> > &v,string &word,const int move[4][2]){
		if (idx+1==word.length()) return true;
		v[x][y]=true;
		for (int t=0;t<4;t++)
		{
			int xx=x+move[t][0],yy=y+move[t][1];
			if (xx<0 || yy<0 || xx>=m || yy>=n) continue;
			if (v[xx][yy]) continue;
			if (word[idx+1]==board[xx][yy])
				if (dfs(xx,yy,idx+1,board,v,word,move))
				{
					v[x][y]=false;
					return true;
				}
		}
		v[x][y]=false;
		return false;
	}
    bool exist(vector<vector<char> > &board, string word) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        const int move[4][2]={{-1,0},{0,-1},{0,1},{1,0}};
        m=board.size();
        vector<vector<bool> > v(m);
        if (m==0) return false;
        n=board[0].size();
        int i,j,k;
        for (i=0;i<m;i++) v[i].resize(n);
        bool ans=false;
        for (i=0;i<m;i++) for (j=0;j<n;j++)
        	if (board[i][j]==word[0]) ans|=dfs(i,j,0,board,v,word,move);
		return ans;
    }
};
