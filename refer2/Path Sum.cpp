/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool hasPathSum(TreeNode *root, int sum) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root==NULL) return false;
        queue<pair<TreeNode *,int> > q;
        q.push(make_pair(root,root->val));
        while (!q.empty())
        {
			TreeNode* x=q.front().first;
			int curSum=q.front().second;
			q.pop();
			if (x->left==NULL && x->right==NULL)
				if (curSum==sum) return true;
			if (x->left!=NULL) q.push(make_pair(x->left,curSum+x->left->val));
			if (x->right!=NULL) q.push(make_pair(x->right,curSum+x->right->val));
		}
		return false;
    }
};
