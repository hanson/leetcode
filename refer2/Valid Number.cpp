class Solution {
public:
	bool isInt(string s,bool prefix){
		if (s=="" || s=="+" || s=="-") return false;
		int i=0;
		if (s[0]=='+' || s[0]=='-')
			if (prefix) i++;
			else return false;
		for (;i<s.length();i++)
			if (!isdigit(s[i])) return false;
		return true;
	}
	bool isFloat(string s){
		if (s=="") return false;
		int k=s.find('.');
		if (k==string::npos) return isInt(s,true);
		bool l=isInt(s.substr(0,k),true),r=isInt(s.substr(k+1,string::npos),false);
		if (l && r || l && k==s.length()-1 || r && k==0 || r && k==1 && (s[0]=='+' || s[0]=='-')) return true;
		return false;
	}
    bool isNumber(const char *s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int x,y,z,i,j,k;
        for (j=0;s[j]!=0;j++);
        for (j--;j>=0 && s[j]==' ';j--);
        if (j<0) return false;
        for (i=0;s[i]==' ';i++);
        string t(s+i,j-i+1);
        
        if ((k=t.find('e'))!=string::npos)
		{
			bool l=isFloat(t.substr(0,k)),r=isInt(t.substr(k+1,string::npos),true);
			if (l && r) return true;
			else return false;
		}
		return isFloat(t);
    }
};
