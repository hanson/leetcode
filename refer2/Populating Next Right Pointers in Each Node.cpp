/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
    void connect(TreeLinkNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write in main() function
        if (root==NULL) return;
        queue<pair<TreeLinkNode *,int> > q;
        int depth=0,k;
        TreeLinkNode *right=NULL;
        q.push(make_pair(root,0));
        while (!q.empty())
        {
			TreeLinkNode *x=q.front().first;
			k=q.front().second;
			q.pop();
			if (x->right!=NULL) q.push(make_pair(x->right,k+1));
			if (x->left!=NULL) q.push(make_pair(x->left,k+1));
			if (k==depth) x->next=right;
			else depth=k, x->next=NULL;
			right=x;
		}
        return;
    }
};
