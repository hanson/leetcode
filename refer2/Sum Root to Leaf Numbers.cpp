class Solution {
public:
	int ans;
	void dfs(TreeNode *root,int x){
		if (root==NULL) return;
		if (root->left==NULL && root->right==NULL) ans+=x*10+root->val;
		dfs(root->left,x*10+root->val);
		dfs(root->right,x*10+root->val);
	}
    int sumNumbers(TreeNode *root) {
		// Start typing your C/C++ solution below
		// DO NOT write int main() function
		ans=0; dfs(root,0);
		return ans;
    }
};
