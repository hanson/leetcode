class Solution {
public:
    int maximalRectangle(vector<vector<char> > &matrix) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int m=matrix.size();
        if (m==0) return 0;
        int i,j,k,u,v,ans=0;
        for (i=0;i<m;i++) matrix[i].push_back('0');
        int n=matrix[0].size();
        vector<vector<int> > f(2);
        f[0].resize(n); f[1].resize(n);
        for (i=0;i<m;i++)
        {
			u=i&1; v=1-u;
			for (j=0;j<n;j++) f[u][j]= (matrix[i][j]=='0'? 0:1+f[v][j]);
			stack<pair<int,int> > s;
			for (j=0;j<n;j++)
			{
				k=j;
				while (!s.empty() && s.top().second>f[u][j])
					k=s.top().first,ans=max(ans,(j-k)*s.top().second),s.pop();
				if (!s.empty() && s.top().second==f[u][j]);
				else s.push(make_pair(k,f[u][j]));
			}
			
		}
		return ans;
    }
};
