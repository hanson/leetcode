/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	int dfs(TreeNode *x){
		if (x==NULL) return 1<<30;
		if (x->left==NULL && x->right==NULL) return 1;
		return 1+min(dfs(x->left),dfs(x->right));
	}
    int minDepth(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root==NULL) return 0;
		return dfs(root);
    }
};
