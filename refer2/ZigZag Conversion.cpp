class Solution {
public:
    string convert(string s, int nRows) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (nRows==1) return s;
        vector<string> a(nRows,"");
        int i,j,k,n=s.length();
        for (i=0;i<n;i++)
        {
			k=i%(2*nRows-2);
			if (k>=nRows) k=nRows-(k-nRows)-2;
			a[k]+=s[i];
		}
		string ret="";
		for (i=0;i<nRows;i++) ret+=a[i];
		return ret;
    }
};
