/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int k=0;
        ListNode *x=l1,*y=l2,tmp(0),*head=&tmp,*z=head,*p,*q;
        while (x!=NULL && y!=NULL)
        {
			k+=x->val+y->val;
			z->next=new ListNode(k%10);
			k/=10; z=z->next;
			x=x->next; y=y->next;
		}
		if (x!=NULL)
		{
			z->next=x; x->val+=k;
			while (x->next!=NULL && x->val>=10)
				x->next->val+=x->val/10,x->val%=10,x=x->next;
			if (x->val>=10)
				x->next=new ListNode(x->val/10),x->val%=10;
		}else if (y!=NULL)
		{
			z->next=y; y->val+=k;
			while (y->next!=NULL && y->val>=10)
				y->next->val+=y->val/10,y->val%=10,y=y->next;
			if (y->val>=10)
				y->next=new ListNode(y->val/10),y->val%=10;
		}else if (k>0)
			z->next=new ListNode(k);
		return head->next;
    }
};
