class Solution {
public:
    int trap(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        stack<pair<int,int> > stack;
        int i,j,k,ans,last;
        for (ans=0,i=0;i<n;i++)
        {
			for (last=0;!stack.empty() && stack.top().first<=A[i];stack.pop())
			{
				ans+=(stack.top().first-last)*(i-1-stack.top().second);
				last=stack.top().first;
			}
			if (!stack.empty()) ans+=(A[i]-last)*(i-1-stack.top().second);
			stack.push(make_pair(A[i],i));
		}
		return ans;
    }
};
