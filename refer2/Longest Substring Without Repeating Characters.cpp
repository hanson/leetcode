class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<bool> v(128,false);
        int i,j,k,n=s.length(),ans=0;
        for (i=j=0;j<n;j++)
        {
			if (!v[s[j]]) v[s[j]]=true;
			else
			{
				while (s[i]!=s[j]) v[s[i]]=false,i++;
				i++;
			}
			ans=max(ans,j-i+1);
		}
		return ans;
    }
};
