class Solution {
public:
    string addBinary(string a, string b) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int m=a.size(),n=b.size(),t,k,i;
        reverse(a.begin(),a.end());
        reverse(b.begin(),b.end());
        if (m>n) b.append(m-n,'0'),n=m;
		else a.append(n-m,'0'),m=n;
        string ret(m,'0');
        for (t=0,i=0;i<m;i++)
        {
			t=t+(int)(a[i]-'0')+(int)(b[i]-'0');
			ret[i]=(t&1)+'0';
			t>>=1;
		}
		if (t==1) ret.append("1");
		reverse(ret.begin(),ret.end());
		return ret;
    }
};
