class Solution {
public:
    bool isMatch(const char *s, const char *p) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function    
        if (*s==0 && *p==0) return true;
        if (*p!=0 && *(p+1)=='*')
        {
			if (isMatch(s,p+2)) return true;
			while (*s!=0 && (*s==*p || *p=='.'))
				if (isMatch(++s,p+2)) return true;
			return false;
		}
		if (*s==0 || *p==0) return false;
		return (*p=='.' || *s==*p) && isMatch(s+1,p+1);
    }
};
