class Solution {
public:
    vector<int> twoSum(vector<int> &numbers, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<pair<int,int> > a;
        vector<int> ret;
        int i,j,k,n=numbers.size();
        for (i=0;i<n;i++) a.push_back(make_pair(numbers[i],i));
        sort(a.begin(),a.end());
        for (i=0,j=n-1;i<j;)
        {
			if (a[i].first+a[j].first==target)
			{
				ret.push_back(min(a[i].second,a[j].second)+1);
				ret.push_back(max(a[i].second,a[j].second)+1);
				return ret;
			}
			if (a[i].first+a[j].first>target) j--;
			else i++;
		}
    }
};
