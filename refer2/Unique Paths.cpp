class Solution {
public:
    int uniquePaths(int m, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > f(m);
        if (f.size()==0) return 0;
        int i,j,k;
        for (i=0;i<m;i++) f[i].resize(n);
    	for (i=0;i<m;i++) f[i][0]=1;
    	for (j=0;j<n;j++) f[0][j]=1;
    	for (i=1;i<m;i++) for (j=1;j<n;j++)
    		f[i][j]+=f[i-1][j]+f[i][j-1];
    	return f[m-1][n-1];
    }
};
