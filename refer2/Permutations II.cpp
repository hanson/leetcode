class Solution {
public:
	vector<vector<int> > ans;
	void dfs(int x,vector<int> &num,vector<int> &now,vector<bool> &v){
		if (x==num.size())
		{
			ans.push_back(now);
			return;
		}
		int last=1<<30;
		for (int i=0;i<num.size();i++) if (!v[i] && num[i]!=last)
		{
			last=num[i]; v[i]=true;
			now.push_back(num[i]);
			dfs(x+1,num,now,v);
			now.pop_back(); v[i]=false;
		}
	}
    vector<vector<int> > permuteUnique(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        sort(num.begin(),num.end());
        ans.clear();
        vector<int> now;
        vector<bool> v(num.size(),false);
        dfs(0,num,now,v);
        return ans;
    }
};
