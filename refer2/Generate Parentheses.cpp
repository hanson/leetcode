class Solution {
public:
	vector<string> ans;
	void dfs(int x,string &now,int delta){
		if (x==0)
		{
			ans.push_back(now+string(delta,')'));
			return;
		}
		
		now=now+"(";
		dfs(x-1,now,delta+1);
		now.pop_back();
		
		if (delta>0)
		{
			now=now+")";
			dfs(x,now,delta-1);
			now.pop_back();
		}
	}
    vector<string> generateParenthesis(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ans.clear();
        string now="";
        dfs(n,now,0);
        return ans;
    }
};
