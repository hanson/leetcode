class Solution {
public:
    int searchInsert(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int l=0,r=n-1,mid;
        while (l<=r)
        {
			mid=(l+r)>>1;
			if (target<=A[mid]) r=mid-1;
			else l=mid+1;
		}
		return l;
    }
};
