class Solution {
public:
	int m;
	int a[2][2],c[2][2];
	void multiply(int a[2][2],int b[2][2]){
		int c[2][2];
		for (int i=0;i<2;i++) for (int j=0;j<2;j++)
		{
			c[i][j]=0;
			for (int k=0;k<2;k++) c[i][j]+=a[i][k]*b[k][j];
		}
		for (int i=0;i<2;i++) for (int j=0;j<2;j++) a[i][j]=c[i][j];
	}
			
	void fb(int x){
		if (x==0) return;
		fb(x>>1);
		multiply(a,a);
		if (x & 1) multiply(a,c);
	}
		
    int climbStairs(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        m=2;
		a[0][0]=a[1][1]=1; a[0][1]=a[1][0]=0;
		c[0][0]=0; c[0][1]=c[1][0]=c[1][1]=1;
        fb(n);
        return a[1][1];
    }
};
