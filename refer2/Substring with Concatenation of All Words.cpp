class Solution {
public:
    vector<int> findSubstring(string S, vector<string> &L) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> ret;
        unordered_map<string,pair<int,int> > map;
        unordered_map<string,pair<int,int> >::iterator it;
    	int start,i,j,k,left,num,len=L[0].length();
    	
    	for (i=0;i<L.size();i++)
    		if (map.find(L[i])!=map.end())
    			map[L[i]].first++;
    		else
    			map[L[i]].first=1;
    	for (start=0;start<len;start++)
    	{
			for (it=map.begin();it!=map.end();it++) it->second.second=0;
    		for (left=start,num=0,i=start;i+len-1<S.length();i+=len)
    		{
				string s=S.substr(i,len);
				it=map.find(s);
				if (it==map.end())
				{
					while (left<i)
					{
						num--;
						map[S.substr(left,len)].second--;
						left+=len;
					}
					left+=len;
					continue;
				}else if (it->second.second==it->second.first)
				{
					while (1)
					{
						num--;
						string tmp=S.substr(left,len);
						map[tmp].second--;
						left+=len;
						if (tmp==s) break;
					}
				}
				it->second.second++; num++;
				if (num==L.size()) ret.push_back(left);
			}
		}
		return ret;
    }
};
