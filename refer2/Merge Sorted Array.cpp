class Solution {
public:
    void merge(int A[], int m, int B[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (n==0) return;
        int i,j,k,idx;
        for (idx=m+n-1,i=m-1,j=n-1;i>=0 && j>=0;)
        	if (A[i]>B[j]) A[idx--]=A[i--];
        	else A[idx--]=B[j--];
        while (i>=0) A[idx--]=A[i--];
        while (j>=0) A[idx--]=B[j--];
        return;
    }
};
