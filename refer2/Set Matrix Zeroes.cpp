class Solution {
public:
    void setZeroes(vector<vector<int> > &matrix) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int m=matrix.size();
        if (m==0) return;
        int n=matrix[0].size();
        int i,j,k,t;
        bool row=false,col=false;
        for (i=0;i<m;i++) if (matrix[i][0]==0) row=true;
        for (j=0;j<n;j++) if (matrix[0][j]==0) col=true;
        
        for (i=1;i<m;i++) for (j=1;j<n;j++)
        	if (matrix[i][j]==0)
				matrix[0][j]=matrix[i][0]=0;
		for (i=1;i<m;i++) for (j=1;j<n;j++)
        	if (matrix[i][0]==0 || matrix[0][j]==0)
        		matrix[i][j]=0;
        
        if (row) for (i=0;i<m;i++) matrix[i][0]=0;
		if (col) for (j=0;j<n;j++) matrix[0][j]=0;
    }
};
