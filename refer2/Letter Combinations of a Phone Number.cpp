class Solution {
public:
	vector<string> ans;
	void dfs(int x,string &digits,string &now,string a[]){
		if (x==digits.length())
		{
			ans.push_back(now);
			return;
		}
		for (int i=0;i<a[digits[x]-'2'].length();i++)
		{
			now+=a[digits[x]-'2'][i];
			dfs(x+1,digits,now,a);
			now.pop_back();
		}
	}
    vector<string> letterCombinations(string digits) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        string a[8]={"abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
        string now="";
        ans.clear(); dfs(0,digits,now,a);
        return ans;
    }
};
