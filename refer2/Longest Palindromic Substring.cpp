class Solution {
public:
    string longestPalindrome(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,rm,n,ans;
        string t;
        for (t=s,s="#",i=0;i<t.length();i++)
        	s+=string(1,t[i])+"#";
        n=s.length(); rm=0; k=0; ans=0;
        vector<int> f(n);
        for (f[0]=0,i=1;i<n;i++)
        {
			if (i<rm)
				if (i+f[k+k-i]<rm)
				{
					f[i]=f[k+k-i];
					continue;
				}else
					j=min(i+f[k+k-i],rm)-i;
			else
				j=0;
			while (i-j-1>=0 && i+j+1<n && s[i-j-1]==s[i+j+1]) j++;
			f[i]=j; rm=i+j; k=i;
			if (f[ans]<f[i]) ans=i;
		}
        for (t="",i=ans-f[ans];i<=ans+f[ans];i++)
        	if (s[i]!='#') t+=string(1,s[i]);
        return t;
    }
};
