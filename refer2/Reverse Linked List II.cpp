/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseBetween(ListNode *head, int m, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (m==n) return head;
        ListNode *x,*y,*z,*yy;
        int i,j,k;
        if (m>1)
        {
        	for (x=head,i=1;i<m-1;i++) x=x->next;
        	y=x->next,z=x;
		}else
		{
			y=head;
			z=NULL;
		}
        for (yy=y,i=m;i<=n;i++)
        {
			ListNode *tmp=y->next;
			y->next=z;
			z=y; y=tmp;
		}
		if (m>1) x->next=z;
		else head=z;
		yy->next=y;
        return head;
    }
};
