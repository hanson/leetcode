class Solution {
public:
    vector<vector<int> > generateMatrix(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > ret(n);
		const int move[4][2]={{0,1},{1,0},{0,-1},{-1,0}};
		int i,j,k,len,dir,t,x,y;
		for (i=0;i<n;i++) ret[i].resize(n);
		for (t=0,j=0;j<n-1;j++) ret[0][j]=++t;
		for (dir=1,i=0,j=n-1,k=1;t+1<n*n;k+=(++dir&1))
				for (dir%=4,x=0;x<n-k;x++,i+=move[dir][0],j+=move[dir][1])
					ret[i][j]=++t;
		if (n>0) ret[i][j]=++t;
		return ret;
    }
};
