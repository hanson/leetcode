/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (head==NULL) return head;
        ListNode *x,*y,*z,t(0);
        t.next=head; head=&t;
        for (x=head;x->next!=NULL;)
        {
			for (y=x->next;y!=NULL && y->val==x->next->val;y=y->next);
			if (x->next->next!=y) x->next=y;
			else x=x->next;
		}
		return head->next;
    }
};
