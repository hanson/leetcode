/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	int ans;
	int dfs(TreeNode *x){
		if (x==NULL) return 0;
		int left=dfs(x->left), right=dfs(x->right);
		ans=max(ans, max( max(left+right,0),max(left,right) )+x->val );
		return max(0,max(left,right))+x->val;
	}
		
    int maxPathSum(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ans=-(1<<30);
        dfs(root);
        return ans;
    }
};
