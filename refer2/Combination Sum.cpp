class Solution {
public:
	vector<vector<int> > ans;
	void dfs(int target,int idx,vector<int> &a,vector<int> &now,vector<vector<bool> > &f){
		if (target==0)
		{
			ans.push_back(vector<int>(now.rbegin(),now.rend()));
			return;
		}
		if (idx<0 || target<0 || !f[target][idx]) return;
		dfs(target,idx-1,a,now,f);
		now.push_back(a[idx]);
		dfs(target-a[idx],idx,a,now,f);
		now.pop_back();
	}
    vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> a(candidates),now;
        int n=a.size(),i,j,k;
        vector<vector<bool> > f(target+1);
        f[0].assign(n,true);
        for (i=1;i<=target;i++) f[i].resize(n);
        sort(a.begin(),a.end());
        for (i=1;i<=target;i++) for (f[i][0]=(i%a[0]==0),j=1;j<n;j++)
        	if (i>=a[j]) f[i][j]=f[i][j-1]||f[i-a[j]][j];
        	else f[i][j]=f[i][j-1];
        ans.clear(); now.clear(); dfs(target,n-1,a,now,f);
        return ans;
    }
};
