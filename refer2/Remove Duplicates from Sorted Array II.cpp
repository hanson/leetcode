class Solution {
public:
    int removeDuplicates(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,idx;
        for (idx=0,i=0;i<n;i=j)
        {
			A[idx++]=A[i++];
			if (i<n && A[i]==A[i-1]) A[idx++]=A[i++];
			for (j=i;i<n && j<n && A[i-1]==A[j];j++);
		}
		return idx;
    }
};
