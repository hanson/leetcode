class Solution {
public:
    bool canJump(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j;
        for (j=0,i=0;i<n && i<=j;i++)
        	j=max(j,A[i]+i);
        return i==n;
    }
};
