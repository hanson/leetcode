/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
	bool smaller(Interval &i,Interval &j){
		return i.start<j.start;
	}
		
    vector<Interval> merge(vector<Interval> &intervals) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<Interval> ret;
        int n=intervals.size();
        sort(intervals.begin(),intervals.end(),smaller);
        int i,j,k;
        for (i=0;i<n;i=j+1)
        {
			for (k=intervals[i].end,j=i;j+1<n && intervals[j+1].start<=k;j++)
				k=max(k,intervals[j+1].end);
			ret.push_back(Interval(intervals[i].start,k));
		}
        return ret;
    }
};
