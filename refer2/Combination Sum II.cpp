class Solution {
public:
	vector<vector<int> > ans;
	void dfs(int target,int idx,vector<int> &a,vector<int> &now,vector<vector<bool> > &f){
		if (target==0)
		{
			ans.push_back(vector<int>(now.rbegin(),now.rend()));
			return;
		}
		if (idx<0 || target<0 || !f[target][idx]) return;
		int i,j;
		for (i=idx;i-1>=0 && a[i-1]==a[idx];i--);
		dfs(target,i-1,a,now,f);
		for (j=idx;j>=i;j--)
		{
			now.push_back(a[idx]);
			dfs(target-(idx-j+1)*a[idx],i-1,a,now,f);
		}
		for (j=idx;j>=i;j--) now.pop_back();
	}
    vector<vector<int> > combinationSum2(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> a(num),now;
        int n=a.size(),i,j,k;
        vector<vector<bool> > f(target+1);
        f[0].assign(n,true);
        for (i=1;i<=target;i++) f[i].resize(n);
        sort(a.begin(),a.end());
        for (i=1;i<=target;i++) for (f[i][0]=(i%a[0]==0),j=1;j<n;j++)
        	if (i>=a[j]) f[i][j]=f[i][j-1]||f[i-a[j]][j-1];
        	else f[i][j]=f[i][j-1];
        ans.clear(); now.clear(); dfs(target,n-1,a,now,f);
        return ans;
    }
};
