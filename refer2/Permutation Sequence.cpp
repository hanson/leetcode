class Solution {
public:
    string getPermutation(int n, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        string s;
        vector<int> a(n+1);
        vector<bool> v(n+1);
        int i,j,x,y,z;
        for (a[0]=1,i=1;i<=n;i++) a[i]=a[i-1]*i;
        for (i=1;i<=n;i++)
        {
			x=(k-1)/a[n-i]+1;
			for (j=0;x>0;x--) for (j++;v[j];j++);
			v[j]=true; s+=(char)('0'+j);
			k=(k-1)%a[n-i]+1;
		}
		return s;
    }
};
