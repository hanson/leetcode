class Solution {
public:
    bool isPalindrome(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (x<0) return false;
        int k;
        for (k=1;x/k>=10;k*=10);
        while (x>=10)
        {
			if (x/k!=x%10) return false;
			x-=x/k*k; k/=100; x/=10;
		}
		return true;
    }
};
