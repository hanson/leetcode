class Solution {
public:
	vector<vector<int> > ans;
	void dfs(int x,vector<int> &S,vector<int> &now){
		if (x>=S.size())
		{
			ans.push_back(now);
			return;
		}
		int i,j,k;
		for (j=x;j+1<S.size() && S[x]==S[j+1];j++);
		dfs(j+1,S,now);
		for (i=x;i<=j;i++)
		{
			now.push_back(S[i]);
			dfs(j+1,S,now);
		}
		for (i=x;i<=j;i++) now.pop_back();
	}	
		
		
    vector<vector<int> > subsetsWithDup(vector<int> &S) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,n=S.size();
        sort(S.begin(),S.end());
        vector<int> now;
        ans.clear(); dfs(0,S,now);
        return ans;
    }
};
