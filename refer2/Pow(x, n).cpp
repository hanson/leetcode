class Solution {
public:
    double powL(double x,long long n){
		if (n<0) return 1/powL(x,-n);
        if (n==0) return 1;
        double tmp=powL(x,n/2);
        tmp*=tmp;
        if (n & 1) tmp*=x;
        return tmp;
	}
    double pow(double x, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
		return powL(x,n);
    }
};
