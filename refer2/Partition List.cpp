/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *partition(ListNode *head, int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode t(0),*r,*y,*z;
        t.next=head; head=&t;
        for (r=head;r->next!=NULL && r->next->val<x;r=r->next);
        if (r->next==NULL) return head->next;
        for (z=y=r->next;y->next!=NULL;)
        	if (y->next->val<x)
        	{
				r->next=y->next; r=y->next;
				y->next=y->next->next;
			}else
				y=y->next;
		r->next=z;
		return head->next;
    }
};
