class Solution {
public:
	string convert(int x,char one,char five,char ten){
		if (x<=3) return string(x,one);
		if (x==4) return string(1,one)+string(1,five);
		if (x<=8) return string(1,five)+string(x-5,one);
		if (x==9) return string(1,one)+string(1,ten);
	}
    string intToRoman(int num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        string ret="";
		ret=convert(num%10,'I','V','X')+ret; num/=10;
		ret=convert(num%10,'X','L','C')+ret; num/=10;
		ret=convert(num%10,'C','D','M')+ret; num/=10;
		ret=convert(num%10,'M',' ',' ')+ret; num/=10;
		return ret;
    }
};
