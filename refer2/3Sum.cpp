class Solution {
public:
	void addAnswer(vector<vector<int> > &a,int x,int y,int z){
		vector<int> tmp;
		tmp.push_back(x); tmp.push_back(y); tmp.push_back(z);
		a.push_back(tmp);
	}
    vector<vector<int> > threeSum(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n=num.size(),i,j,k,x,y;
        vector<vector<int> > ret;
        sort(num.begin(),num.end());
        for (i=0;i+2<n;i++)
        {
			for (j=i+1,k=n-1;j<k;)
			{
				if (num[j]+num[k]+num[i]==0) addAnswer(ret,num[i],num[j],num[k]);
				for (x=j;x+1<=k && num[x]==num[x+1];x++);
				for (y=k;y-1>=j && num[y]==num[y-1];y--);
				if (abs(num[i]+num[x+1]+num[k])<abs(num[i]+num[j]+num[y-1]))
					j=x+1;
				else
					k=y-1;
			}
			while (i+2<n && num[i+1]==num[i]) i++;
		}
		return ret;
    }
};
