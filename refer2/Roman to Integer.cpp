class Solution {
public:
    int romanToInt(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int n=s.length(),i,j,ans;
        vector<int> a(n);
        for (i=0;i<n;i++)
        	switch (s[i])
        	{
				case 'I': a[i]=1; break;
				case 'V': a[i]=5; break;
				case 'X': a[i]=10; break;
				case 'L': a[i]=50; break;
				case 'C': a[i]=100; break;
				case 'D': a[i]=500; break;
				case 'M': a[i]=1000; break;
			}
		for (ans=j=0,i=n-1;i>=0;i--)
			if (a[i]>=j) ans+=a[i],j=a[i];
			else ans-=a[i];
		return ans;
    }
};
