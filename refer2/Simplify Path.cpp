class Solution {
public:
    string simplifyPath(string path) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<string> ret;
        string ss;
        int i,j,k,n=path.length();
        for (i=0;i<n;)
        {
			for (j=i+1;j<n && path[j]=='/';j++);
			if (j>=n) break;
			for (i=j+1;i<n && path[i]!='/';i++);
			string s=path.substr(j,i-j);
			if (s==".") continue;
			if (s=="..") {if (ret.size()>0) ret.pop_back();}
			else ret.push_back("/"+s);
		}
		if (ret.size()==0) ret.push_back("/");
		for (i=0;i<ret.size();i++) ss+=ret[i];
		return ss;
    }
};
