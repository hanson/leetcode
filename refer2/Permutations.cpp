class Solution {
public:
	vector<vector<int> > ans;
	void dfs(int x,vector<int> now){
		if (x==now.size())
		{
			ans.push_back(now);
			return;
		}
		for (int i=x;i<now.size();i++)
		{
			swap(now[x],now[i]);
			dfs(x+1,now);
			swap(now[x],now[i]);
		}
	}
    vector<vector<int> > permute(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ans.clear();
        dfs(0,num);
        return ans;
    }
};
