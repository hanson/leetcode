/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	TreeNode* dfs(int x,int y,int l,int r,vector<int> &inorder, vector<int> &preorder){
		if (x>y) return NULL;
		int k;
		for (k=x;k<=y && inorder[k]!=preorder[l];k++);
		TreeNode* node=new TreeNode(preorder[l]);
		node->left=dfs(x,k-1,l+1,l+k-x,inorder,preorder);
		node->right=dfs(k+1,y,l+k-x+1,r,inorder,preorder);
		return node;
	}
    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return dfs(0,inorder.size()-1,0,preorder.size()-1,inorder,preorder);
    }
};
