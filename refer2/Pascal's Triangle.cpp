class Solution {
public:
    vector<vector<int> > generate(int numRows) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > ret;
        if (numRows==0) return ret;
        vector<int> a;
        a.push_back(1);
        ret.push_back(a);
        for (int i=1;i<numRows;i++)
        {
			a.assign(1,1);
			for (int j=1;j<i;j++)
				a.push_back(ret[i-1][j-1]+ret[i-1][j]);
			a.push_back(ret[i-1][i-1]);
			ret.push_back(a);
		}
		return ret;
    }
};
