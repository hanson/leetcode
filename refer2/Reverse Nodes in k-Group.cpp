/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseKGroup(ListNode *head, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (k==1) return head;
        ListNode tmp(0),*x=&tmp,*y,*z,*p,*q;
        int i,j;
        x->next=head; head=x;
        while (1)
        {
			for (y=x,i=0;i<k && y;i++,y=y->next);
			if (y==NULL) break;
			for (z=x->next->next,p=x->next;p!=y;)
				q=z->next, z->next=p, p=z, z=q;
			x->next->next=z; p=x->next; x->next=y; x=p;
		}
		return head->next;
    }
};
