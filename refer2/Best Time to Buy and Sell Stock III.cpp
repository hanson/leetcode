class Solution {
public:
    int maxProfit(vector<int> &prices) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,n=prices.size(),ret=0;
        if (n<=1) return 0;
        int f=0,g=-(1<<30);
        for (j=prices[0],i=1;i<n;i++)
        {
			ret=max(ret,prices[i]+g);
			g=max(g,f-prices[i]);
			f=max(f,prices[i]-j);
			ret=max(ret,f);
			if (prices[i]<j) j=prices[i];
		}
		return ret;
    }
};
