class Solution {
public:
    bool isPalindrome(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k;
        string t="";
        for (i=0;i<s.length();i++)
        	if (isupper(s[i])) t+=tolower(s[i]);
        	else if (islower(s[i]) || isdigit(s[i])) t+=s[i];
        string r=t;
        r.replace(r.begin(),r.end(),r.rbegin(),r.rend());
        return t==r;
    }
};
