class Solution {
public:
	vector<string> dfs(string s,int k){
		vector<string> ret;
		if (s=="" && k==0)
		{
			ret.push_back("");
			return ret;
		}
		if (s=="" || k==0) return ret;
		for (int i=0;i<s.length();i++)
		{
			if (i>0 && s[0]=='0') break;
			string t=s.substr(0,i+1);
			int tmp=atoi(t.c_str());
			if (tmp>255) return ret;
			vector<string> a=dfs(s.substr(i+1,s.length()-i),k-1);
			for (int j=0;j<a.size();j++)
				if (a[j]=="") ret.push_back(t);
				else ret.push_back(t+"."+a[j]);
		}
		return ret;
	}
    vector<string> restoreIpAddresses(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
		return dfs(s,4);
    }
};
