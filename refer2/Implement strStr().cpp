class Solution {
public:
    char *strStr(char *haystack, char *needle) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        char *s=haystack,*t=needle;
        if (s==NULL || t==NULL) return NULL;
        int m=strlen(s),n=strlen(t),i,j,k;
        if (n==0) return s;
        vector<int> next(n);
        for (next[0]=0,k=-1,i=1;i<n;i++)
        {
			while (k>=0 && t[i]!=t[k+1]) k=next[k]-1;
			if (t[i]==t[k+1]) ++k;
			next[i]=k+1;
		}
		for (k=-1,i=0;i<m;i++)
		{
			while (k>=0 && s[i]!=t[k+1]) k=next[k]-1;
			if (s[i]==t[k+1]) ++k;
			if (k+1==n) return s+i-n+1;
		}
		return NULL;
    }
};
