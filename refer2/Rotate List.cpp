/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *rotateRight(ListNode *head, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (head==NULL) return NULL;
        ListNode *x,*y,*z;
        int cnt;
        for (x=head,cnt=0;x!=NULL;x=x->next) cnt++;
        k%=cnt;
        if (k==0) return head;
        for (x=head,++k;k<cnt;x=x->next,k++);
        y=x->next; x->next=NULL; x=y;
        for (;x->next!=NULL;x=x->next);
        x->next=head;
        return y;
    }
};
