class Solution {
public:
	int ans;
	void dfs(int x,int n,int left,int right,int down){
		if (x==n)
		{
			ans++;
			return;
		}
		for (int i=0;i<n;i++)
		{
			if ((left | right | down) & 1<<i) continue;
			dfs(x+1,n,(left|1<<i)<<1,(right|1<<i)>>1,(down|1<<i));
		}
	}
    int totalNQueens(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ans=0;
        dfs(0,n,0,0,0);
        return ans;
    }
};
