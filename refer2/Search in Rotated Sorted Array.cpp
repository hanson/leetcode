class Solution {
public:
    int search(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int l,r,mid,x,y,z;
        for (l=0,r=n-1;l<=r;)
        {
    		mid=(l+r)>>1;
			if (A[mid]<A[0]) r=mid-1;
			else l=mid+1;
		}
		if (target>=A[0]) l=0;
		else l=r+1,r=n-1;
		while (l<=r)
        {
			mid=(l+r)>>1;
			if (A[mid]<target) l=mid+1;
			else r=mid-1;
		}
		if (l>=n) return -1;
		if (A[l]!=target) return -1;
		return l;
    }
};
