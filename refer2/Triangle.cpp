class Solution {
public:
    int minimumTotal(vector<vector<int> > &triangle) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<vector<int> > f(2);
        int i,j,k,n=triangle.size(),ret,u,v;
        if (n==1) return triangle[0][0];
        f[1].push_back(triangle[0][0]);
        for (i=1;i<n;i++)
        {
			u=i & 1; v=1-u;
			f[v].resize(i+1);
			f[v][0]=f[u][0]+triangle[i][0]; f[v][i]=f[u][i-1]+triangle[i][i];
			for (j=1;j<i;j++) f[v][j]=min(f[u][j-1],f[u][j])+triangle[i][j];
		}
		for (ret=1<<30,j=0;j<n;j++) ret=min(ret,f[v][j]);
		return ret;
    }
};
