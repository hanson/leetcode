class Solution {
public:
    int longestConsecutive(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k,ans=0;
        int n=num.size();
        unordered_set<int> hset;
        unordered_set<int>::iterator it;
        for(i=0;i<n;i++) hset.insert(num[i]);
        for(i=0;i<n;i++){
            it=hset.find(num[i]);
            k=1;
	        if(it!=hset.end()){
                for (j=num[i]+1;(it=hset.find(j))!=hset.end();j++)
                    k++,hset.erase(it);
                for (j=num[i]-1;(it=hset.find(j))!=hset.end();j--)
                    k++,hset.erase(it);
                if (ans<k) ans=k;
            }
		}
		return ans;	
    }
};
