/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	bool flag;
	int dfs(TreeNode *x){
		if (x==NULL) return 0;
		int left=dfs(x->left),right=dfs(x->right);
		if (abs(left-right)>1) flag=false;
		return max(left,right)+1;
	}
    bool isBalanced(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        flag=true;
        dfs(root);
        return flag;
    }
};
