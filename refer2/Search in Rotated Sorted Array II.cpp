class Solution {
public:
    int findMax(int x,int y,int A[]){
		if (x==y) return x;
    	if (x+1==y) return A[x]>A[y]?x:y;
		int mid=(x+y)>>1;
		if (A[x]<A[mid]) return findMax(mid,y,A);
		if (A[x]>A[mid]) return findMax(x,mid,A);
		int l=findMax(x,mid,A),r=findMax(mid,y,A);
		return A[r]>=A[l]? r:l;
	}
    bool search(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int l,r,mid,x,y,z;
        r=findMax(0,n-1,A);
		if (target>=A[0]) l=0;
		else l=r+1,r=n-1;
		while (l<=r)
        {
			mid=(l+r)>>1;
			if (A[mid]<target) l=mid+1;
			else r=mid-1;
		}
		if (l>=n) return false;
		if (A[l]!=target) return false;
		return true;
    }
};
