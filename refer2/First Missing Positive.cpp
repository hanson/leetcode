class Solution {
public:
    int firstMissingPositive(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int i,j,k;
        for (i=0;i<n;i++)
        	if (A[i]<=0) A[i]=n+2;
        for (i=0;i<n;i++)
        {
			k=abs(A[i]);
			if (k<=n && A[k-1]>0) A[k-1]=-A[k-1];
		}
		for (i=0;i<n && A[i]<0;i++);
		return i+1;
    }
};
