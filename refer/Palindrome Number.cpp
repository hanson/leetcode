class Solution {
public:
    bool isPalindrome(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        if (x < 0) return false;
        int base = 1;
        while ((long long)base * 10 <= x) base *= 10;
        for (int i = base, j = 1; i > j; i /= 10, j *= 10) {
            int a = x / i % 10, b = x / j % 10;
            if (a != b) return false;
        }
        return true;
    }
};