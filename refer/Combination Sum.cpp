class Solution {
public:
    vector<vector<int> > ans;
    vector<int> a, q;
    int n;
    
    void DFS(int step, int remain) {
        if (remain == 0) {
            ans.push_back(q);
            return;
        }
        
        for (int i = step; i < n; ++i)
            if (a[i] <= remain) {
                q.push_back(a[i]);
                DFS(i, remain - a[i]);
                q.pop_back();
            }
    }
    
    vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        sort(candidates.begin(), candidates.end());
        unique(candidates.begin(), candidates.end());
    
        ans.clear();
        a = candidates; n = a.size();
        DFS(0, target);
     
        return ans;
    }
};