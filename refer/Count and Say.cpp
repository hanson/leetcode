class Solution {
public:
    string cal(int x) {
        string ret = "";
        while (x) {
            char c = x % 10 + '0';
            ret = c + ret;
            x /= 10;
        }
        return ret;
    }
    
    string countAndSay(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
     
        string s = "1";
        for (int i = 1; i < n; ++i) {
            string t = "";
            int len = s.size();
            for (int k, j = 0; j < len; j = k) {
                k = j;
                while (k < len && s[j] == s[k]) ++k;
                t += cal(k - j) + s[j];
            }
            s = t;
        }
        
        return s;
    }
};