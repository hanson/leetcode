class Solution {
public:
    vector<vector<int> > threeSum(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        sort(num.begin(), num.end());
        
        int n = num.size();
        
        vector< vector<int> > ret;
        for (int i = 0; i < n; ++i) {
            if (i && num[i - 1] == num[i]) continue;
            int j = i + 1, k = n - 1;
            while (j < k) {
                if (num[i] + num[j] + num[k] < 0) ++j;
                else if (num[i] + num[j] + num[k] > 0) --k;
                else {
                    vector<int> triple(3);
                    triple[0] = num[i]; triple[1] = num[j]; triple[2] = num[k];
                    ret.push_back(triple);
                    
                    for (++j; j < k && num[j] == num[j - 1]; ++j);
                    --k;
                }
            }
        }
        
        return ret;
    }
};