class Solution {
public:
    int jump(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function

        int *f = new int[n], *q = new int[n];
        q[0] = 0; f[0] = 0;
        for (int h = 0, t = 1, i = 1; i < n; ++i) {
            while (h < t && q[h] + A[q[h]] < i) ++h;
            if (h < t) f[i] = f[q[h]] + 1;
            if (h == t || i + A[i] > q[t - 1] + A[q[t - 1]]) q[t++] = i;
        }
        
        return f[n - 1];
    }
};