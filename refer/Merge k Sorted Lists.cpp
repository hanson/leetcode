/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    void moveup(int i, vector<ListNode *> &heap) {
        ListNode *key = heap[i];
        while (i > 1 && key->val < heap[i / 2]->val) {
            heap[i] = heap[i / 2];
            i /= 2;
        }
        heap[i] = key;
    }
    
    void movedown(int i, vector<ListNode *> &heap) {
        ListNode *key = heap[i];
        while (i * 2 < heap.size()) {
            int j = i * 2;
            if (j + 1 < heap.size() && heap[j + 1]->val < heap[j]->val) ++j;
            if (key->val <= heap[j]->val) break;
            heap[i] = heap[j]; i = j;
        }
        heap[i] = key;
    }
    
    ListNode *mergeKLists(vector<ListNode *> &lists) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        int k = lists.size();
        if (k == 0) return NULL;
        
        vector<ListNode *> heap;
        heap.push_back(NULL);
        for (int i = 0; i < k; ++i)
            if (lists[i] != NULL) heap.push_back(lists[i]);
        k = heap.size() - 1;
        if (k == 1) return heap[1];
                
        for (int i = k / 2; i >= 1; --i) movedown(i, heap);
        
        ListNode *head = NULL, *last = NULL;
        while (k > 1) {
            ListNode *cur = heap[1];
            if (head == NULL) head = cur;
            else last->next = cur;
            last = cur;
            
            heap[1] = heap[1]->next;
            if (heap[1] == NULL) {
                heap[1] = heap[k];
                heap.pop_back();
                --k;
            }
            if (k) movedown(1, heap);
        }
        
        if (k == 1) last->next = heap[1];
        
        return head;
    }
};