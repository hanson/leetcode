class Solution {
public:
    vector<int> twoSum(vector<int> &numbers, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function

        int n = numbers.size();
        vector<pair<int, int> > a(n);
        for (int i = 0; i < n; ++i) a[i] = make_pair(numbers[i], i + 1);
        
        sort(a.begin(), a.end());
        
        for (int i = 0, j = n - 1; i < j; )
            if (a[i].first + a[j].first == target) {
                vector<int> ret(2); ret[0] = a[i].second; ret[1] = a[j].second;
                sort(ret.begin(), ret.end());
                return ret;
            }
            else if (a[i].first + a[j].first < target) ++i;
            else --j;
    }
};