class Solution {
public:
    vector<vector<int> > fourSum(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
     
        sort(num.begin(), num.end());
        
        vector< vector<int> > ret;
        int n = num.size();
        for (int i = 0; i < n; ++i) {
            if (i && num[i - 1] == num[i]) continue;
            for (int j = i + 1; j < n; ++j) {
                if (j > i + 1 && num[j - 1] == num[j]) continue;
                int x = j + 1, y = n - 1;
                while (x < y) {
                    if (num[i] + num[j] + num[x] + num[y] < target) ++x;
                    else if (num[i] + num[j] + num[x] + num[y] > target) --y;
                    else {
                        vector<int> quad(4);
                        quad[0] = num[i]; quad[1] = num[j]; quad[2] = num[x]; quad[3] = num[y];
                        ret.push_back(quad);
                        for (++x; x < y && num[x - 1] == num[x]; ++x);
                        --y;
                    }
                }
            }
        }
        
        return ret;
    }
};