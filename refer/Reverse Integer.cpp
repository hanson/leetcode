class Solution {
public:
    int reverse(int x) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        bool neg = x < 0;
        if (neg) x = -x;
        
        int base = 1;
        while ((long long)base * 10 <= x) base *= 10;
        
        int ret = 0;
        for (int i = base, j = 1; i >= j; i /= 10, j *= 10) {
            int a = x / i % 10, b = x / j % 10;
            ret += b * i;
            if (i > j) ret += a * j;
        }
        
        return neg ? -ret : ret;
    }
};