class Solution {
public:
    int searchInsert(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        int l = 0, r = n - 1;
        while (l <= r) {
            int m = (l + r) >> 1;
            if (A[m] < target) l = m + 1;
            else r = m - 1;
        }
        
        return l;
    }
};