class Solution {
public:
    void DFS(int l, int r, int n, string s, vector<string> &ret) {
        if (l == n && r == n) {
            ret.push_back(s);
            return;
        }
        
        if (l < n) DFS(l + 1, r, n, s + '(', ret);
        if (l > r) DFS(l, r + 1, n, s + ')', ret);
    }
    
    vector<string> generateParenthesis(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        vector<string> ret;
        DFS(0, 0, n, "", ret);
        
        return ret;
    }
};