class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
     
        vector<int> num(256, 0);
        int n = s.size(), ret = 0;
        for (int j = 0, i = 0; i < n; ++i) {
            ++num[s[i]];
            while (num[s[i]] > 1) --num[s[j++]];
            if (i - j + 1 > ret) ret = i - j + 1;
        }
        
        return ret;
    }
};