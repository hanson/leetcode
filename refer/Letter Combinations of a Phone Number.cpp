class Solution {
public:
    string pat[10];
    
    void DFS(int step, string s, int n, string &digits, vector<string> &ans) {
        if (step == n) {
            ans.push_back(s);
            return;
        }
        
        for (int i = 0; i < pat[digits[step] - '0'].size(); ++i)
            DFS(step + 1, s + pat[digits[step] - '0'][i], n, digits, ans);
    }
    
    vector<string> letterCombinations(string digits) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        pat[2] = "abc"; pat[3] = "def"; pat[4] = "ghi";
        pat[5] = "jkl"; pat[6] = "mno"; pat[7] =  "pqrs";
        pat[8] = "tuv"; pat[9] = "wxyz";
        vector<string> ans;
        DFS(0, "", digits.size(), digits, ans);
        
        return ans;
    }
};