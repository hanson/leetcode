/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseKGroup(ListNode *head, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        if (k == 1) return head;
            
        ListNode *advance = head;
        for (int i = 0; i < k; ++i) {
            if (advance == NULL) return head;
            advance = advance->next;
        }
                    
        ListNode *p = head, *last = NULL, *ret = NULL;
        while (1) {
            ListNode *first = p, *q = p->next;
            for (int i = 0; i < k - 1; ++i) {
                ListNode *r = q->next;
                q->next = p;
                p = q; q = r;
                if (advance != NULL) advance = advance->next;
            }
            
            if (ret == NULL) ret = p;
            else last->next = p;
            last = first;
            first->next = q;
            p = q;
            if (advance == NULL) break;
            advance = advance->next;
        }
        
        return ret;
    }
};