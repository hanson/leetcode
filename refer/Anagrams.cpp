class Solution {
public:
    vector<string> anagrams(vector<string> &strs) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        map<string, int> dic;
        int n = strs.size();
        vector<bool> mark(n, 0);
        for (int i = 0; i < n; ++i) {
            string s = strs[i];
            sort(s.begin(), s.end());
            if (dic.find(s) != dic.end()) mark[dic[s]] = mark[i] = 1;
            dic[s] = i;
        }
        vector<string> ret;
        for (int i = 0; i < n; ++i)
            if (mark[i]) ret.push_back(strs[i]);
        return ret;
    }
};