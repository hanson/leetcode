class Solution {
public:
    int atoi(const char *str) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        while (*str) {
            if (*str == ' ') {
                ++str;
                continue;
            }
            
            bool neg = 0;
            if (*str == '+') ++str;
            else if (*str == '-') {
                neg = 1;
                ++str;
            }
            
            long long res = 0;
            while (*str && isdigit(*str)) {
                res = res * 10 + *str - '0';
                ++str;
            }
            
            if (neg) res = -res;
            int ret;
            if (res < -2147483648LL) ret = -2147483648LL;
            else if (res > 2147483647LL) ret = 2147483647LL;
            else ret = (int)res;
            
            return ret;
        }
                
    }
};