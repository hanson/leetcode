class Solution {
public:
    string longestCommonPrefix(vector<string> &strs) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
            
        string s = "";
        int n = strs.size();
        bool flag = 1;
        for (int i = 0; n && i < strs[0].size(); ++i) {
            char c = strs[0][i];
            for (int j = 1; j < n; ++j)
                if (i >= strs[j].size() || i < strs[j].size() && strs[j][i] != c) {
                    flag = 0;
                    break;
                }
            if (!flag) break;
            s += c;
        }
        
        return s;
    }
};