class Solution {
public:
    int maxSubArray(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        int ret = A[0];
        for (int sum = 0, i = 0; i < n; ++i) {
            sum += A[i];
            if (sum > ret) ret = sum;
            if (sum < 0) sum = 0;
        }
        
        return ret;
    }
};