class Solution {
public:
    void nextPermutation(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        vector<int>::iterator p = num.end(); --p;
        while (p != num.begin() && *p <= *(p - 1)) --p;
        if (p == num.begin()) {
            reverse(num.begin(), num.end());
            return;
        }
        
        --p;
        vector<int>::iterator q = num.end(); --q;
        while (*q <= *p) --q;
        swap(*p, *q);
        ++p;
        reverse(p, num.end());
    }
};