class Solution {
public:
    int longestValidParentheses(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
     
        int n = s.size(), ret = 0;
        for (int cnt = 0, len = 0, i = 0; i < n; ++i) {
            if (s[i] == '(') {
                ++cnt;
                ++len;
            }
            else {
                --cnt; ++len;
                if (cnt == 0) ret = max(ret, len);
                else if (cnt < 0) cnt = len = 0;
            }
        }
        for (int cnt = 0, len = 0, i = n - 1; i >= 0; --i) {
            if (s[i] == ')') {
                ++cnt;
                ++len;
            }
            else {
                --cnt; ++len;
                if (cnt == 0) ret = max(ret, len);
                else if (cnt < 0) cnt = len = 0;
            }
        }
        
        return ret;
    }
};