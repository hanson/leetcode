class Solution {
public:
    vector<int> searchRange(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        vector<int> ret(2);
        int l = 0, r = n - 1;
        while (l <= r) {
            int m = (l + r) >> 1;
            if (A[m] < target) l = m + 1;
            else r = m - 1;
        }
        ret[0] = l < n && A[l] == target ? l : -1;
        
        l = 0; r = n - 1;
        while (l <= r) {
            int m = (l + r) >> 1;
            if (A[m] > target) r = m - 1;
            else l = m + 1;
        }
        ret[1] = r >= 0 && A[r] == target ? r : -1;
        
        return ret;
    }
};