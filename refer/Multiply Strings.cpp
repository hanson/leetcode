class Solution {
public:
    string multiply(string num1, string num2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        int n = num1.size(), m = num2.size();
        vector<int> res(n + m + 1, 0);
        
        reverse(num1.begin(), num1.end());
        reverse(num2.begin(), num2.end());
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j) {
                res[i + j] += (num1[i] - '0') * (num2[j] - '0');
                res[i + j + 1] += res[i + j] / 10;
                res[i + j] %= 10;
            }
        
        int z = n + m;
        while (z && res[z] == 0) --z;
        
        string ret = "";
        for (int i = z; i >= 0; --i) ret += static_cast<char>(res[i] + '0');
        
        return ret;
    }
};