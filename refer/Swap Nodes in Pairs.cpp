/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *swapPairs(ListNode *head) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        if (head == NULL || head->next == NULL) return head;
        
        ListNode *ret = head->next, *prev = NULL, *p = head;
        while (p != NULL && p->next != NULL) {
            ListNode *q = p->next;
            p->next = q->next;
            q->next = p;
            if (prev != NULL) prev->next = q;
            prev = p; p = p->next;
        }
        
        return ret;
    }
};