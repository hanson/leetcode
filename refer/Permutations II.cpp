class Solution {
public:
    vector<vector<int> > permuteUnique(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        sort(num.begin(), num.end());
        
        vector<vector<int> > ret;
        ret.push_back(num);
        bool flag;
        do {
            flag = 0;
            for (vector<int>::iterator i = num.end() - 1; i != num.begin(); --i) 
                if (*(i - 1) < *i) {
                    vector<int>::iterator j = num.end() - 1;
                    while (*j <= *(i - 1)) --j;
                    swap(*(i - 1), *j);
                    reverse(i, num.end());
                    flag = 1;
                    ret.push_back(num);
                    break;
                }
        } while (flag);
        return ret;
    }
};