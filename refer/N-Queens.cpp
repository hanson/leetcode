class Solution {
public:
    void DFS(int i, int n, int col, int x, int y, vector<string> &board, vector<vector<string> > &ans) {
        if (i == n) {
            ans.push_back(board);
            return;
        }
        
        for (int j = 0; j < n; ++j)
            if (!((col >> j) & 1) && !((x >> (i - j + n - 1)) & 1) && !((y >> (i + j)) & 1)) {
                board[i][j] = 'Q';
                DFS(i + 1, n, col | (1 << j), x | (1 << (i - j + n - 1)), y | (1 << (i + j)), board, ans);
                board[i][j] = '.';
            }
    }
    
    vector<vector<string> > solveNQueens(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        vector<vector<string> > ret;
        vector<string> board(n);
        for (int i = 0; i < n; ++i) {
            board[i] = "";
            for (int j = 0; j < n; ++j) board[i] += ".";
        }
        
        DFS(0, n, 0, 0, 0, board, ret);
        return ret;
    }
};