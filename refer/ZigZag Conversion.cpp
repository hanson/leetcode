class Solution {
public:
    string convert(string s, int nRows) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function    
        
        if (nRows == 0) return 0;
        if (nRows == 1) return s;
        
        for (int i = 0; i < nRows; ++i) s += " ";
        int n = s.size();
        string ret = "";
        for (int i = 0; i < nRows; ++i)
            if (i == 0 || i == nRows - 1) {
                int j = i;
                while (j < n) {
                    if (s[j] != ' ') ret += s[j];
                    j += nRows + nRows - 2;
                }
            }
            else {
                bool d = 1;
                int j = i, k = 0;
                while (j < n) {
                    if (d) {
                        k += nRows;
                        if (s[j] != ' ') ret += s[j];
                    }
                    else {
                        k += nRows - 2;
                        int p = j % (nRows + nRows - 2) - nRows;
                        if (k - 1 - p < n && s[k - 1 - p] != ' ') ret += s[k - 1 - p];
                    }
                    j += nRows - 1;
                    d = !d;
                }
            }
            
        return ret;
    }
};