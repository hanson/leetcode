class Solution {
public:
    bool isMatch(const char *s, const char *p) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function    
        
        if (*s == 0 && *p == 0) return 1;
        if (*p == 0) return 0;
        
        if (*(p + 1) == '*') {
            bool ret = isMatch(s, p + 2);
            if (ret) return ret;
            while (*s != 0 && (*p == '.' || *p == *s)) {
                ret = isMatch(s + 1, p + 2);
                if (ret) return ret;
                ++s;
            }
            return ret;
        }
        else {
            if (*s == 0) return 0;
            if (*p == '.' || *p == *s) return isMatch(s + 1, p + 1);
            else return 0;
        }
    }
};