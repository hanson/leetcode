class Solution {
public:
    int firstMissingPositive(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
     
        for (int i = 0; i < n; ++i)
            if (A[i] <= 0) A[i] = n + 1;
        
        for (int i = 0; i < n; ++i)
            if (A[i] <= n && A[i] >= -n && A[abs(A[i]) - 1] > 0) A[abs(A[i]) - 1] = -A[abs(A[i]) - 1];
        
        for (int i = 0; i < n; ++i)
            if (A[i] > 0) return i + 1;
        return n + 1;
    }
};