class Solution {
public:
    int median(int *A, int m, int *B, int n, int k) {
        int i = (int)((double)m / (m + n) * (k - 1)), j = k - 1 - i;
        
        int A1 = (i == 0) ? INT_MIN : A[i - 1], A2 = (i == m) ? INT_MAX : A[i];
        int B1 = (j == 0) ? INT_MIN : B[j - 1], B2 = (j == n) ? INT_MAX : B[j];
        
        if (B1 < A2 && A2 <= B2) return A2;
        if (A1 < B2 && B2 <= A2) return B2;
        
        if (A2 < B2) return median(A + i + 1, m - i - 1, B, j, k - i - 1);
        else return median(A, i, B + j + 1, n - j - 1, k - j - 1);
    }

    double findMedianSortedArrays(int A[], int m, int B[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        if (m == 0 && n == 0) return 0;
        
        int ret = median(A, m, B, n, (m + n) / 2 + 1);
        if (!((m + n) & 1)) ret += median(A, m, B, n, (m + n) / 2);
        else ret <<= 1;
        
        return ret / 2.0;
    }
};