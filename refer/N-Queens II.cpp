class Solution {
public:
    void DFS(int i, int n, int col, int x, int y, int &ans) {
        if (i == n) {
            ++ans;
            return;
        }
        
        for (int j = 0; j < n; ++j)
            if (!((col >> j) & 1) && !((x >> (i - j + n - 1)) & 1) && !((y >> (i + j)) & 1))
                DFS(i + 1, n, col | (1 << j), x | (1 << (i - j + n - 1)), y | (1 << (i + j)), ans);
    }

    int totalNQueens(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        int ret = 0;
        DFS(0, n, 0, 0, 0, ret);
        return ret;
    }
};