class Solution {
public:
    vector<vector<int> > ans;
    vector<int> a, b, q;
    int m;
    
    void DFS(int step, int remain) {
        if (remain == 0) {
            ans.push_back(q);
            return;
        }
        
        if (step == m) return;
        
        DFS(step + 1, remain);
        for (int i = 1; i <= b[step] && remain - i * a[step] >= 0; ++i) {
            q.push_back(a[step]);
            DFS(step + 1, remain - i * a[step]);
        }
        for (int i = 1; i <= b[step] && remain - i * a[step] >= 0; ++i) q.pop_back();
    }
    
    vector<vector<int> > combinationSum2(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
 
        a.clear(); b.clear();
        sort(num.begin(), num.end());
        int n = num.size();
        m = 0;
        for (int i = 0; i < n; ++i)
            if (m == 0 || a[m - 1] < num[i]) {
                a.push_back(num[i]);
                b.push_back(1);
                ++m;
            }
            else ++b[m - 1];

        ans.clear();
        DFS(0, target);
     
        return ans;
    }
};