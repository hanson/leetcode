class Solution {
public:
    int divide(int dividend, int divisor) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        if (divisor == 1) return dividend;
        if (divisor == -1) return -dividend;
        
        bool neg = (dividend < 0) ^ (divisor < 0);
        
        long long a = dividend, b = divisor;
        if (a < 0) a = -a;
        if (b < 0) b = -b;
        
        int ret = 0;
        while (a >= b) {
            long long x = b;
            int cnt = 1;
            while (x + x <= a) {
                x += x;
                cnt += cnt;
            }
            ret += cnt;
            a -= x;
        }
        
        return neg ? -ret : ret;
    }
};
