class Solution {
public:
    void DFS(int step, int n, vector<int> &num, vector<vector<int> > &ret) {
        if (step == n) {
            ret.push_back(num);
            return;
        }
        
        for (int i = step; i < n; ++i) {
            swap(num[i], num[step]);
            DFS(step + 1, n, num, ret);
            swap(num[i], num[step]);
        }
    }

    vector<vector<int> > permute(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        vector<vector<int> > ret;
        DFS(0, num.size(), num, ret);
        return ret;
    }
};