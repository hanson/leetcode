class Solution {
public:
    string longestPalindrome(string s) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        int n = s.size();
        string t = "";
        t += ".";
        for (int i = 0; i < n; ++i) {
            t += s[i];
            t += ".";
        }
        
        n = t.size();
        vector<int> ext(n);
        ext[0] = 1;
        for (int j = 0, i = 1; i < n; ++i) {
            int ii = j - (i - j);
            if (i > j + ext[j] - 1) {
                ext[i] = 0;
                while (i - ext[i] >= 0 && i + ext[i] < n && t[i - ext[i]] == t[i + ext[i]]) ++ext[i];
                j = i;
            }
            else {
                int l = j + ext[j] - 1 - i + 1;
                if (ext[ii] == l) {
                    ext[i] = ext[ii];
                    while (i - ext[i] >= 0 && i + ext[i] < n && t[i - ext[i]] == t[i + ext[i]]) ++ext[i];
                    j = i;
                }
                else ext[i] = min(ext[ii], l);
            }
        }
        
        int p = 0;
        for (int i = 0; i < n; ++i)
            if (ext[i] > ext[p]) p = i;
        
        string ret = "";
        for (int i = p - ext[p] + 1; i < p + ext[p]; ++i)
            if (t[i] != '.') ret += t[i];
            
        return ret;
    }
};