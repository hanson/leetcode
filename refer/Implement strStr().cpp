class Solution {
public:
    char *strStr(char *haystack, char *needle) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (*needle == 0) return haystack;
        
        char *p1 = haystack, *p2 = needle;
        while (*p1 && *++p2) ++p1;
        char *tail = p1;
        while (*tail) {
            char *p1 = haystack, *p2 = needle;
            while (*p1 && *p2 && *p1 == *p2) {
                ++p1; ++p2;
            }
            if (*p2 == 0) return haystack;
            ++haystack; ++tail;
        }
        return NULL;
    }
};