class Solution {
public:
    int threeSumClosest(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        sort(num.begin(), num.end());
        
        int n = num.size(), ret = num[0] + num[1] + num[2];
        for (int i = 0; i < n; ++i) {
            int j = i + 1, k = n - 1;
            while (j < k) {
                if (abs(num[i] + num[j] + num[k] - target) < abs(ret - target))
                    ret = num[i] + num[j] + num[k];
                if (num[i] + num[j] + num[k] < target) ++j;
                else if (num[i] + num[j] + num[k] > target) --k;
                else return ret;
            }
        }
        
        return ret;
    }
};