class Solution {
public:
    double pow(double x, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        bool neg = 0;
        long long m = n;
        if (m < 0) {
            m = -m;
            neg = 1;
        }
        
        double ret = 1.0;
        while (m) {
            if (m & 1) ret *= x;
            x *= x; m >>= 1;
        }
        
        return neg ? 1.0 / ret : ret;
    }
};