class Solution {
public:
    int trap(int A[], int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        if (n == 0) return 0;
        
        int *B = new int[n], *C = new int[n];
        B[0] = 0;
        for (int i = 1; i < n; ++i) B[i] = max(B[i - 1], A[i - 1]);
        C[n - 1] = 0;
        for (int i = n - 2; i >= 0; --i) C[i] = max(C[i + 1], A[i + 1]);
        
        int ret = 0;
        for (int i = 0; i < n; ++i) {
            int h = min(B[i], C[i]);
            if (h > A[i]) ret += h - A[i];
        }
        
        delete [] B; delete [] C;
        return ret;
    }
};