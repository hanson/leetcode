class Solution {
public:
    string intToRoman(int num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function    
        
        const string pat[] = {"I", "V", "X", "L", "C", "D", "M"};
        
        string s = "";
        int base = 0;
        while (num) {
            int x = num % 10;
            if (x < 4) {
                for (int i = 0; i < x; ++i) s = pat[base] + s;
            }
            else if (x == 4) s = pat[base] + pat[base + 1] + s;
            else if (x == 5) s = pat[base + 1] + s;
            else if (x < 9) {
                for (int i = 5; i < x; ++i) s = pat[base] + s;
                s = pat[base + 1] + s;
            }
            else s = pat[base] + pat[base + 2] + s;
            base += 2; num /= 10;
        }
        
        return s;
    }
};