/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
            
        if (l1 == NULL) return l2;
        if (l2 == NULL) return l1;
        
        ListNode *head;
        if (l1->val < l2->val) {
            head = l1;
            l1 = l1->next;
        }
        else {
            head = l2;
            l2 = l2->next;
        }
        
        ListNode *last = head;
        while (l1 != NULL && l2 != NULL) {
            if (l1->val < l2->val) {
                last->next = l1;
                l1 = l1->next;
            }
            else {
                last->next = l2;
                l2 = l2->next;
            }
            last = last->next;
        }
        if (l1 == NULL) last->next = l2;
        if (l2 == NULL) last->next = l1;
        
        return head;
    }
};