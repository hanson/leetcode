class Solution {
public:
    bool isValidSudoku(vector<vector<char> > &board) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        vector<int> row(9, 0), col(9, 0), blo(9, 0);
        for (int i = 0; i < 9; ++i)
            for (int j = 0; j < 9; ++j) {
                char c = board[i][j];
                if (c == '.') continue;
                int k = c - '1';
                if ((row[i] >> k) & 1) return false;
                row[i] |= 1 << k;
                if ((col[j] >> k) & 1) return false;
                col[j] |= 1 << k;
                if ((blo[(i / 3) * 3 + j / 3] >> k) & 1) return false;
                blo[(i / 3) * 3 + j / 3] |= 1 << k;
            }
            
        return true;
    }
};