class Solution {
public:
    int maxArea(vector<int> &height) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        int n = height.size(), l = 0, r = n - 1;
        int ret = 0;
        while (l < r) {
            if ((r - l) * min(height[l], height[r]) > ret) ret = (r - l) * min(height[l], height[r]);
            if (height[l] < height[r]) ++l;
            else if (height[l] > height[r]) --r;
            else {
                ++l;
                --r;
            }
        }
        
        return ret;
    }
};