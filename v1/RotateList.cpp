#include <iostream>
using namespace std;

/**
 ** Definition for singly-linked list.
 **/
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
    public:
        ListNode *rotateRight(ListNode *head, int k) {
            if (head == NULL)
                return head;

            int len = 0;
            ListNode *tmp = head;
            while (tmp != NULL)
            {
                ++len;
                tmp = tmp->next;
            }

            k = k % len;
            ListNode *ph = head;
            ListNode *pt = head;
            while (k > 0)
            {
                pt = pt->next;
                --k;
            }
            while (pt->next != NULL)
            {
                pt = pt->next;
                ph = ph->next;
            }

            pt->next = head;
            head = ph->next;
            ph->next = NULL;

            return head;
        }
};

int main()
{
    return 0;
}

