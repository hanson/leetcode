#include <algorithm>
#include <iostream>
#include <queue>
#include <string>
#include <vector>
using namespace std;

class Solution
{
    private:
        int row;
        int col;
        int dr[4];
        int dc[4];
        class Point
        {
            public:
            int r;
            int c;
            Point(int tr, int tc)
            {
                r = tr;
                c = tc;
            }
        };
    public:
        void init()
        {
            dr[0] = 0;
            dr[1] = 0;
            dr[2] = -1;
            dr[3] = 1;
            dc[0] = 1;
            dc[1] = -1;
            dc[2] = 0;
            dc[3] = 0;
        }

        bool isvalid(int r, int c)
        {
            return (r>=0 && r<row && c>=0 && c<col);
        }

        void bfs(vector<vector<char> > &board, int r, int c)
        {
            Point *cur;
            queue<Point*> que;
            while (que.empty() == false)
            {
                que.pop();
            }
            board[r][c] = 'A';
            que.push(new Point(r, c));
            while (que.empty() == false)
            {
                cur = que.front();
                que.pop();
            //board[r][c] = 'A';
                for (int i = 0; i < 4; ++i)
                {
                    int tr = cur->r + dr[i];
                    int tc = cur->c + dc[i];
                    if (isvalid(tr, tc) == false)
                    {
                        continue;
                    }
                    if (board[tr][tc] == 'O')
                    {
                        board[tr][tc] = 'A';
                        que.push(new Point(tr, tc));
                    }
                }
            }
        }
          
        void dealwith(vector<vector<char> > &board)
        {
            int c, r;
            //up most row
            for (c = 0; c < col; ++c)
            {
                if (board[0][c] == 'O')
                {
                    bfs(board, 0, c);
                }
            }
            //left most col
            for (r = 0; r < row; ++r)
            {
                if (board[r][col-1] == 'O')
                {
                    bfs(board, r, col-1);
                }
            }
            //down most row
            for (c = 0; c < col; ++c)
            {
                if (board[row-1][c] == 'O')
                {
                    bfs(board, row-1, c);
                }
            }
            //right most col
            for (r = 0; r < row; ++r)
            {
                if (board[r][0] == 'O')
                {
                    bfs(board, r, 0);
                }
            }
        }

        void solve(vector<vector<char> > &board) {
            row = board.size();
            if (row <= 0)
            {
                return;
            }
            col = board[0].size();
            if (col <= 0)
            {
                return;
            }
            init();
            dealwith(board);
            //
            int c, r;
            for (r = 0; r < row; ++r)
            {
                for (c = 0; c < col; ++c)
                {
                    if (board[r][c] == 'O')
                    {
                        board[r][c] = 'X';
                    }
                    else if (board[r][c] == 'A')
                    {
                        board[r][c] = 'O';
                    }
                }
            }
        }
};

int main()
{
    return 0;
}
