#include <iostream>
using namespace std;

class Solution
{
    public:
        void dfs(TreeNode *curNode)
        {
            if (curNode == NULL)
                return;
            if (curNode->left != NULL)
            {
                dfs(curNode->left);

                TreeNode* temp = curNode->left;
                while (temp->right != NULL)
                {
                    temp = temp->right;
                }
                temp->right = curNode->right;
                curNode->right = curNode->left;
                curNode->left = NULL;
            }

            if (curNode->right != NULL)
            {
                dfs(curNode->right);
            }
        }

        void flatten(TreeNode *root) {
            
            dfs(root);
        
        }
};

int main()
{
    return 0;
}

