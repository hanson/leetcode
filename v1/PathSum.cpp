#include <iostream>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
    private:
        bool flag;
    public:

        void dfs(TreeNode *curNode, int pathSum, int sum)
        {
            if (curNode == NULL)
                return;

            if (flag)
            {
                return;
            }
            
            int curSum = pathSum + curNode->val;
            if (curSum==sum && curNode->left==NULL && curNode->right==NULL)
            {
                flag = true;
                return;
            }

            dfs(curNode->left, curSum, sum);
            dfs(curNode->right, curSum, sum);
        }

        bool hasPathSum(TreeNode *root, int sum) {
            flag = false;
            dfs(root, 0, sum);

            return flag;
        }
};

int main()
{
    return 0;
}

