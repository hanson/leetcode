#include <iostream>
#include <string>
using namespace std;

class Solution
{
    public:
        bool isSame(char lhc, char rhc)
        {
            if (lhc>='A'&& lhc<='Z')
            {
                lhc -= 'A';
                lhc += 'a';
            }
            
            
            if (rhc>='A'&& rhc<='Z')
            {
                rhc -= 'A';
                rhc += 'a';
            }

            if(lhc != rhc)
            {
                return false;
            }

            return true;
        }

        bool isAlphaNumeric(char ch)
        {
            if ((ch>='a' && ch<='z') || (ch>='A'&& ch<='Z') || (ch>='0'&& ch<='9'))
            {
                return true;
            }

            return false;
        }

        bool isPalindrome(string s)
        {
            if (s == "")
                return true;

            int lhs;
            int rhs;
            lhs = 0;
            rhs = s.length()-1;
            while (!isAlphaNumeric(s[lhs]) && lhs<=s.length())
            {
                ++lhs;
            }

            while (!isAlphaNumeric(s[rhs]) && rhs>=-1)
            {
                --rhs;
            }

            while (lhs <= rhs)
            {
                if (!isSame(s[lhs], s[rhs]))
                {
                    return false;
                }
                
                ++lhs;
                --rhs;
                while (!isAlphaNumeric(s[lhs]) && lhs<=s.length())
                {
                    ++lhs;
                }

                while (!isAlphaNumeric(s[rhs]) && rhs>=-1)
                {
                    --rhs;
                }
            }

            return true;
        }
};
