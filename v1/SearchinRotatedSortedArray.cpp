#include <iostream>
using namespace std;

class Solution
{
    private:
        int pivotindex;
        int pivotval;
    public:   
        void setpivot(int p, int pv)
        {
            if (pivotindex==-1 || pivotval>pv)
            {
                pivotindex = p;
                pivotval = pv;
            }
        }

        int findp(int A[], int n)
        {
            pivotindex = -1;
            int left = 0;
            int right = n-1;
            int mid;
            while (left <= right)
            {
                mid = (left+right)>>1;
                if (A[left] > A[mid])
                {
                    setpivot(mid, A[mid]);
                    right = mid - 1;
                }
                else if (A[mid] > A[right])
                {
                    setpivot(right, A[right]);
                    left = mid + 1;
                }
                else
                {
                    setpivot(left, A[left]);
                    break;
                }
            }
        }

        int binsearch(int A[], int left, int right, int target)
        {
            while (left <= right)
            {
                int mid = (left+right)>>1;
                if (A[mid] == target)
                {
                    return mid;
                }
                else if (A[mid] > target)
                {
                    right = mid - 1;
                }
                else
                {
                    left = mid + 1;
                }
            }

            return -1;
        }

        int search(int A[], int n, int target) {
            if (n <= 0)
            {
                return -1;
            }
            findp(A, n);

            int l1 = 0;
            int r1 = pivotindex-1;
            int l2 = pivotindex;
            int r2 = n-1;
            //search l1 - r1
            if (pivotindex == 0)
            {
                return binsearch(A, 0, n-1, target);
            }
            else if (target > A[n-1])
            {
                return binsearch(A, l1, r1, target);
            }
            //search l2 - r2
            else if (target < A[0])
            {
                return binsearch(A, l2, r2, target);
            }

            return -1;
        }
};

int main()
{
    Solution s;
    int A[12] = {47,48,49,411,412,413,414,415,16,17,24,25};
    cout<<s.search(A, 12, 411)<<endl;
    return 0;
}

