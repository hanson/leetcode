#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

/**
 *  Definition for singly-linked list.
 **/

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
    public:
        ListNode *swapPairs(ListNode *head) {
            if (head == NULL)
            {
                return head;
            }

            ListNode *pre = head;
            ListNode *cur = head->next;

            while (cur != NULL)
            {
                swap(pre->val, cur->val);
                pre = cur->next;
                if (pre == NULL)
                {
                    break;
                }
                cur = pre->next;
            }

            return head;
        }
};

int main()
{
    return 0;
}
