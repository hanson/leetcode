#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
        vector<int> getRow(int rowIndex) {
            vector<int> tri;
            vector<int> temp;
            for (int i = 0; i <= rowIndex; ++i)
            {
                temp.push_back(1);
                tri.push_back(1);
            }

            for (int k = 1; k <= rowIndex; ++k)
            {
                for (int i = 0; i <= k; ++i)
                {
                    if (i == 0)
                    {
                        tri[i] = 1;
                    }
                    else if (i == k)
                    {
                        tri[i] = 1;
                    }
                    else
                    {
                        tri[i] = temp[i]+temp[i-1];
                    }
                }
                for (int i = 0; i <= k; ++i)
                {
                    temp[i] = tri[i];
                }
            }

            return tri;
        }
};

int main()
{
    return 0;
}

