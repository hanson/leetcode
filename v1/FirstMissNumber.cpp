#include <iostream>

using namespace std;

class Solution
{
    public:
        int *hash;

        //鸽巢原理
        int firstMissingPositive(int A[], int n) {
            if (n == 0)
                return 1;
            hash = (int*)malloc((n+1+1)*sizeof(int));//0 - n+1
            for (int i = 1; i <= n+1; ++i)
            {
                hash[i] = 0;
            }

            for (int i = 0; i < n; ++i)
            {
                if (A[i] <= 0 || A[i] > n)
                    continue;

                hash[ A[i] ] = 1;
            }

            for (int i = 1; i <= n+1; ++i)
            {
                if (hash[i] == 0)
                    return i;
            }
        }
};
