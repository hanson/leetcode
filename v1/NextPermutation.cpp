#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class Solution
{
    public:
        void nextPermutation(vector<int> &num) {
            int len = num.size();
            if (len <= 0)
                return;

            bool flag = false;
            for (int k = len-1; k >= 0; --k)
            {
                int temp = -1;
                for (int i = k+1; i < len; ++i)
                {
                    if (num[k] < num[i] && (temp==-1 || num[i]<=num[temp]))
                    {
                        temp = i;
                    }
                }
                if (temp == -1)
                {
                    continue;
                }

                swap(num[k], num[temp]);
                sort(num.begin()+k+1, num.end());
                flag = true;
                break;
            }
            if (!flag)
            {
                sort(num.begin(), num.end());
            }
        }
};
