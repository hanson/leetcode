#include <iostream>
#include <string>
#include <vector>
using namespace std;

//dp 
class Solution
{
    private:
        vector<vector<vector<bool> > > dp;
    public:   
        bool isScramble(string s1, string s2) {
            int len = s1.length();
            if (len != s2.length())
            {
                return false;
            }

            dp.clear();
            for (int i = 0; i < len; i++)
            {
                vector<vector<bool> > tmp(len, vector<bool>(len + 1));
                dp.push_back(tmp);
            }

            for (int i = 0; i < len; i++)
            {       
                for (int j = 0; j < len; j++)  
                {
                    if (s1[i] == s2[j])  
                    {
                        dp[i][j][1] = true;
                    }
                }
            }

            for (int k = 2; k <= len; ++k)
            {
                for (int i1 = 0; i1 < len; ++i1)
                {
                    for (int i2 = 0; i2 < len; ++i2)
                    {
                        for (int p = 1; p < k; ++p)
                        {
                            int q = k - p;
                            if (i1+p>=len || i2+p>=len || i2+q>=len || i1+q>=len)
                            {
                                continue;
                            }

                            if ((dp[i1][i2][p] && dp[i1+p][i2+p][q]) || (dp[i1][i2+q][p] && dp[i1+p][i2][q]))
                            {
                                dp[i1][i2][k] = true;
                                break;
                            }
                        }
                    }
                }
            }

            return dp[0][0][len];
        }
};

int main()
{
    Solution s;
    cout<<s.isScramble("aa", "aa")<<endl;

    return 0;
}
