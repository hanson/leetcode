#include <algorithm>
#include <iostream>

using namespace std;

class Solution
{
    public:    
        double findkth(int A[], int aoffset, int m, int B[], int boffset, int n, int k)
        {
            if (m > n)
            {
                return findkth(B, boffset, n, A, aoffset, m, k);
            }

            if (m == 0)
            {
                return B[ boffset+k-1 ];
            }
            if (k == 1)
            {
                return min(A[aoffset], B[boffset]);
            }
            
            int pa = min(k/2, m);
            int pb = k - pa;

            if (A[aoffset + pa - 1] < B[boffset + pb - 1])
            {
                return findkth(A, aoffset + pa, m - pa, B, boffset, n, k - pa);
            }

            return findkth(A, aoffset, m, B, boffset+pb, n-pb, k-pb);
        }
        double findMedianSortedArrays(int A[], int m, int B[], int n) {
            int len = m+n;
            if (len%2 == 0)
            {
                return (findkth(A, 0, m, B, 0, n, len/2) + findkth(A, 0, m, B, 0, n, len/2+1))/2*1.0;
            }

            return findkth(A, 0, m, B, 0, n, len/2+1);
        }
};

int main()
{}
