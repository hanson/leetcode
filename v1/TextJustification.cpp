#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        vector<string> fullJustify(vector<string> &words, int L) {
            vector<string> ret;
            int st = 0;
            int len = words.size();
            while (st < len)
            {
                //[st end)
                int lc = words[st].size();
                int end = st+1;
                while (end<len)
                {
                    if ( (lc+1+words[end].length()) > L)
                    {
                        break;
                    }
                    lc += (1 + words[end].length());
                    ++end;
                }

                if (end == len)
                {
                    int tlc = 0;
                    string tmp = "";
                    for (int k = st; k < end; ++k)
                    {
                        if (k != st)
                        {
                            ++tlc;
                            tmp += ' ';
                        }
                        tlc += words[k].length();
                        tmp.append(words[k]);
                    }
                    for (int k = tlc + 1; k <= L; ++k)
                    {
                        tmp += ' ';
                    }
                    ret.push_back(tmp);
                    break;
                }
                else
                {
                    int wc = (end - st);
                    --wc;
                    int spacec;
                    if (wc > 0)
                        spacec = (L - (lc - wc))/wc;
                    string tmp = "";
                    int fsc = (L-(lc-wc)) - spacec*(wc);
                    int tlc = 0;
                    for (int k = st; k < end; ++k)
                    {
                        if (k != st) 
                        {
                            if (fsc > 0)
                            {
                                --fsc;
                                tmp += ' ';
                                ++tlc;
                            }
                            int tmpsc = spacec;
                            while (tmpsc > 0)
                            {
                                --tmpsc;
                                tmp += ' ';
                                ++tlc;
                            }
                        }

                        tlc += words[k].length();
                        tmp.append(words[k]);
                    }

                    for (int k = tlc + 1; k <= L; ++k)
                    {
                        tmp += ' ';
                    }
                    ret.push_back(tmp);
                }

                st = end;
            }
            return ret;
        }
};

int main()
{
    return 0;
}
