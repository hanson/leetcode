#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    private:
        int n;
        vector<vector<int> >pms;
    public:
        bool has(vector<int> num)
        {
            for (int i = 0; i < pms.size(); ++i)
            {
                int k;
                for (k = 0; k < num.size(); ++k)
                {
                    if (num[k] != pms[i][k])
                    {
                        break;
                    }
                }
                if (k >= num.size())
                {
                    return true;
                }
            }
            return false;
        }

        void genpms(int cur, vector<int> num)
        {
            if (cur >= num.size())
            {
                if (has(num) == false)
                {
                    pms.push_back(num);
                }
                return;
            }
            for (int i = cur; i < num.size(); ++i)
            {
                if (i > cur && num[i]==num[cur])
                {
                    continue;
                }
                swap(num[cur], num[i]);
                genpms(cur+1, num);
                swap(num[cur], num[i]);
            }
        }

        vector<vector<int> > permuteUnique(vector<int> &num) {
             pms.clear();
             sort(num.begin(), num.end());
             genpms(0, num);
             return pms;
        }
};

int main()
{
    return 0;
}

