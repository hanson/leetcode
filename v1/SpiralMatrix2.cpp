#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
    	vector<vector<int> > generateMatrix(int n) {
            vector<vector<int> >matrix;
            for (int i = 0; i < n; ++i)
            {
                vector<int> tmp(n);
                matrix.push_back(tmp);
            }
            int clen = n;
            int rlen = n;
            int wx = 0, wy = -1;
            long val = 1; 
            while (true)
            {
                //->
                for (int k = 1; k <= clen; ++k)
                {
                    ++wy;
                    matrix[wx][wy] = val++;
                }
                --rlen;
                if (rlen <= 0)
                {
                    break;
                }
                //|
                //V
                for (int k = 1; k <= rlen; ++k)
                {
                    ++wx;
                    matrix[wx][wy] = val++;
                }
                --clen;
                if (clen <= 0)
                {
                    break;
                }
                //<-
                for (int k = 1; k <= clen; ++k)
                {
                    --wy;
                    matrix[wx][wy] = val++;
                }
                --rlen;
                if (rlen <= 0)
                {
                    break;
                }
                //^
                //|
                for (int k = 1; k <= rlen; ++k)
                {
                    --wx;
                    matrix[wx][wy] = val++;
                }
                --clen;
                if (clen <= 0)
                {
                    break;
                }
            }

            return matrix;
        }
};

int main()
{
    Solution s;
    vector<vector<int> > a =  s.generateMatrix(1);
    cout<<a[0][0];
    return 0;
}
