#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
    private:
        vector<string> ips;
    public:
        bool isvalidIP(string str)
        {
            if (str.length() > 3)
            {
                return false;
            }
            if (str.length() == 1)
            {
                return true;
            }
            if (str.length() == 2)
            {
                if (str[0] == '0')
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            if (str.length() == 3)
            {
                if (str[0] == '0')
                {
                    return false;
                }
                else if (str[0] == '1')
                {
                    return true;
                }
                else if (str[0] == '2')
                {
                    if (str[1]<='4' && str[1]>='0')
                    {
                        return true;
                    }
                    else if (str[1] == '5')
                    {
                        if (str[2]>='0' && str[2]<='5')
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        void solve(int cur, int sec, string str, string tstr)
        {
            if (cur >= str.length())
            {
                return;
            }
            //done restore
            if (sec == 4)
            {
                if (isvalidIP(str.substr(cur)) == false)
                    return;

                ips.push_back(tstr+'.'+str.substr(cur));
                return;
            }
        
            for (int len = 1; len <= 3; ++len)
            {
                if (isvalidIP(str.substr(cur, len)) == false)
                {
                    continue;
                }
                string tmp = "";
                if (sec != 1)
                {
                    tmp += '.';
                }
                tmp += str.substr(cur, len);
                solve(cur+len, sec+1, str, tstr+tmp);
            }
        }

        vector<string> restoreIpAddresses(string s) {
            ips.clear();
            solve(0, 1, s, "");
            return ips;
        }
};

int main()
{
    return 0;
}

