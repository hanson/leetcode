#include <iostream>
using namespace std;

class Solution
{
    public:
        bool canJump(int A[], int n) {
            int jump = 0;
            for (int i = 0; i < n-1; ++i)
            {
                if (A[i] == 0 && jump == 0)
                {
                    return false;
                }

                if (A[i] > jump)
                {
                    jump = A[i];
                    --jump;
                    if (jump <= 0)
                    {
                        jump = 0;
                    }
                }
                else
                {
                    --jump;
                }
            }


            return true;
        }
};

int main()
{
    return 0;
}

