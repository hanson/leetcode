#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    private:
        vector<vector<int> >sets;
    public:
        bool isDup(vector<int> tmp)
        {
            for (int k = sets.size()-1; k>=0; --k)
            {
                if (tmp.size() != sets[k].size())
                    continue;
                
                int i = 0;
                for (i = 0; i < tmp.size(); ++i)
                {
                    if (tmp[i] != sets[k][i])
                    {
                        break;
                    }
                }
                if (i == tmp.size())
                {
                    return true;
                }
            }

            return false;
        }

        vector<vector<int> > subsetsWithDup(vector<int> &S) {
            sets.clear();
            int len = S.size();
            if (len == 0)
            {

                return sets;
            }
            sort(S.begin(), S.end()); 
            for (int k = 0; k<(1<<len); ++k)
            {
                vector<int> tmp;
                for (int i = 0; i < len; ++i)
                {
                    if (k&(1<<i))
                    {
                        tmp.push_back(S[i]);
                    }
                }
                if (isDup(tmp) == true)
                {
                    continue;
                }
                sets.push_back(tmp);
            }
            return sets;
        }
};

int main()
{
    return 0;
}
