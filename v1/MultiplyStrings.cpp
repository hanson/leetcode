#include <iostream>
#include <string>
using namespace std;

class Solution
{
    public:
        string add(string num1, string num2)
        {
            int sum;
            int carry = 0;
            string ret;
            int i1 = num1.length()-1;
            int i2 = num2.length()-1;
            while (i1>=0 && i2>=0)
            {
                sum = carry + num1[i1]-'0' + num2[i2]-'0';
                carry = sum / 10;
                ret.insert(0, 1, sum%10+'0');
                --i1;
                --i2;
            }

            while (i1>=0)
            {
                sum = carry + num1[i1]-'0';
                carry = sum / 10;
                ret.insert(0, 1, sum%10+'0');
                --i1;
            }

            while (i2>=0)
            {
                sum = carry + num2[i2]-'0';
                carry = sum / 10;
                ret.insert(0, 1, sum%10+'0');
                --i2;
            }

            if (carry > 0)
            {
                ret.insert(0, 1, carry+'0');
            }
            
            return ret;
        }

        string multiply(string num1, string num2) {
            string ret;
            ret.clear();
            for (int k = 0; k < num2.length(); ++k)
            {
                int carry = 0;
                int pro = 0;
                string temp;
                temp.clear();
                for (int i = num1.length()-1; i >= 0; --i)
                {
                    pro = carry + (num2[k]-'0')*(num1[i]-'0');
                    carry = pro / 10;
                    temp.insert(0, 1, (pro%10)+'0');
                }
                if (carry > 0)
                {
                    temp.insert(0, 1, carry+'0');
                }
                ret.append("0");
                ret = add(ret, temp);
            }
            
            while (ret.length()>1 && ret[0]=='0')
            {
                ret.erase(0, 1);
            }
            return ret;
        }
};

int main()
{
    Solution s;
    cout<<s.multiply("9", "9")<<endl;
    return 0;
}

