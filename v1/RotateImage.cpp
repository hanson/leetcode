#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
         void rotate(vector<vector<int> > &matrix) {
        
             vector<vector<int> > tmatrix(matrix);
             int len = matrix.size();
             for (int index = 0; index < len/2; ++index)   
             {
                int k; 
                //up low
                for (k = index; k<len-index; ++k)
                {
                    matrix[index][k] = tmatrix[len-1-k][index];
                }
                //right col
                for (k = index; k<len-index; ++k)
                {
                    matrix[k][len-1-index] = tmatrix[index][k];
                }
                //bottom low
                for (k = index; k<len-index; ++k)
                {
                    matrix[len-1-index][k] = tmatrix[len-1-k][len-1-index];
                }
                //left col
                for (k = index; k<len-index; ++k)
                {
                    matrix[k][index] = tmatrix[len-1-index][k];
                }
            }
         }
};

int main()
{
    return 0;
}
