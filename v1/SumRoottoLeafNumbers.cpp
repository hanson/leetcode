#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

/**
 ** Definition for binary tree
 **/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
    private:
        int sum;
    public:
        void dfs(TreeNode *cur, int pv)
        {
            if (cur == NULL)
            {
                return;
            }

            cur->val += pv*10;
            if (cur->left==NULL && cur->right==NULL)
            {
                sum += cur->val;
            }
            else
            {
                dfs(cur->left, cur->val);
                dfs(cur->right, cur->val);
            }
        }

        int sumNumbers(TreeNode *root) {
            sum = 0;
            dfs(root, 0);
            return sum;
        }
};

int main()
{
    return 0;
}
