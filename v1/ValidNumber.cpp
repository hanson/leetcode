#include <iostream>
#include <string>
using namespace std;

class Solution
{
    public:
        bool isdigit(char ch)
        {
            return (ch>='0' && ch<='9');
        }

        bool isNumber(const char *s) {
            //ignore head blank
            while(*s!='\0' && *s==' ')
            {
                ++s;
            }
            if (*s=='+' || *s=='-')
            {
                ++s;
            }
            if (*s == '\0')
            {
                return false;
            }
            //hasdigit
            bool hasdigit = false;
            //decimal point
            bool hasdp = false;
            //e
            bool hase = false;
            for (;*s!='\0'; ++s)
            {
                if (*s=='.')
                {
                    if (hasdp == false)
                    {
                        hasdp = true;
                    }
                    else
                    {
                        return false;
                    }
                    continue;
                }
                if (*s=='e' && hase==false)
                {
                    if (hasdigit == false)
                    {
                        return false;
                    }
                    hasdp = true;
                    if (hase==false)
                    {
                        hase = true;
                        //check;
                        ++s;
                        if (*s == '\0')
                        {
                            return false;
                        }
                        else if (*s =='.' )
                        {
                            return false;
                        }
                        else if (*s=='+' || *s=='-')
                        {
                            ++s;
                            if (isdigit(*s) == false)
                            {
                                return false;
                            }
                        }
                        else if (isdigit(*s) == false)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    continue;
                }
                if (*s == ' ')
                {
                    while (*s!='\0' && *s==' ')
                    {
                        ++s;
                    }
                    if (*s == '\0')
                    {
                        
                        return hasdigit;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (isdigit(*s) == false)
                {
                    return false;
                }

                hasdigit = true;
            }

            return hasdigit;
        }
};

int main()
{
    Solution s;
    cout<<s.isNumber(".  ")<<endl;
}
