#include <iostream>
using namespace std;

class Solution
{
    public:
        int removeDuplicates(int A[], int n) {
            if (n == 0)
            {
                return 0;
            }
            
            int oc = 1;
            int ret = 1;
            for (int p = 1; p < n; ++p)
            {
                if (A[p]==A[ret-1] && oc>=2)
                {
                    continue;
                }
                else if (A[p]==A[ret-1])
                {
                    ++oc;
                    ++ret;
                    A[ret-1] = A[p];
                }
                else
                {
                    oc = 1;
                    ++ret;
                    A[ret-1] = A[p];
                }
            }

            return ret;
        }
};

int main()
{
    return 0;
}

