#include <iostream>
#include <cmath>

using namespace std;

class Solution
{
    public:
            
        int divide(long long dividend, long long divisor) {
            if (divisor == 1)
            {
                return dividend;
            }
            if (divisor == -1)
            {
                return -dividend;
            }

            long long ret = 0;

            bool neg = false;
            neg = (dividend<0)^(divisor<0);
            
            dividend = abs(dividend);
            divisor = abs(divisor);

            for (; dividend >= 0;)
            {
                long long td = divisor;
                long long tr = 1;
                while (td < dividend)
                {
                    td<<=1;
                    tr<<=1;
                }
                if (td != dividend)
                {
                    td>>=1;
                    tr>>=1;
                }
                dividend -= td;
                ret += tr;
            }

            if (neg)
                ret = -ret;

            return ret;
        }
};

int main()
{
    Solution s;
    long long a, b;
    while(cin>>a>>b)
    {
        cout<<s.divide(a, b)<<endl;
    }
    return 0;
}

