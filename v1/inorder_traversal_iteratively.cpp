#include <vector>
#include <iostream>
using namespace std;
/**
 * Definition for binary tree
 */

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
 
class Solution {
public:
    vector<int> treeArr;

    void getTreeArr(TreeNode *root)
    {
        if (root == NULL)
        {
            return;
        }
   
        getTreeArr(root->left);
        treeArr.push_back(root->val);
        getTreeArr(root->right);
    }

    void getTreeArr_interatively(TreeNode *root)
    {
        vector<TreeNode*> stack;

        TreeNode* cur = root;
        while(cur != NULL || stack.empty() == false)
        {
            while (cur != NULL)
            {
                stack.push_back(cur);
                cur = cur->left;
            }
            if (stack.empty() == false)
            {
                cur = stack[ stack.size() - 1 ];
                treeArr.push_back(cur->val);
                
                stack.pop_back();
                cur = cur->right;
            }
        }
    }

    vector<int> inorderTraversal(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        treeArr.clear();

        getTreeArr_interatively(root);

        return treeArr;
    }
};

int main()
{
    Solution S;
    
    TreeNode root(3);

    TreeNode t(1);
    root.left = &t;

    TreeNode r(2);
    root.right = &r;
    vector<int> ans = S.inorderTraversal(&root);
    for (int i = 0; i < ans.size(); ++i)
    {
        cout<<ans[i]<<endl;
    }
    return 0;
}

