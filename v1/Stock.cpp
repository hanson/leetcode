#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
        int maxProfit(vector<int> &prices) {
            if (prices.size() == 0)
                return 0;

            int ret = 0;
        
            int stackHead = prices[0];
            for (int i = 1; i < prices.size(); ++i)
            {
                int p;
                if ((p = prices[i] - stackHead) > ret)
                {
                    ret = p;
                }
                if (p < 0)
                {
                    stackHead = prices[i];
                }
            }

            return ret;
        }
};

int main()
{
}

