#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
            int row = obstacleGrid.size();
            if (row == 0)
            {
                return 0;
            }
            int col = obstacleGrid[0].size();
            if (col == 0)
            {
                return 0;
            }
            
            vector<vector<int> >dp;
            for (int k = 0; k < row; ++k)
            {
                vector<int> tmp(col);
                dp.push_back(tmp);
            }
            if (obstacleGrid[0][0] == 1)
            {
                return 0;
            }
            dp[0][0] = 1;
            for (int r = 1; r < row; ++r)
            {
                dp[r][0] = 0;
                if (obstacleGrid[r][0] != 1)
                {
                    dp[r][0] = dp[r-1][0];
                }
            }
            for (int c = 1; c < col; ++c)
            {
                dp[0][c] = 0;
                if (obstacleGrid[0][c] != 1)
                {
                    dp[0][c] = dp[0][c-1];
                }
            }
            for (int r = 1; r < row; ++r)
            {
                for (int c = 1; c < col; ++c)
                {
                    dp[r][c] = 0;
                    if (obstacleGrid[r][c] != 1)
                    {
                        dp[r][c] = dp[r-1][c] + dp[r][c-1];
                    }
                }
            }

            return dp[row-1][col-1];
        }
};

int main()
{
    return 0;
}
