#include <iostream>
#include <queue>
#include <vector>
#include <algorithm>
using namespace std;
/**
 * Definition for binary tree
 */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    //tq2 is empty
    vector<int> getLevel(queue<TreeNode *> &tq1, queue<TreeNode *> &tq2)
    {
        TreeNode *head;
        vector<int> yu;
        yu.clear();

        while (tq1.empty() == false)
        {
            head = tq1.front();
            yu.push_back(head->val);
            tq1.pop();
            if (head->left != NULL)
            {
                tq2.push(head->left);
            }
            if (head->right != NULL)
            {
                tq2.push(head->right);
            }
        }

        return yu;
    }

    void zigzag(vector<vector<int> > &vec)
    {
        for (int index = 0; index < vec.size(); ++index)
        {
           vector<int> &elem = vec[index];

           if (index % 2 != 0)
           {
               for (int k = 0; k < elem.size()/2; ++k)
               {
                    int temp = elem[k];
                    int op = elem.size()-1-k;
                    elem[k] = elem[ op ];
                    elem[op] = temp;
               }
           }
        }

    }

    vector<vector<int> > zigzagLevelOrder(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        
        vector<vector<int> > ret;
        ret.clear();

        if (root == NULL)
            return ret;

        queue<TreeNode *> levelQue1;
        queue<TreeNode *> levelQue2;
        levelQue1.push(root);

        while (levelQue1.empty() == false || levelQue2.empty() == false)
        {
            vector<int> retVal;
            
            if (levelQue1.empty() == true)
            {
               retVal = getLevel(levelQue2, levelQue1);
            }
            else
            {
               retVal = getLevel(levelQue1, levelQue2);
            }

            if (retVal.empty() == false)
            {
                ret.push_back(retVal);
            }
            else
            {
                break;
            }
        }

        zigzag(ret);

        return ret;
    }
};

int main()
{
    Solution S;
  

    TreeNode root(3);

    TreeNode t(1);
    root.left = &t;

    TreeNode r(2);
    root.right = &r;
    vector<vector<int> > ans = S.zigzagLevelOrder(&root);
    for (int i = 0; i < ans.size(); ++i)
    {
	vector<int> val = ans[i];
	for (int j = 0; j < val.size(); ++j)
	{        
		cout<<val[j]<<endl;
	}
    }
    return 0;
}


