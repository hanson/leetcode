#include <iostream>
using namespace std;

class Solution
{
    public:
        int reverse(int x) {
            int flag = 1;
            if (x == 0)
            {
                return x;
            }
            else if (x < 0)
            {
                flag = -1;
                x = -x;
            }
            int ret=0;
            
            while (x)
            {
                ret *= 10;
                ret += (x%10);
                x /= 10;
            }

            return flag*ret;
        }
};

int main()
{
    return 0;
}
