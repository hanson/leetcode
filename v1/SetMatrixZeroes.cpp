#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
        void checkcol(vector<vector<int> > &matrix, int curcol, int cols)
        {
            if (curcol == cols)
            {
                return;
            }

            bool flag = false;
            for (int r = 0; r < matrix.size(); ++r)
            {
                if (matrix[r][curcol] == 0)
                {
                    flag = true;
                    break;
                }
            }
            checkcol(matrix, curcol+1, cols);

            if (flag == false)
            {
                return;
            }

            for (int r = 0; r < matrix.size(); ++r)
            {
                matrix[r][curcol] = 0;
            }
        }

        void checkrow(vector<vector<int> > &matrix, int currow, int rows)
        {
            if (currow == rows)
            {
                int cols;
                if ((cols=matrix[0].size()) > 0)
                {
                    checkcol(matrix, 0, cols);
                }
                 
                return;
            }
            bool flag = false;
            for (int k = 0; k < matrix[0].size(); ++k)
            {
                if (matrix[currow][k] == 0)
                {
                    flag = true;
                    break;
                }
            }
            checkrow(matrix, currow+1, rows);
            if (flag == true)
            {
                for (int k = 0; k < matrix[0].size(); ++k)
                {
                    matrix[currow][k] = 0;
                }
            }
        }

        void setZeroes(vector<vector<int> > &matrix) {
            if (matrix.size() > 0)
            {
                checkrow(matrix, 0, matrix.size());
            }
        }
};

int main()
{
    return 0;
}

