#include <iostream>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}     
};

class Solution
{
    public:
    vector<int> inorder;
    vector<int> postorder;

        TreeNode *buildLeftSubTree(int left, int right, int rootIndex)
        {
            if (left > right)
            {
                return NULL;
            }

            TreeNode *curNode = (TreeNode*)malloc(sizeof(TreeNode));
            curNode->val = postorder[rootIndex];
            int curIndex;
            for (int i = left; i <= right; ++i)
            {
                if (inorder[i] == curNode->val)
                {
                    curIndex = i;
                    break;
                }
            }

            curNode->left = buildLeftSubTree(left, curIndex-1, rootIndex-1-(right-curIndex));
            curNode->right = buildRightSubTree(curIndex+1, right, rootIndex-1);
            
            return curNode;
        }

        TreeNode *buildRightSubTree(int left, int right, int rootIndex)
        {
            if (left > right)
            {
                return NULL;
            }
            
            TreeNode *curNode = (TreeNode*)malloc(sizeof(TreeNode));
            curNode->val = postorder[rootIndex];
            int curIndex;
            for (int i = left; i <= right; ++i)
            {
                if (inorder[i] == curNode->val)
                {
                    curIndex = i;
                    break;
                }
            }

            curNode->left = buildLeftSubTree(left, curIndex-1, rootIndex-1-(right-curIndex));
            curNode->right = buildRightSubTree(curIndex+1, right, rootIndex-1);
            
            return curNode;
        }

        TreeNode *buildTree(vector<int> &tinorder, vector<int> &tpostorder) {
            if (tinorder.size() == 0)
                return NULL;

            inorder.clear();
            postorder.clear();
            for (int i = 0; i < tinorder.size(); ++i)
            {
                inorder.push_back(tinorder[i]);
                postorder.push_back(tpostorder[i]);
            }

            TreeNode* root = (TreeNode*)malloc(sizeof(TreeNode));
            root->val = postorder[ postorder.size()-1 ];
            
            int rootIndex;
            for (int i = 0; i < inorder.size(); ++i)
            {
                if (inorder[i] == root->val)
                {
                    rootIndex = i;
                    break;
                }
            }

            root->left = buildLeftSubTree(0, rootIndex-1, rootIndex-1 );
            root->right = buildRightSubTree(rootIndex+1, inorder.size()-1, inorder.size()-1-1);
    
            return root;
        }
};

int main()
{
    vector<int> in;
    in.push_back(1);
    in.push_back(3);
    in.push_back(2);

    vector<int> out;
    out.push_back(3);
    out.push_back(2);
    out.push_back(1);

    Solution S;
    TreeNode * root = S.buildTree(in, out);
    cout<<root->val<<endl;
    while (root != NULL)
    {
        cout<<root->val<<endl;
        root = root->left;
    }
    return 0;
}

