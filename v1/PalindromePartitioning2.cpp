#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
    private:
        int *dp;
        int **isP;
    public:
        void init(int n)
        {
            dp = new int[n+1];
            isP = new int*[n];
            for (int i = 0; i < n; ++i)
            {
                isP[i] = new int[n];
            }
        }
        int minCut(string s) {
            int n = s.length();
            init( n );
            for (int i = 0; i <= n; ++i)
            {
                dp[i] = n-i;
            }

            for (int i = 0; i < n; ++i)
            {
                for (int k = i; k < n; ++k)
                {
                    isP[i][k] = 0;
                }
                isP[i][i] = 1;
            }

            for (int index = n-1; index >= 0; --index)
            {
                for (int k = index; k < n; ++k)
                {
                    if (s[k]==s[index] && (k-index<2 || isP[index+1][k-1]==1))
                    {
                        isP[index][k] = 1;
                        dp[index] = min(dp[index], 1+dp[k+1]);
                    }
                }
            }

            return dp[0] - 1;
        }
};

int main()
{
    return 0;
}
