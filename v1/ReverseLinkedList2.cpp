#include <iostream>
using namespace std;

/**
 ** Definition for singly-linked list.
 **/
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
    public:
        ListNode *reverse(ListNode *head, int till)
        {
            if (till == 1)
            {
                return head;
            }

            int index = 2;
            ListNode *cur = head->next;
            ListNode *pre = head;
            ListNode *tmp;
            while (index <= till)
            {
                tmp = cur->next;
                cur->next = pre;
                pre = cur;
                cur = tmp;
                ++index;
            }

            head->next = cur;
            head = pre;
            return head;
        }

        ListNode *reverseBetween(ListNode *head, int m, int n) {
            ListNode *walker = head;
            if (m == 1)
            {
                head = reverse(head, n-m+1);
                return head;
            }

            int index = 1;
            while (walker != NULL)
            {
                if (index == m-1)
                {
                    break;
                }
                walker = walker->next;
                ++index;
            }

            walker->next = reverse(walker->next, n-m+1);

            return head;
        }
};

int main()
{
    return 0;
}

