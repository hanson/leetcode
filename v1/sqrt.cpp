#include <iostream>
#include <cmath>
using namespace std;

class Solution
{
    public:
            
        int sqrt(int x) {
            double ans = 1;
            while ( abs(ans*ans - x) > 0.1)
            {
                ans = (ans + x/ans)/2;
            }

            return (int)ans;
        }
};

int main()
{
    return 0;
}
