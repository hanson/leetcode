#include <iostream>
#include <string>
#include <vector>
using namespace std;

      
bool cmp(string str1, string str2)
{
    return str1.length() < str2.length();
}

class Solution
{
    public:

        string get_lcp(string st1, string st2)
        {
            string lcp = "";
            int i1 = 0;
            int i2 = 0;
            while (i1 < st1.length() && i2 < st2.length())
            {
                if (st1[i1] == st2[i2])
                {
                    lcp += st1[i1];
                }
                else
                {
                    break;
                }
                ++i1;
                ++i2;
            }

            return lcp;
        }

        string longestCommonPrefix(vector<string> &strs) {
            sort(strs.begin(), strs.end(), cmp);

            if (strs.size() == 0)
            {
                return "";
            }

            string lp = strs[0];
            for (int i = 1; i < strs.size(); ++i)
            {
                lp = get_lcp(lp, strs[i]);
            }

            return lp;
        }
};

int main()
{
    vector<string> strs;
    strs.push_back("qwert");
    strs.push_back("qrt");
    strs.push_back("qt");
    strs.push_back("qt123");
    strs.push_back("qt34567");

    Solution s;
    cout<<s.longestCommonPrefix(strs)<<endl;

    return 0;
}

