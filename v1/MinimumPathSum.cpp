#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class Solution
{
    public:
        int minPathSum(vector<vector<int> > &grid) {
            int r, c;
            for (r = 0; r < grid.size(); ++r)
            {
                for (c = 0; c < grid[r].size(); ++c)
                {
                    int temp = -1;
                    if (c > 0)
                    {
                        temp = grid[r][c-1];
                    }

                    if (r > 0)
                    {
                        if (temp == -1)
                        {
                            temp = grid[r-1][c];
                        }
                        else if (temp > grid[r-1][c])
                        {
                            temp = grid[r-1][c];
                        }
                    }
                    if (temp != -1)
                    {
                        grid[r][c] += temp;
                    }
                }
            }

            return grid[r-1][c-1];
        }
};

int main()
{
    return 0;
}

