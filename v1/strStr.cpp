#include <iostream>
#include <string.h>
using namespace std;

class Solution
{
    public:
        int *next;

        void getNext(char *str)
        {
            next[0] = -1;
            int j = -1;
            for (int i = 1; i < strlen(str); ++i)
            {
                while (j >= 0 && str[j+1]!=str[i])
                {
                    j = next[j];
                }

                if (str[j+1] == str[i])
                {
                    ++j;
                }

                next[i] = j;
            }
        }

        char *kmpMatch(char *haystack, char *needle)
        {
            int j = -1;
            for (int i = 0; i < strlen(haystack); ++i)
            {
                while (j >= 0 && haystack[i] != needle[j+1])
                {
                    j = next[j];
                }

                if (needle[j+1] == haystack[i])
                {
                    ++j;
                }

                if (j+1 == strlen(needle))
                {
                    return haystack + (i-j);
                }
            }

            return NULL;
        }

        void outNext(int len)
        {
            for (int i = 0; i < len; ++i)
            {
                cout<<" "<<next[i];
            }
            cout<<endl;
        }

        //kmp
        char *strStr(char *haystack, char *needle) {
            if (needle == NULL || haystack == NULL)
            {
                return NULL;
            }
            int lenNeedle = strlen(needle);
            int lenH = strlen(haystack);
            if (lenNeedle == 0)
            {
                return haystack;
            }
            if (lenNeedle > lenH)
            {
                return NULL;
            }

            next = (int*)malloc(sizeof(int) * (lenNeedle+1));
            getNext(needle);
            return kmpMatch(haystack, needle);
        }
};

int main()
{
    Solution s;
    char j[2];
    j[0] = 'a';
    j[1] = '\0';
    cout<<s.strStr(j, j)<<endl;
    return 0;
}

