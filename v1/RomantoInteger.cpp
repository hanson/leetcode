#include <iostream>
#include <map>
using namespace std;

class Solution
{
    private:
        map<char, int> dic;
    public:
        Solution()
        {
            dic.insert(map<char, int>::value_type('I', 1));
            dic.insert(map<char, int>::value_type('V', 5));
            dic.insert(map<char, int>::value_type('X', 10));
            dic.insert(map<char, int>::value_type('L', 50));
            dic.insert(map<char, int>::value_type('C', 100));
            dic.insert(map<char, int>::value_type('D', 500));
            dic.insert(map<char, int>::value_type('M', 1000));
        }

        int RomantoSign(string s, int i)
        {
            if (i == s.length() - 1)
            {
                return 1;
            }
            else if (dic[s[i]] < dic[s[i + 1]])
            {
                return -1;
            }

            return 1;
        }

        int romanToInt(string s) {
            int num = 0;
            for (int i = 0; i < s.length(); i++)
            {
                num += RomantoSign(s, i) * dic[s[i]];
            }

            return num;
        }
};

int main()
{
    return 0;
}

