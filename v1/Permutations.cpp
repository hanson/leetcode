#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    private:
        int n;
        vector<vector<int> >pms;
    public:
        void genpms(int cur, vector<int> num)
        {
            if (cur >= num.size())
            {
                pms.push_back(num);
                return;
            }
            for (int i = cur; i < num.size(); ++i)
            {
                swap(num[cur], num[i]);
                genpms(cur+1, num);
                swap(num[cur], num[i]);
            }
        }

        vector<vector<int> > permute(vector<int> &num) {
             pms.clear();
             genpms(0, num);
             return pms;
        }
};

int main()
{
    return 0;
}

