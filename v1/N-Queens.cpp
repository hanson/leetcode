#include <iostream>
#include<stdlib.h>
#include <vector>
#include <string>
using namespace std;

class Solution
{
    private:
        vector<vector<string> > pls;
        int* vcol;
        int* vdg;
        int* vadg;
        int n;
    public:
        int getdg(int row, int col)
        {
            return (row+col-1);
        }

        int getadg(int row, int col)
        {
            return (row-col + n);
        }

        void dfs(int row, vector<string> tmp)
        {
            if (row > n)
            {
                pls.push_back(tmp);
                return;
            }

            for (int col = 1; col <= n; ++col)
            {
                if (vcol[col]==1 || vdg[getdg(row, col)]==1 || vadg[getadg(row, col)]==1)
                {
                    continue;
                }
                
                vcol[col] = 1;
                vdg[getdg(row, col)] = 1;
                vadg[getadg(row, col)] = 1;
                string ts;
                for (int k = 1; k <= n; ++k)
                {
                    if (k == col)
                    {
                        ts.append("Q");
                    }
                    else
                    {
                        ts.append(".");
                    }
                }
                tmp.push_back(ts);
                dfs(row+1, tmp);
                tmp.pop_back();
                vcol[col] = 0;
                vdg[getdg(row, col)] = 0;
                vadg[getadg(row, col)] = 0;
            }
        }

        vector<vector<string> > solveNQueens(int tn) {
            pls.clear();
            vcol = (int*)malloc((tn+2)*sizeof(int));
            vdg = (int*)malloc(sizeof(int) * (2*tn+2));
            vadg = (int*)malloc(sizeof(int) * (2*tn+2));
            for (int k = 1; k <= tn; ++k)
            {
                vcol[k] = 0;
            }
            for (int k = 1; k <= 2*tn+1; ++k)
            {
                vdg[k] = vadg[k] = 0;
            }
            vector<string> tmp;
            n = tn;
            tmp.clear();
            dfs(1, tmp);
            return pls;
        }
};
