#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:    
        int longestConsecutive(vector<int> &num) {
            int len = num.size();
            if (len == 0)
            {
                return 0;
            }

            map<int, bool> nums;
            for (int i = 0; i < len; ++i)
            {
                nums.insert(pair<int,bool>(num[i], false));
            }
            
            map<int, bool>::iterator it;
            int ret = 0;
            int tmp = 0;
            for (it = nums.begin(); it != nums.end(); ++it)
            {
                if (it->second == true)
                    continue;
                tmp = 1;
                it->second = true;
                int succ = it->first + 1;
                int pre = it->first - 1;
                map<int, bool>::iterator tmpit;
                while ((tmpit=nums.find(pre)) != nums.end())
                {
                    --pre;
                    ++tmp;
                    tmpit->second = true;
                }
                while ((tmpit=nums.find(succ)) != nums.end())
                {
                    ++succ;
                    ++tmp;
                    tmpit->second = true;
                }

                ret = ret>tmp?ret:tmp;
            }

            return ret;
        }
};

int main()
{
    return 0;
}
