#include <algorithm>
#include <iostream>
using namespace std;

class Solution
{
    private:
        int pleft;
        int pright;

    public:
        void checkleft(int cur, int A[], int &n, int elem)
        {
            if (cur < 0)
            {
                pleft = 0;
                return;
            }
            else if (A[cur] == elem)
            {
                int k = cur;
                while (k>=0 && A[k]==elem)
                {
                    --k;
                }
                
                pleft = k + 1;
                // k+1 - cur is elem
            }
        }

        void checkright(int cur, int A[], int &n, int elem)
        {
            if (cur >= n)
            {
                pright = n-1;
                return;
            }
            else if (A[cur] == elem)
            {
                int k = cur;
                while (k<=n-1 && A[k]==elem)
                {
                    ++k;
                }
                pright = k-1;
                //mid - k-1 is elem
            }
        }
        
        void check(int cur, int A[], int& n, int elem)
        {
            checkleft(cur, A, n, elem);
            checkright(cur, A, n, elem);
        }

        //pleft - pright
        void solve(int A[], int &n)
        {
            //pleft - pright is elem
            //pright + 1 - n-1 is not elem
            int pn = n-1;
            for (int ti = pleft; ti <= pright && pn>=pright+1; ++ti)
            {
                swap(A[ti], A[pn]);
                --pn;
            }

            n = (n - (pright-pleft+1));
        }

        int removeElement(int A[], int n, int elem) {
             if (n == 0)
                 return 0;
             sort(A, A+n);
                pleft = -1;
                pright = -1;
              int left = 0;
            int right = n-1;
            while (left <= right)
            {
                int mid = (left+right)/2;
                if (A[mid] == elem)
                {
                    check(mid, A, n, elem);
                    solve(A, n);
                    return n;
                }
                else if (A[mid] < elem)
                {
                    left = mid+1;
                }
                else
                {
                    right = mid - 1;
                }
            }

            return n;
        }
};
