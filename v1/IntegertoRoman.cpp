#include <iostream>
#include <string>
#include <map>
#include <vector>
using namespace std;

class Solution
{
    private:
    vector<string> roman_symbol;

    public:
        Solution()
        {
            roman_symbol.push_back("I");   
            roman_symbol.push_back("V");   
            roman_symbol.push_back("X");   
            roman_symbol.push_back("L");   
            roman_symbol.push_back("C");   
            roman_symbol.push_back("D");   
            roman_symbol.push_back("M");   
        }

        string get_roman_val(int num, int loc)
        {
            if (num == 0)
            {
                return "";
            }
            else if (num == 1)
            {
                return roman_symbol[loc];
            }
            else if (num == 2)
            {
                return roman_symbol[loc]+roman_symbol[loc];
            }
            else if (num == 3)
            {
                return roman_symbol[loc] + roman_symbol[loc]+roman_symbol[loc];
            }
            else if (num == 4)
            {
                return roman_symbol[loc] + roman_symbol[loc+1];
            }
            else if (num == 5)
            {
                return roman_symbol[loc+1];
            }
            else if (num == 6)
            {
                return roman_symbol[loc+1] + roman_symbol[loc];
            }
            else if (num == 7)
            {
                return roman_symbol[loc+1] + roman_symbol[loc] + roman_symbol[loc];
            }
            else if (num == 8)
            {
                return roman_symbol[loc+1] + roman_symbol[loc] + roman_symbol[loc] + roman_symbol[loc];
            }
            else if (num == 9)
            {
                return roman_symbol[loc] + roman_symbol[loc+2];
            }
        }

        string intToRoman(int num)
        {
            string roman_val;

            int temp;
            int loc = 0;
            while (num > 0)
            {
                temp = num % 10;
                num = num / 10;
                roman_val.insert(0, get_roman_val(temp, loc));
                loc += 2;
            }

            return roman_val;    
        }
};

int main()
{
    return 0;
}

