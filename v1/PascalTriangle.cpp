#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
        vector<vector<int> > generate(int numRows) {
            vector<vector<int> >pt;
            for (int i = 0; i < numRows; ++i)
            {
                vector<int> ele;
                for (int k = 0; k <= i; ++k)
                {
                    if (k == 0)
                    {
                        ele.push_back(1);
                    }
                    else if (k == i)
                    {
                        ele.push_back(1);
                    }
                    else
                    {
                        ele.push_back( pt[i-1][k-1]+pt[i-1][k]);
                    }
                }

                pt.push_back(ele);
            }

            return pt;
        }
};

int main()
{
    return 0;
}


