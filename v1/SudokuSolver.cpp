#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        bool isvalid(const vector<char>& vec) {
            vector<bool> occur(9, false);
            for (int i = 0; i < 9; ++i) {
                if (vec[i] == '.')
                    continue;
                if (occur[vec[i]-'1']) 
                    return false;
                else 
                    occur[vec[i]-'1'] = true;
            }
   
            return true;
        }

        bool checkvalid(vector<vector<char> > &board, int r, int c)
        {
            //check row
            if (isvalid(board[r]) == false)
            {
                return false;
            }
            vector<char> chs(9);
            //check col
            for (int i = 0; i < 9; ++i)
            {
                chs[i] = (board[i][c]);
            }
            if (isvalid(chs) == false)
            {
                return false;
            }
            vector<char> chs2;
            //check grid
            int gr = r / 3;
            int gc = c / 3;
            for (int tr = gr*3; tr < gr*3 + 3; ++tr)
            {
                for (int tc = gc*3; tc < gc*3 + 3; ++tc)
                {
                    chs2.push_back( (board[tr][tc]) );
                }
            }
            if (isvalid(chs2) == false)
            {
                return false;
            }
            
            return true;
        }

        bool solve(vector<vector<char> > &board)
        {
            for (int r = 0; r < 9; ++r)
            {
                for (int c = 0; c < 9; ++c)
                {
                    if (board[r][c] == '.')
                    {
                        for (int val = 1; val <= 9; ++val)
                        {
                            board[r][c] = val + '0';
                            if (checkvalid(board, r, c) && solve(board))
                            {
                                return true;
                            }
                        }
                        board[r][c] = '.';
                        return false;
                    }
                }
            }

            return true;
        }

        void solveSudoku(vector<vector<char> > &board) {
            solve(board);
        }
};
