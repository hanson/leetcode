#include <iostream>
using namespace std;

/**
 *  * Definition for singly-linked list.
 **/
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
    public:
        ListNode *head;
        ListNode *cur;
    
        void add2List(int val)
        {
            if (cur == NULL)
            {
                cur = new ListNode(val);
                head = cur;
            }
            else
            {
                cur->next = new ListNode(val);
                cur = cur->next;
            }
        }
        ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
            head = NULL;
            cur = head;

            while(l1!=NULL && l2!=NULL)
            {
               if (l1->val <= l2->val)
               {
                   add2List(l1->val); 
                   l1 = l1->next;
               }
               else
               {
                   add2List(l2->val); 
                    l2 = l2->next;
               }
            }

            while(l1!=NULL)
            {
                add2List(l1->val); 
                l1 = l1->next;
            }
            while(l2!=NULL)
            {
                add2List(l2->val); 
                l2 = l2->next;
            }

            return head;
        }
};

int main()
{
    return 0;
}

