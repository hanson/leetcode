#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <stdlib.h>
#include <functional>
#include <queue>
#include <map>

using namespace std;

class Solution
{
    private:
        class Step
        {
            public:
            int trans;
            string str;
            vector<string> seqs;

            Step(int t, string s)
            {
                seqs.clear();
                trans = t;
                str = s;
            }

            bool ishas(string str)
            {
                for (int i = 0; i < seqs.size(); ++i)
                {
                    if (str == seqs[i])
                    {
                        return true;
                    }
                }
                return false;
            }
        };
    public:
        vector<vector<string> > findLadders(string start, string end, unordered_set<string> &dict) {
            vector<vector<string> > ret;
            queue<Step*> que;
            int sentinel = -1;
            while (!que.empty())
            {
                que.pop();
            }
            Step *head;
            Step *tt = new Step(1, start);
            tt->seqs.push_back(start);
            que.push(tt);
            while (que.empty() == false)
            {
                head = que.front();
                que.pop();
                int cs = head->trans;
                string str = head->str;
                if (sentinel!=-1 && cs>sentinel)
                {
                    break;
                }
                if (sentinel == cs && str==end)
                {
                    ret.push_back(head->seqs);
                }

                for (int k = 0; k < str.length(); ++k)
                {
                    for (char ch = 'a'; ch <= 'z'; ++ch)
                    {
                        if (str[k] == ch)
                            continue;
                        string cur(str);
                        cur[k] = ch;
                        if (cur == end)
                        {
                            if (sentinel == -1)
                                sentinel = cs + 1;
                        }

                       if (dict.count(cur))
                        {
                            if (head->ishas(cur) == false)
                            {
                                Step *tmp = new Step(cs+1, cur);
                                for (int i = 0; i < head->seqs.size(); ++i)
                                {
                                    tmp->seqs.push_back(head->seqs[i]);
                                }
                                tmp->seqs.push_back(cur);
                                que.push(tmp);
                            }
                        }
                    }
                }
            }

            return ret;
        }
};

