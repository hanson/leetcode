#include <iostream>
using namespace std;

class Solution
{
    private:
        int num[10];
        string perm;
    public:
        string getPermutation(int n, int k) {
            int fac = 1;
            for (int i = 0; i < n; ++i)
            {
                fac *= (i+1);
                num[i] = i + 1;
            }
            
            --k;
            k %= fac;
            fac /= n;
            perm = "";
            for (int i = 0; i < n-1; ++i)
            {
                perm.push_back('0' + num[ k/fac + i ]);

                for (int loop = k/fac+i; loop > i; --loop)
                {
                    num[loop] = num[loop-1];
                }

                k %= fac;
                fac /= (n-i-1);
            }
            
            perm.push_back('0'+num[n-1]);

            return perm;
        }
};

