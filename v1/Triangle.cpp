#include <algorithm>
#include  <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
            
        int minimumTotal(vector<vector<int> > &triangle) {
            int len = triangle.size();
            if (len == 0)
                return 0;

            for (int k = len-2; k >= 0; --k)
            {
                for (int i = 0; i < triangle[k].size(); ++i)
                {
                    triangle[k][i] += min(triangle[k+1][i], triangle[k+1][i+1]);
                }
            }

            return triangle[0][0];
        }
};

int main()
{
    return 0;
}

