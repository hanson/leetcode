#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:   
        vector<int> searchRange(int A[], int n, int target) {
            vector<int> r;
            r.clear();
            for (int i = 0; i < n; ++i)
            {
                if (A[i] == target)
                {
                    if (r.size() == 2)
                    {
                        r.pop_back();
                    }

                    r.push_back(i);
                }
            }

            if (r.size() == 1)
            {
                r.push_back(r[0]);
            }

            if (r.empty() == true)
            {
                r.push_back(-1);
                r.push_back(-1);
            }

            return r;
        }
};

int main()
{
    return 0;
}

