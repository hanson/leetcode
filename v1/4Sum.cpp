#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:

    int binSearch(int c, vector<int> num, int start)
    {
        int left = start;
        int right = num.size()-1;
        int mid;
        while (left <= right)
        {
            mid = (left + right)/2;

            if (num[mid] == c)
            {
                return mid;
            }
            else if (num[mid] > c)
            {
                right = mid - 1;
            }
            else
            {
                left = mid + 1;
            }
        }

        return -1;
    }

  vector<vector<int> > fourSum(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        sort(num.begin(), num.end());
        int it_a;
        int it_b;
        int it_c;
        int it_d;
        vector<vector<int> > trips;

        for (it_a = 0; it_a < num.size(); ++it_a)
        {
            if (it_a != 0)
            {
                while (it_a < num.size() && num[it_a] == num[it_a - 1])
                {
                    ++it_a;
                }
            }
            if (it_a == num.size())
            {
                break;
            }
            for (it_b = it_a+1; it_b < num.size(); ++it_b)
            {
                if (it_b != (it_a+1))
                {
                    while (it_b < num.size() && num[it_b] == num[it_b-1])
                    {
                        ++it_b;
                    }
                }
                if (it_b == num.size())
                {
                    break;
                }
                it_c = it_b + 1;
                if (it_c < num.size())
                {
                    if (it_c != (it_b+1))
                    {
                        while (it_c < num.size() && num[it_c] == num[it_c-1])
                        {
                            ++it_c;
                        }
                    }
                    if (it_c == num.size())
                    {
                        break;
                    }
                    
                    it_d = num.size() - 1;
                    if (it_d > it_c)
                    {
                        if (it_d != num.size() - 1)
                        {
                            while (it_d > it_c && num[it_d] == num[it_d+1])
                            {
                                --it_d;
                            }
                        }
                        if (it_d <= it_c)
                        {
                            continue;
                        }
                        //
                        int low = it_c;
                        int high = it_d;
                        int sum = num[it_a]+num[it_b];

                        while (low < high)
                        {
                            sum = num[it_a] + num[it_b] + num[low]+num[high];

                            if (sum == target)
                            {
                                vector<int> trip;
                                trip.push_back(num[it_a]);
                                trip.push_back(num[it_b]);
                                trip.push_back(num[low]);
                                trip.push_back(num[high]);
                                trips.push_back(trip);
                                ++low;
                                while (low < num.size() && num[low] == num[low-1])
                                {
                                    ++low;
                                }
                                --high;
                                while (high > 0 && num[high] == num[high + 1])
                                {
                                    --high;
                                }

                            }
                            else if (sum > target)
                            {
                                --high;
                            }
                            else
                            {
                                ++low;
                            }
                        }
                    }
                }
            }
        }
        
        return trips;
    }
};

int main()
{
    Solution s;
    vector<int> test;
    int k = 3;
    while (k--)
    {
    for (int i = 5; i >= -5; --i)
    {
        test.push_back(i);
    }
    }
    vector<vector<int> >ret = s.fourSum(test, 10);
    vector<vector<int> >::iterator it;
    for (it = ret.begin(); it != ret.end(); ++it)
    {
        vector<int> trip = *it;
        vector<int>::iterator pit;
        for (pit = trip.begin(); pit != trip.end(); ++pit)
        {
            cout<<*pit<<" ";
        }
        cout<<endl;
    }
}

