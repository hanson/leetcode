#include <iostream>
#include <stack>
using namespace std;

class Solution
{
    private:
        stack<int> pstack;
    public:
        int longestValidParentheses(string s) {
            while (pstack.empty() == false)
            {
                pstack.pop();
            }

            int ans = 0;
            int st = s.length();
            for (int i = 0; i < s.length(); ++i)
            {
                if (s[i] == '(')
                {
                    pstack.push(i);
                }
                //")"
                else if (pstack.empty() == false)
                {
                    int tmp = pstack.top();
                    pstack.pop();
                    st = st < tmp ? st : tmp;
                    if (pstack.empty() == true)
                    {
                        ans = ans > (i-st+1) ? ans : (i-st+1);
                    }
                    else
                    {
                        ans = ans > (i-pstack.top()) ? ans : (i-pstack.top());
                    }
                }
                else
                {
                    st = i + 1;
                }
            }
            
            return ans;
        }
};

int main()
{
    return 0;
}

