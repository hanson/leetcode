#include <iostream>
using namespace std;

 // Definition for binary tree
 struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };
 
class Solution {
public:
    int ans;

    int dfs(TreeNode *curNode)
    {
        if (curNode == NULL)
            return 0;
        int temp = curNode->val;
        int left = dfs(curNode->left);
        int right = dfs(curNode->right);

        if ((temp+left+right) > ans)
        {
            ans = (temp + left+ right);
        }
        if ((temp+right) > ans)
        {
            ans = (temp + right);
        }
        if ((temp+left) > ans)
        {
            ans = (temp + left);
        }

        int t = left;
        if (left < right)
            t = right;
        if (t > 0)
        {
            temp += t;
        }

        return temp;
    }

    int maxPathSum(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL)
        {
            return 0;
        }
        ans = root->val;

        dfs(root);

        return ans;
    }
};

int main()
{

    return 0;
}

