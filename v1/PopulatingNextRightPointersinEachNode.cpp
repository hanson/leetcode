#include <iostream>
#include <queue>
using namespace std;

//Definition for binary tree with next pointer.
struct TreeLinkNode
{
    int val;
    TreeLinkNode *left, *right, *next;
    TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
};
queue<TreeLinkNode*> nodeQue;
queue<int> levelQue;

class Solution
{
    public:
        void bfs(TreeLinkNode *curNode)
        {
            if(curNode == NULL)
                return;
          
            while (nodeQue.empty() == false)
            {
                nodeQue.pop(); 
            }
            nodeQue.push(curNode);
            while (levelQue.empty() == false)
            {
                levelQue.pop();
            }
            levelQue.push(0);
            TreeLinkNode *preNode = NULL;
            int preLevel = 0;
            while (nodeQue.empty() == false)
            {
                int curLevel = levelQue.front();
                levelQue.pop();
                TreeLinkNode *curNode = nodeQue.front();
                nodeQue.pop();
                if (curLevel != preLevel)
                {
                    preLevel = curLevel;
                    if (preNode != NULL)
                        preNode->next = NULL;
                    preNode = curNode;
                }
                else
                {
                    if (preNode != NULL)
                        preNode->next = curNode;
                    preNode = curNode;
                }
                if (curNode->left != NULL)
                {
                    levelQue.push(curLevel+1);
                    nodeQue.push(curNode->left);
                }
                if (curNode->right != NULL)
                {
                    levelQue.push(curLevel+1);
                    nodeQue.push(curNode->right);
                }
            }

        }

        void connect(TreeLinkNode *root) {
            bfs(root);        
        }
};

int main()
{
    return 0;
}

