/**
 * Definition for binary tree
 */
#include <iostream>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
 
class Solution {
public:
    vector<int> nodes;
    
    TreeNode* buildBST(int left, int right)
    {
        if (left > right)
        {
            return NULL;
        }

        TreeNode* curNode = (TreeNode*)malloc(sizeof(TreeNode));
        int curIndex = left + (right - left)/2;
        curNode->val = nodes[ curIndex ];
        curNode->left = buildBST(left, curIndex-1);
        curNode->right = buildBST(curIndex+1, right);

        return curNode;
    }

    TreeNode *sortedArrayToBST(vector<int> &num) {
        TreeNode* root = NULL;
        if (num.size() == 0)
        {
            return root;
        }
        nodes.clear();
        for (int i = 0; i < num.size(); ++i)
        {
            nodes.push_back(num[i]);
        }
        root = buildBST(0, nodes.size()-1);
        
        return root;
    }
};

int main()
{
    Solution s;
    vector<int> vec;
    vec.push_back(1);
    vec.push_back(2);
    s.sortedArrayToBST(vec);
    return 0;
}

