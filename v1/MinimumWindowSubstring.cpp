#include <iostream>
#include <string>
using namespace std;

class Solution
{
    private:
        int rec[128];
        int tr[128];
    public:
        bool check()
        {
            for (int k = 0; k < 128; ++k)
            {
                if (tr[k] < rec[k])
                {
                    return false;
                }
            }

            return true;
        }

        string minWindow(string S, string T) {
            string res = "";
            int st = 0;
            int end = 0;
            for (int k = 0; k < 128; ++k)
            {
                rec[k] = 0;
            }
            for (int k = 0; k < 128; ++k)
            {
                tr[k] = 0;
            }
            for (int k = 0; k < T.length(); ++k)
            {
                ++rec[ (int)T[k] ];
            }
            ++tr[ (int)S[end] ];
            
            while (end < S.length())
            {
               if ( check() )
               {
                    if (res == "")
                    {
                        res = S.substr(st, (end-st+1));
                    }
                    else if (res.length() > (end-st+1))
                    {
                        res = S.substr(st, (end-st+1));
                    }
                    --tr[ (int)S[st] ];
                    ++st;
               }
               else
               {
                    ++end;
                    ++tr[ (int)S[end] ];
               }
            }

            return res;
        }
};

