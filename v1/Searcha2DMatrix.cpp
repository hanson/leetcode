#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
            
        bool searchMatrix(vector<vector<int> > &matrix, int target) {
            int left = 0;
            int sz = matrix.size();
            if (sz <= 0)
            {
                return false;
            }
            int len = matrix[0].size();
            int right = len*sz-1;
            while (left <= right)
            {
                int mid = (left + right)>>1;
                int tx = mid/len;
                int ty = mid%len;
                if (target == matrix[tx][ty])
                {
                    return true;
                }
                //mid < target
                else if (target > matrix[tx][ty])
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid - 1;
                }
            }
            
            return false;
        }
};

int main()
{
    return 0;
}
