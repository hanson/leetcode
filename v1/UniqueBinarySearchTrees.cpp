#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    private:
        int *trees;
    public:
        int numTrees(int n) {
            trees = (int*)malloc((1+n) * sizeof(int));
            trees[0] = 1;
            for (int i = 1; i <= n; ++i)
            {
                trees[i] = 0;
                for (int k = 0; k < i; ++k)
                {
                    trees[i] += trees[k]*trees[i-1-k];
                }
            }

            return trees[n];
        }
};

int main()
{
    return 0;
}
