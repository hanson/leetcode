#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        int uniquePaths(int m, int n) {
            if (m && n == 0)
            {
                return 0;
            }
            
            vector<vector<int> >dp;
            for (int k = 0; k < m; ++k)
            {
                vector<int> tmp(n);
                dp.push_back(tmp);
            }
            for (int r = 0; r < m; ++r)
            {
                dp[r][0] = 1;
            }
            for (int c = 0; c < n; ++c)
            {
                dp[0][c] = 1;
            }
            for (int r = 1; r < m; ++r)
            {
                for (int c = 1; c < n; ++c)
                {
                    dp[r][c] = dp[r-1][c] + dp[r][c-1];
                }
            }

            return dp[m-1][n-1];
        }
};

int main()
{
    return 0;
}
