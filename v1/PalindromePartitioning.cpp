#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
    private:
        vector<vector<string> >ps;
    public:
        vector<vector<string> > getps()
        {
            return ps;
        }
        
        void clrps()
        {
            ps.clear();
        }

        bool ispalidrome(string str)
        {
            if (str.length() == 0)
                return false;
            if (str.length() == 1)
                return true;
            
            int toleft;
            int toright;
            //odd
            if (str.length()%2 != 0)
            {
                toright = toleft = str.length()/2;
                while (toleft >= 0)
                {
                    if (str[toleft] != str[toright])
                    {
                        return false;
                    }
                    ++toright;
                    --toleft;
                }
                return true;
            }
            else//even
            {
                toright = str.length()/2;
                toleft = toright - 1;
                while (toleft >= 0)
                {
                    if (str[toleft] != str[toright])
                    {
                        return false;
                    }
                    ++toright;
                    --toleft;
                }
                return true;
            }

            return false;
        }

        void ptdfs(string ts, int index, vector<string> tmp)
        {
            if (index >= ts.length())
            {
                ps.push_back( tmp );
                return;
            }

            for (int i = index; i < ts.length(); ++i)
            {
                if (ispalidrome( ts.substr(index, (i-index+1)) )) 
                {
                    tmp.push_back( ts.substr(index, (i-index+1)) );
                    ptdfs(ts, i+1, tmp);
                    tmp.pop_back();
                }
            }
        }

        vector<vector<string> > partition(string s) {
            clrps();
            vector<string> tmp;
            tmp.clear();
            ptdfs(s, 0, tmp);

            return getps();
        }
};

