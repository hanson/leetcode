#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
        int maxProfit(vector<int> &prices) {
            if (prices.size() == 0)
                return 0;

            int ans = 0;
            vector<int> stack;
            
            stack.push_back(prices[0]);
           
            for (int i = 1; i < prices.size(); ++i)
            {
                if (stack.size() == 0)
                {
                    stack.push_back(prices[0]);
                }
                else if (stack.size() == 1)
                {
                    if (prices[i] < stack[0])
                    {
                        stack.pop_back();
                    }
                
                    stack.push_back(prices[i]);
                }
                else
                {
                    if (prices[i] >= stack[1])
                    {
                        stack.pop_back();
                        stack.push_back(prices[i]);
                    }
                    else
                    {
                        ans += (stack[1] - stack[0]);
                        stack.clear();
                        stack.push_back(prices[i]);
                    }
                }
            }

            if (stack.size() == 2)
            {
                ans += (stack[1] - stack[0]);
            }

            return ans;
        }
};

int main()
{
}

