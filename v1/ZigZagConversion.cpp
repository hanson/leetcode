#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        string convert(string s, int nRows) {
            vector<string> zigzags;
            zigzags.clear();
            for (int i = 0; i < nRows; ++i)
            {
                string str = "";
                zigzags.push_back(str);
            }

            int len = s.length();
            int index = 0;
            while(index < len)
            {
                for (int i = 0; i<nRows&&index<len; ++i)
                {
                    zigzags[i] += s[index];
                    ++index;
                }

                for (int k = nRows-2; k>0 && index<len; --k)
                {
                    zigzags[k] += s[index];
                    ++index;
                }
            }
            string ret = "";
            for (int i = 0; i < nRows; ++i)
            {
                ret.append( zigzags[i] );
            }
            return ret;
        }
};

int main()
{
    Solution s;
    s.convert("PAYPALISHIRING", 2);
    return 0;
}
