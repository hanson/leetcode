#include <iostream>
using namespace std;

/**
 ** Definition for singly-linked list.
 **/

 
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
    public:
            
        ListNode *removeNthFromEnd(ListNode *head, int n) {
            ListNode *ph;
            ListNode *pt;
            ph = pt = head;
            while (n>0)
            {
                pt = pt->next;
                --n;
            }
            ListNode *pre = NULL;
            while (pt != NULL)
            {
                pt = pt->next;
                pre = ph;
                ph = ph->next;
            }
            if (pre == NULL)
            {
                head = head->next;
            }
            else
            {
                pre->next = ph->next;
            }
            return head;
        }
};

int main()
{
    return 0;
}

