#include <iostream>
#include <vector>

using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
    private:
        vector<vector<int> >paths;
    public:

        void dfs(TreeNode *curNode, int pathSum, vector<int> path, int sum)
        {
            if (curNode == NULL)
                return;

            int curSum = pathSum + curNode->val;
            path.push_back(curNode->val);
            if (curSum==sum && curNode->left==NULL && curNode->right==NULL)
            {
                paths.push_back(path);
                return;
            }

            dfs(curNode->left, curSum, path, sum);
            dfs(curNode->right, curSum, path, sum);

            path.pop_back();
        }

        vector<vector<int> > pathSum(TreeNode *root, int sum) {
            paths.clear();
            vector<int> path;
            dfs(root, 0, path, sum);
            return paths;
        }
};

int main()
{
    return 0;
}

