#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <map>
using namespace std;

class Solution
{
    public:
        vector<int> findSubstring(string S, vector<string> &L) {
            int len = S.length();
            int lsize = L.size();
            vector<int> ret;
            int sublen;
            if (len<=0 || lsize<=0 || len<(sublen=lsize*L[0].length()))
            {
                return ret;
            }
            map<string, int>dic;
            map<string, int>::iterator it;;
            for (int i = 0; i < lsize; ++i)
            {
                if ((it=dic.find(L[i])) != dic.end())
                {
                    ++dic[ L[i] ];
                    continue;
                }

                dic.insert( pair<string, int>(L[i], 1) );
            }
            int k;
            int llen = L[0].length();
            int index = 0;
            //index to sublen-1
            while (index+sublen-1 < len)
            {
                map<string, int> tmp(dic);
                for (k = 0; k < lsize; ++k)
                {
                    string substr = S.substr(index+llen*k, llen);
                    
                    if ( (it=tmp.find(substr)) != tmp.end())
                    {
                        if (it->second >= 1)
                        {
                            --it->second;
                        }
                        if (it->second == 0)
                        {
                            tmp.erase(it);
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                
                if (k == lsize)
                {
                    ret.push_back(index);
                }
                ++index;
            }

            return ret;
        }
};

int main()
{
    Solution s;
    vector<string> tmp;
    tmp.push_back("a");
    tmp.push_back( "b" );
    s.findSubstring("aaa", tmp);
    return 0;
}
