#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

class Solution {
public:

    int binSearch(int c, vector<int> num, int start)
    {
        int left = start;
        int right = num.size()-1;
        int mid;
        while (left <= right)
        {
            mid = (left + right)/2;

            if (num[mid] == c)
            {
                return c;
            }
            else if (num[mid] > c)
            {
                right = mid - 1;
            }
            else
            {
                left = mid + 1;
            }
        }

        int ret = num[mid];
        if ((ret > c) && ( (mid - 1)>=start ) && abs(num[mid-1] - c) < abs(ret - c))
        {
            ret = num[mid - 1];
        }
        else if((ret>c) && ( (mid+1)<num.size() ) && (abs(num[mid+1]-c)<abs(ret-c)))
        {
            ret = num[ mid+1 ];
        }

        return ret;
    }

    int threeSumClosest(vector<int> &num, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int ans = 0;
        bool firstFlag = true;
        sort(num.begin(), num.end());
        for (int it_a = 0; it_a < num.size(); ++it_a)
        {
            if (it_a != 0)
            {
                while (it_a < num.size() && num[it_a] == num[ it_a-1 ])
                {
                    ++it_a;
                }
            }
            if (it_a == num.size())
            {
                break;
            } 
            for (int it_b = it_a+1; it_b < num.size(); ++it_b)
            {
                if (it_b != (it_a+1))
                {
                    while (it_b < num.size() && num[it_b] == num[it_b-1])
                    {
                        ++it_b;
                    }
                }
                if (it_b == num.size())
                {
                    break;
                }
		
                //it_b + 1;
                if (it_b + 1 != num.size())
                {
                    int c = target - (num[ it_a ] + num[ it_b ]);
                    int temp = binSearch(c, num, it_b+1);
                    
                    if (temp == c)
                    {
                        return target;
                    }
                    if (firstFlag == true)
                    {
                        firstFlag = false;
                        ans = (num[it_a] + num[it_b] + temp);
                    }
                    else if ( abs(temp-c) < abs(ans - target))
                    {
                        ans = (num[it_a] + num[it_b] + temp);
                    }
                }
            }
        }

        return ans;
    }
};

int main()
{
    Solution s;
    vector<int> test;
    int k = 1;
    while (k--)
    {
        for (int i = 5; i >= -5; --i)
        {
            test.push_back(i);
        }
    }
    
    cout<<s.threeSumClosest(test, 100)<<endl;
}

