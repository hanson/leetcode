#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
        vector<int> plusOne(vector<int> &digits) {
            int carry = 1;
            for (int i = digits.size()-1; i >= 0; --i)
            {
                int sum = digits[i] + carry;
                carry = sum / 10;
                digits[i] = sum % 10;
            }
            vector<int> ret;
            if (carry > 0)
            {
                ret.push_back(carry); 
            }
            for (int i = 0; i < digits.size(); ++i)
            {
                ret.push_back(digits[i]);
            }
            return ret;
        }
};

int main()
{
}

