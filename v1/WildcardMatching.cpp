#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        bool isMatch(const char *s, const char *p) {
            const char *st = s;
            const char *sp = p;
            const char *star = NULL;

            while (*st != '\0')
            {
               if (*sp == '*')
               {
                   s = st;
                   while (*sp == '*')
                   {
                    ++sp;
                   }
                   star = sp-1;
                   continue;
               }
                
               if (*sp == '\0')
               {
                    if (star == NULL)
                    {
                        return false;
                    }
                    ++s;
                    st = s;
                    sp = star + 1;
                    continue;
               }

               if (*sp == '?')
               {
                    ++sp;
                    ++st;
               }
               else if (*sp == *st)
               {
                    ++sp;
                    ++st;
               }
               else if (*sp != *st)
               {
                   ++s;
                   st = s;
                   if (star == NULL)
                   {
                       return false;
                   }
                   sp = star + 1;
               }
            }

            while (*sp == '*')
            {
                ++sp;
            }

            return (*sp=='\0');
        }
};

