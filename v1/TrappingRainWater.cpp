#include <iostream>
using namespace std;

class Solution
{
    public:   
        int trap(int A[], int n) {
            if (n <= 0)
            {
                return 0;
            }

            int h = 0;
            for (int i = 0; i < n; ++i)
            {
                if (A[i] > A[h])
                {
                    h = i;
                }
            }
            int ans = 0;
            int t = 0;

            for (int i = 0; i <= h; ++i)
            {
                if (A[i] > A[t])
                {
                    t = i;
                }
                else
                {
                    ans += (A[t] - A[i]);
                }
            }

            t = n - 1;
            for (int k = n-1; k >= h; --k)
            {
                if (A[k] > A[t])
                {
                    t = k;
                }
                else
                {
                    ans += (A[t] - A[k]);
                }
            }
            return ans;
        }
};

int main()
{
    return 0;
}

