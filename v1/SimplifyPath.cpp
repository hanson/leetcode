#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
    private:
        vector<string> ps;
    public:
        void handlestr(string tmp)
        {
            if (tmp == "/")
            {
                return;
            }
            if (tmp.compare("/.") == 0)
            {
                return;
            }
            if (tmp.compare("/..") == 0)
            {
                if (ps.empty() == false)
                {
                    ps.pop_back();
                }
                return;
            }
            else
            {
                ps.push_back(tmp);
            }
        }

        string simplifyPath(string path) {
            ps.clear();
            int fir, sec;
            for (fir = 0; fir < path.length(); ++fir)
            {
                if (path[fir] == '/')
                {
                    for (sec = fir+1; sec < path.length(); ++sec)
                    {
                        if (path[sec] == '/')
                        {
                            handlestr(path.substr(fir, sec-fir));
                            break;
                        }
                    }
                    if (sec == path.length())
                    {
                        handlestr(path.substr(fir));
                        break;
                    }
                    fir = sec - 1;
                }
            }
            string ret = "";
            for (int i = 0; i < ps.size(); ++i)
            {
                ret += ps[i];
            }

            return ret=="" ? "/" : ret;
        }
};

int main()
{
    Solution s;
    cout<<s.simplifyPath("///")<<endl;
    return 0;
}
