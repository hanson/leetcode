#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    private:
        vector<vector<int> >sets;
    public:
        vector<vector<int> > subsets(vector<int> &S) {
            sets.clear();
            int len = S.size();
            if (len == 0)
            {

                return sets;
            }
            sort(S.begin(), S.end()); 
            for (int k = 0; k<(1<<len); ++k)
            {
                vector<int> tmp;
                for (int i = 0; i < len; ++i)
                {
                    if (k&(1<<i))
                    {
                        tmp.push_back(S[i]);
                    }
                }
                sets.push_back(tmp);
            }
            return sets;
        }
};

int main()
{
    return 0;
}
