#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <queue>
using namespace std;

class Solution
{
    private:
        int row;
        int col;
        bool flag;
        int dr[4];
        int dc[4];
        class Point
        {
            public:
            int r;
            int c;
            Point(int tr, int tc)
            {
                r = tr;
                c = tc;
            }
        };
    public:
        void init()
        {
            dr[0] = 0;
            dr[1] = 0;
            dr[2] = -1;
            dr[3] = 1;
            dc[0] = 1;
            dc[1] = -1;
            dc[2] = 0;
            dc[3] = 0;
        }

        bool isvalid(int r, int c)
        {
            return (r>=0 && r<row && c>=0 && c<col);
        }

        void dfs(vector<vector<char> > tmp, int r, int c, string word, int index)
        {
            if (flag == true)
            {
                return;
            }
            if (index >= word.length())
            {
                flag = true;
                return;
            }
                
            for (int i = 0; i < 4; ++i)
            {
                int tr = r + dr[i];
                int tc = c + dc[i];
                if (isvalid(tr, tc) == false)
                {
                    continue;
                }
                if (tmp[tr][tc] != word[index])
                {
                    continue;
                }
                char rec = tmp[r][c];
                tmp[r][c] = 0;
                dfs(tmp, tr, tc, word, index+1);
                tmp[r][c] = rec;
            }
            return;
        }

        bool exist(vector<vector<char> > &board, string word) {
            flag = false;
	        init();
            row = board.size();
            for (int r = 0; flag==false&&r<row; ++r)
            {
                col = board[r].size();
                for (int c = 0; flag==false&&c<col; ++c)
                {
                   if (board[r][c] == word[0])
                   {
                        vector<vector<char> > tmp(board);
                        dfs(tmp, r, c, word, 1);
                   }
                }
            }

            return flag;
        }
};

int main()
{
    return 0;
}
