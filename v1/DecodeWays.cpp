#include <iostream>
#include <string>
using namespace std;

class Solution
{
    public:
        string str;
        int val[100005];

        bool canDecoding(char fir, char sec)
        {
            if (fir >= '3' || fir <= '0')
                return false;
            if (fir == '2' && sec > '6')
                return false;
            if (fir == '0')
                return false;

            return true;
        }

        int cD(int st)
        {
            if (st >= str.size())
                return 1;
 
            if (val[st] != -1)
                return val[st];
            int ret = 0;
            
            //当数字
            if (str[st] != '0')
            {
                ret += cD(st+1);
            }
            else
            {
                return 0;
            }
            //两个数字
            if (st+1 < str.size() && canDecoding(str[st], str[st+1]))
            {
                ret += cD(st + 2);
            }
            
            return (val[st] = ret);
        }

        int numDecodings(string s)
        {
            if (s.size() == 0)
                return 0;
            if (s[0] == '0')
                return 0;

            str.clear();
            for (int i = 0; i < s.size(); ++i)
            {
                val[i] = -1;
                if (s[i]<'0' || s[i]>'9')
                {
                    return 0;
                }
                str.push_back(s[i]);
            }

            return cD(0);
        }
};

int main()
{
    Solution s;
    cout<<s.numDecodings("230");
    return 0;
}

