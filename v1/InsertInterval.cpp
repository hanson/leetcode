#include <iostream>
#include <vector>

using namespace std;

struct Interval
{
    int start;
    int end;
    Interval(): start(0), end(0) {}
    Interval(int s, int e): start(s), end(e) {}
};

class IntTreeNode
{
    public:
        IntTreeNode *leftChild;
        IntTreeNode *rightChild;
        int start;
        int end;
        int isCover;

        IntTreeNode(int s, int e)
        {
            leftChild = NULL;
            rightChild = NULL;
            start = s;
            end = e;
            isCover = 0;
        }
};

class Solution
{
    public:
        IntTreeNode *treeRoot;
        vector<Interval> results;

        IntTreeNode *buildTree(int lhs, int rhs)
        {
            IntTreeNode *curNode = new IntTreeNode(lhs, rhs);
            
            if (lhs+1 < rhs)
            {
                int mid = (lhs + rhs)>>1;
                curNode->leftChild = buildTree(lhs, mid);
                curNode->rightChild = buildTree(mid, rhs);
            }
            else if (lhs + 1 == rhs)
            {
                curNode->leftChild = buildTree(lhs, lhs);
                curNode->rightChild = buildTree(rhs, rhs);
            }

            return curNode;
        }

        void insertInt(int s, int e, IntTreeNode *curNode)
        {
            if (curNode == NULL)
                return;

            if (curNode->isCover == 1)
                return;

            if (s == curNode->start && e == curNode->end)
            {
                curNode->isCover = 1;
                return;
            }

            int mid = (curNode->start + curNode->end)/2;
            if (e <= mid)
            {
                insertInt(s, e, curNode->leftChild);
            }
            else if (s >= mid)
            {
                insertInt(s, e, curNode->rightChild);
            }
            else
            {
                insertInt(s, mid, curNode->leftChild);
                insertInt(mid, e, curNode->rightChild);
            }
        }

        void insertIntoResults(int s, int e)
        {
            if (results.size() == 0)
            {
                Interval yuInt(s, e);
                results.push_back(yuInt);
            }
            else
            {
                int len = results.size();
                if (results[len-1].end >= s)
                {
                    results[len-1].end = e;
                }
                else
                {
                    Interval yuInt(s, e);
                    results.push_back(yuInt);
                }
            }
        }

        void inorderTreeTravel(IntTreeNode *curNode)
        {
            if (curNode == NULL)
                return;
            if (curNode->isCover == 1)
            {
                insertIntoResults(curNode->start, curNode->end);
                return;
            }

            inorderTreeTravel(curNode->leftChild);
            inorderTreeTravel(curNode->rightChild);
        }

        vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
            int len = intervals.size();
            int maxn = newInterval.end;
            int min = newInterval.start;
            if (len > 0)
            {
                if (intervals[len-1].end > maxn)
                {
                    maxn = intervals[len-1].end;
                }
                if (intervals[0].start < min)
                {
                    min = intervals[0].start;
                }

            }

            treeRoot = buildTree(min, maxn);    
            
            for (int i = 0; i < len; ++i)
            {
                insertInt(intervals[i].start, intervals[i].end, treeRoot);
            }
            insertInt(newInterval.start, newInterval.end, treeRoot);
            
            results.clear();

            inorderTreeTravel(treeRoot);

            return results;
        }
};

int main()
{
    return 0;
}

