#include <iostream>
using namespace std;

/**
 * Definition for binary tree
 **/
 struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };

class Solution
{
    private:
        struct rTreeNode {
            TreeNode *tn;
            int index;
            rTreeNode()
            {
                tn = NULL;
                index = -1;
            }
        };
        int treeindex;
        TreeNode *rhead;
        int headindex;
        TreeNode *rtail;
        int tailindex;
    public:
        rTreeNode* findrbypre(TreeNode *cur)
        {
            if (cur == NULL)
            {
                return NULL;
            }
            rTreeNode *ret = new rTreeNode();
            rTreeNode *pre = NULL;
            rTreeNode *post = NULL;
            if (cur->left != NULL)
            {
                pre = findrbypre(cur->left);
                if (pre != NULL)
                {
                    ret->tn = pre->tn;
                    ret->index = pre->index;
                }
            }

            if (pre!=NULL && pre->tn!=NULL && pre->tn->val > cur->val)
            {
                    if (headindex == -1 || headindex>pre->index)
                    {
                        headindex = pre->index;
                        rhead = pre->tn;
                    }
                    if (tailindex==-1 || tailindex<treeindex)
                    {
                        tailindex = treeindex;
                        rtail = cur;
                    }
            }

            ret->tn = cur;
            ret->index = treeindex;
            ++treeindex;
            
            if (cur->right != NULL)
            {
                post = findrbypre(cur->right);
                if (post != NULL)
                {
                    ret->tn = post->tn;
                    ret->index = post->index;
                }
            }

            return ret;
        }

        rTreeNode* findrbypost(TreeNode *cur)
        {
            if (cur == NULL)
            {
                return NULL;
            }
            rTreeNode *ret = new rTreeNode();
            rTreeNode *pre = NULL;
            rTreeNode *post = NULL;
            if (cur->left != NULL)
            {
                pre = findrbypost(cur->left);
                if (pre != NULL)
                {
                    ret->tn = pre->tn;
                    ret->index = pre->index;
                }
            }
    	    int tmpindex = treeindex;
            if (ret->tn == NULL)
		    {
                ret->tn = cur;
                ret->index = treeindex;
		    }
            ++treeindex;

            if (cur->right != NULL)
            {
                post = findrbypost(cur->right);
                if (post->tn!= NULL && ret->tn==NULL)
                {
		            ret->tn = post->tn;
                    ret->index = post->index;
                }
            }
            if (post!=NULL && post->tn!=NULL && cur->val>post->tn->val)
            {
                    if (headindex == -1 || headindex>tmpindex)
                    {
                        headindex = tmpindex;
                        rhead = cur;
                    }
                    if (tailindex==-1 || tailindex<post->index)
                    {
                        tailindex =post->index;
                        rtail = post->tn;
                    }
            }

            return ret;
        }

        void dorecover(TreeNode *root)
        {
            headindex = -1;
            tailindex = -1;
            
            treeindex = 0;
            findrbypre(root);


            treeindex = 0;
            findrbypost(root);
        }

        void recoverTree(TreeNode *root) {
            rhead = NULL;
            rtail = NULL;
            dorecover(root);
            if (rhead==NULL || rtail==NULL)
            {
                return;
            }
            swap(rhead->val, rtail->val);
        }
};

int main()
{
    TreeNode *root = new TreeNode(2);
    TreeNode *right = new TreeNode(1);
    root->right = right;
    Solution s;
    s.recoverTree(root);
    return 0;
}
