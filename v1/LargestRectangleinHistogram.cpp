#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

using namespace std;

class Solution
{
    public:
        int largestRectangleArea(vector<int> &height) {
            int len = height.size();
            if (len <= 0)
                return 0;
            int ans = 0;

            stack<int> hs; 
            while (hs.empty() == false)
            {
                hs.pop();
            }
            
            hs.push(height[0]);
            for (int i = 1; i < len; ++i)
            {
                if (height[i] >= hs.top())
                {
                    hs.push(height[i]);
                }
                else
                {
                    int num = 0;
                    int temp;
                    while (hs.empty()==false && height[i] < hs.top())
                    {
                        ++num;
                        temp = hs.top()*num;
                        hs.pop();
                        if (temp > ans)
                        {
                            ans = temp;
                        }
                    }
                    for (int k = 0; k <= num; ++k)
                    {
                        hs.push(height[i]);
                    }
                }
            }
                    
            int num = 0;
            int temp;
            while (hs.empty() == false)
            {
                ++num;
                temp = hs.top()*num;
                hs.pop();
                if (temp > ans)
                {
                    ans = temp;
                }
            }

            return ans;
        }
};

int main()
{
    Solution s;
    vector<int> h;
    h.push_back(2);
    h.push_back(0);
    h.push_back(2);
    cout<<s.largestRectangleArea(h);
    return 0;
}

