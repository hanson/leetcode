#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
    private:
        vector<string> digits_letter_map;
        vector<string> lcs;
    public:
        Solution()
        {
            //0
            digits_letter_map.push_back("");
            //1
            digits_letter_map.push_back("");
            digits_letter_map.push_back("abc");
            digits_letter_map.push_back("def");
            digits_letter_map.push_back("ghi");
            digits_letter_map.push_back("jkl");
            digits_letter_map.push_back("mno");
            digits_letter_map.push_back("pqrs");
            digits_letter_map.push_back("tuv");
            digits_letter_map.push_back("wxyz");
        
            lcs.clear();
        }

        void get_LCs(string digits, int index, string lc)
        {
            if (index == digits.length())
            {
                lcs.push_back(lc);
                return;
            }
            int cur = digits[index] - '0';
            string letters = digits_letter_map[cur];
            for (int i = 0; i < letters.length(); ++i)
            {
                lc += letters[i];
                get_LCs(digits, index+1, lc);
                lc.erase(lc.length()-1);
            }
        }

        vector<string> letterCombinations(string digits) {
            string lc = "";
            lcs.clear();
            if (digits.length() == 0)
            { 
                lcs.push_back("");
                return lcs;
            }
            get_LCs(digits, 0, lc);

            return lcs;
        }
};

int main()
{
    Solution s;
    s.letterCombinations("2");
    return 0;
}

