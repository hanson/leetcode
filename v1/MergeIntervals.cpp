#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

/**
 * Definition for an interval.
 **/
struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};

class IntTreeNode
{
    public:
        IntTreeNode *leftChild;
        IntTreeNode *rightChild;
        int start;
        int end;
        int isCover;

        IntTreeNode(int s, int e)
        {
            leftChild = NULL;
            rightChild = NULL;
            start = s;
            end = e;
            isCover = 0;
        }
};

class Solution
{
    public:
        vector<Interval> result;

        IntTreeNode* BuildIntTree(int ts, int te)
        {
            IntTreeNode* curNode = new IntTreeNode(ts, te);
            if (ts + 1 < te)
            {
                int mid = (ts+te)>>1;
                curNode->leftChild = BuildIntTree(ts, mid);
                curNode->rightChild = BuildIntTree(mid, te);
            }
            else if (ts + 1 == te)
            {
                curNode->leftChild = BuildIntTree(ts, ts);
                curNode->rightChild = BuildIntTree(te, te);
            }
            
            return curNode;
        }

        void InsertInt(int ts, int te, IntTreeNode* curNode)
        {
            if (curNode == NULL)
            {
                return;
            }

            if (curNode->isCover == 1)
            {
                return;
            }
            if (ts == curNode->start && te == curNode->end)
            {
                curNode->isCover = 1;
                return;
            }

            int mid = (curNode->start + curNode->end)>>1;
            if (te <= mid)
            {
                InsertInt(ts, te, curNode->leftChild);
            }
            else if (ts >= mid)
            {
                InsertInt(ts, te, curNode->rightChild);
            }
            else
            {
                InsertInt(ts, mid, curNode->leftChild);
                InsertInt(mid, te, curNode->rightChild);
            }
        }

        void InsertIntoResult(int ts, int te)
        {
            int len = result.size();
            if (len > 0 && result[len-1].end >= ts)
            {
                result[len-1].end = te;
                return;
            }
        
            Interval yuInt(ts, te);
            result.push_back(yuInt);
        }

        void InorderTreeTravel(IntTreeNode* curNode)
        {
            if (curNode == NULL)
            {
                return;
            }
            if (curNode->isCover == 1)
            {
                InsertIntoResult(curNode->start, curNode->end);
                return;
            }

            InorderTreeTravel(curNode->leftChild);
            InorderTreeTravel(curNode->rightChild);
        }

        vector<Interval> merge(vector<Interval> &intervals) {
            if (intervals.size() == 0)
            {
                return intervals;
            }
            int ts = intervals[0].start;
            int te = intervals[0].end;
            for (int k = 1; k < intervals.size(); ++k)
            {
                if (intervals[k].start < ts)
                {
                    ts = intervals[k].start;
                }
                if (intervals[k].end > te)
                {
                    te = intervals[k].end;
                }
            }

            IntTreeNode* treeRoot = BuildIntTree(ts, te);
        
            for (int k = 0; k < intervals.size(); ++k)
            {
                InsertInt(intervals[k].start, intervals[k].end, treeRoot);
            }
            
            result.clear();
            InorderTreeTravel(treeRoot);
            return result;
        }
};

int main()
{
    return 0;
}

