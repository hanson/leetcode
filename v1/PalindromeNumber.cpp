#include <iostream>
using namespace std;

class Solution
{
    public:
        bool isPalindrome(int x) {
            if (x < 0)
            {
                return false;
            }
            if (x>=0 && x<=9)
            {
                return true;
            }

            int left = 1;
            int right = 10;
            int tx = x/10;
            while (tx > 0)
            {
                left *= 10;
                tx /= 10;
            }
            
            while (left >= right)
            {
                if ((x/left) != ((x%right)/(right/10)))
                {
                    return false;
                }
                x = x % left;
                x = x - x%right;
                left /= 10;
                right *= 10;
            }

            return true;
        }
};

