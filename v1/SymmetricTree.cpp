#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        bool check(TreeNode *r1, TreeNode *r2)
        {
            if (r1==NULL && r2==NULL)
            {
                return true;
            }
            if (r1==NULL ^ r2==NULL)
            {
                return false;
            }

            if (r1->val != r2->val)
            {
                return false;
            }

            return check(r1->right, r2->left)&&check(r1->left, r2->right);
        }

        bool isSymmetric(TreeNode *root) {
            return check(root, root);
        }
};

int main()
{
    return 0;
}
