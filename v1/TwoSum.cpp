#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
        vector<int> twoSum(vector<int> &numbers, int target) {
           vector<int> tmp(numbers);
           sort(tmp.begin(), tmp.end());
            int ps = 0;
            int pe = tmp.size()-1;
            while (ps <= pe)
            {
                int sum = tmp[ps] + tmp[pe];
                if (sum > target)
                {
                    --pe;
                }
                else if (sum < target)
                {
                    ++ps;
                }
                else
                {
                    break;
                }
            }
            vector<int> ret;
            ret.clear();
            for (int i = 0; i<numbers.size() && ret.size()<2; ++i)
            {
                if (numbers[i] == tmp[ps] || numbers[i]==tmp[pe])
                {
                    ret.push_back(i+1);
                }
            }

            return ret;
        }
};

int main()
{
    return 0;
}

