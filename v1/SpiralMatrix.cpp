#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        vector<int> spiralOrder(vector<vector<int> > &matrix)
        {
            vector<int> so;
            int rlen = matrix.size();
            if (rlen <= 0)
            {
                return so;
            }
            int clen = matrix[0].size();
            if (clen <= 0)
            {
                return so;
            }
            int wx = 0, wy = -1;
    
            while (true)
            {
                //->
                for (int k = 1; k <= clen; ++k)
                {
                    ++wy;
                    so.push_back(matrix[wx][wy]);
                }
                --rlen;
                if (rlen <= 0)
                {
                    break;
                }
                //|
                //V
                for (int k = 1; k <= rlen; ++k)
                {
                    ++wx;
                    so.push_back(matrix[wx][wy]);
                }
                --clen;
                if (clen <= 0)
                {
                    break;
                }
                //<-
                for (int k = 1; k <= clen; ++k)
                {
                    --wy;
                    so.push_back(matrix[wx][wy]);
                }
                --rlen;
                if (rlen <= 0)
                {
                    break;
                }
                //^
                //|
                for (int k = 1; k <= rlen; ++k)
                {
                    --wx;
                    so.push_back(matrix[wx][wy]);
                }
                --clen;
                if (clen <= 0)
                {
                    break;
                }
            }
        }
};

int main()
{
    return 0;
}
