#include <iostream>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
    private:
        int depth;
    public:

        void dfs(TreeNode *curNode, int de)
        {
            if (curNode == NULL)
            {
                return;
            }

            if (curNode->left == NULL && curNode->right == NULL)
            {
                if (depth == 0 || de+1 > depth)
                {
                    depth = de+1;
                }

                return;
            }

            dfs(curNode->left, de+1);
            dfs(curNode->right, de+1);
        }

        int maxDepth(TreeNode *root) {
            depth = 0;
            dfs(root, 0);
            return depth;
        }
};

int main()
{
    return 0;
}

