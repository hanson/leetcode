#include <iostream>
#include <vector>
using namespace std;

class Solution
{
    public:
        int maximalRectangle(vector<vector<char> > &matrix) {
            int row = matrix.size();

            int ans = 0;
            for (int st = 0; st < row; ++st)
            {
                for (int end = st; end < row; ++end)
                {       
                    int col = matrix[st].size();
                    for (int k = 0; k < col; ++k)
                    {
                        if (matrix[st][k]=='1' && matrix[end][k]=='1')
                        {
                            matrix[st][k] = '1';
                        }
                        else
                        {
                            matrix[st][k] = '0';
                        }
                    }
                    
                    int temp = 0;
                    for (int k = 0; k < col; ++k)
                    {
                        if (matrix[st][k] == '1')
                        {
                            ++temp;
                        }
                        else
                        {
                            if (temp*(end-st+1) > ans)
                            {
                                ans = temp*(end-st+1);
                            }

                            temp = 0;
                        }
                    }
                    if (temp*(end-st+1) > ans)
                    {
                        ans = temp*(end-st+1);
                    }
                }
            }

            return ans;
        }
};

int main()
{

    return 0;
}

