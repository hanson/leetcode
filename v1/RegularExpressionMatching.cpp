#include <iostream>
using namespace std;

class Solution
{
    public:
        bool doMatch(const char *s, const char*p)
        {
            if ('\0' == *p)
            {
                return '\0' == *s;
            }

            if (*(p+1) != '*')
            {
                if (*p==*s || *p=='.' && *s!='\0')
                {
                    return doMatch(s+1, p+1);
                }

                return false;
            }
            else
            {
                while(*p == *s || ((*p) == '.' && (*s) != 0))
                {
                    if(doMatch(s, p + 2))
                    {
                        return true;
                    }

                    s++;
                }

                return doMatch(s, p + 2);
            }
        }

        bool isMatch(const char *s, const char *p)
        {
            return doMatch(s, p);
        }
};

int main()
{

    const char *s = "aa";
    const char *p = "a*";
    Solution S;
    cout<<S.isMatch(s, p)<<endl;
    return 0;
}
