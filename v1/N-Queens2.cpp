#include <iostream>
#include<stdlib.h>
#include <vector>
#include <string>
using namespace std;

class Solution
{
    private:
        int* vcol;
        int* vdg;
        int* vadg;
        int n;
        int ans;
    public:
        int getdg(int row, int col)
        {
            return (row+col-1);
        }

        int getadg(int row, int col)
        {
            return (row-col + n);
        }

        void dfs(int row)
        {
            if (row > n)
            {
                ++ans;
                return;
            }

            for (int col = 1; col <= n; ++col)
            {
                if (vcol[col]==1 || vdg[getdg(row, col)]==1 || vadg[getadg(row, col)]==1)
                {
                    continue;
                }
                
                vcol[col] = 1;
                vdg[getdg(row, col)] = 1;
                vadg[getadg(row, col)] = 1;
                dfs(row+1);
                vcol[col] = 0;
                vdg[getdg(row, col)] = 0;
                vadg[getadg(row, col)] = 0;
            }
        }

        int totalNQueens(int tn) {
            vcol = (int*)malloc((tn+2)*sizeof(int));
            vdg = (int*)malloc(sizeof(int) * (2*tn+2));
            vadg = (int*)malloc(sizeof(int) * (2*tn+2));
            for (int k = 1; k <= tn; ++k)
            {
                vcol[k] = 0;
            }
            for (int k = 1; k <= 2*tn+1; ++k)
            {
                vdg[k] = vadg[k] = 0;
            }
            ans = 0;
            n = tn;
            dfs(1);
            return ans;
        }
};

int main()
{}
