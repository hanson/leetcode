#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    string addBinary(string a, string b) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int it_a = a.size()-1;
        int it_b = b.size() - 1;
        int isCarry = 0;
        string sum;
        int temp = 0;
        
        for (; it_a>=0 && it_b>=0; --it_a, --it_b)
        {
            temp = (a[it_a]-'0') + (b[it_b]-'0')+isCarry;
            isCarry = temp / 2;
            if (temp % 2 == 1)
            {
                sum.insert(0, "1");
            }
            else
            {
                sum.insert(0, "0");
            }
        }

        while (it_a >= 0)
        {
            temp = (a[it_a]-'0') + isCarry;
            isCarry = temp / 2;
            if (temp % 2 == 1)
            {
                sum.insert(0, "1");
            }
            else
            {
                sum.insert(0, "0");
            }
            --it_a;
        }

        while (it_b >= 0)
        {
            temp = (b[it_b]-'0') + isCarry;
            isCarry = temp / 2;
            if (temp % 2 == 1)
            {
                sum.insert(0, "1");
            }
            else
            {
                sum.insert(0, "0");
            }
            --it_b;
        }
        if (isCarry != 0)
        {
            sum.insert(0, "1");
        }
        return sum;
    }
};

int main()
{
    Solution s;
    cout<<s.addBinary("111", "1110")<<endl;
    cout<<s.addBinary("111", "10")<<endl;
}

