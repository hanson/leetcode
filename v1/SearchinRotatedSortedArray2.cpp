#include <iostream>
using namespace std;

class Solution
{
    private:
        int pivotindex;
        int pivotval;
    public:   
        void setpivot(int p, int pv)
        {
            if (pivotindex==-1 || pivotval>pv)
            {
                pivotindex = p;
                pivotval = pv;
            }
        }

        bool checkH(int A[], int n, int left, int right, int target)
        {
            if (left < 0 || right<0)
                return false;
            if (right >= n || left >=n)
            {
                return false;
            }
            if (left > right)
            {
                return false;
            }
            while (A[left] == A[right])
            {
                int mid = (left+right)>>1;
                if (left == right)
                {
                    return false;
                }
                if (A[mid] != target)
                {
                    return true;
                }
                else
                {
                    return (checkH(A, n, left, mid-1, target) || checkH(A, n, mid+1, right, target));
                }
            }

            return false;
        }

        int findp(int A[], int n)
        {
            pivotindex = -1;
            int left = 0;
            int right = n-1;
            int mid;
            while (left <= right)
            {
                mid = (left+right)>>1;
                if (A[left]>A[mid] || checkH(A, n, left, mid, A[mid]))
                {
                    setpivot(mid, A[mid]);
                    right = mid - 1;
                }
                else if (A[mid]>A[right] || checkH(A, n, mid, right, A[mid]))
                {
                    setpivot(right, A[right]);
                    left = mid + 1;
                }
                else
                {
                    setpivot(left, A[left]);
                    break;
                }
            }
        }

        bool binsearch(int A[], int n, int left, int right, int target)
        {
            if (right < 0)
            {
                return false;
            }
            if (left >= n)
            {
                return false;
            }
            while (left <= right)
            {
                int mid = (left+right)>>1;
                if (A[mid] == target)
                {
                    return true;
                }
                else if (A[mid] > target)
                {
                    right = mid - 1;
                }
                else
                {
                    left = mid + 1;
                }
            }

            return false;
        }

        bool search(int A[], int n, int target) {
            if (n <= 0)
            {
                return false;
            }
            findp(A, n);

            while (pivotindex-1>=0 && A[pivotindex-1]==pivotval)
            {
                --pivotindex;
            }
            
            int l1 = 0;
            int r1 = pivotindex-1;
            int l2 = pivotindex;
            int r2 = n-1;
            return (binsearch(A, n, l1, r1, target) || binsearch(A, n, l2, r2, target));
        }
};

int main()
{
    Solution s;
    int A[3] = {0, 0, 0};
    cout<<s.search(A, 3, 1)<<endl;
    return 0;
}

