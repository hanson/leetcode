#include <iostream>
#include <string>
#include <string.h>
using namespace std;

class Solution
{
    public:
    int** dp;
    
    int minDistance(string word1, string word2) 
    {
        int len1 = word1.length();
        int len2 = word2.length();

        int ** dp = (int**)malloc((len1+1) * sizeof(int*));

        for (int i = 0; i <= len1; ++i)
        {
            dp[i] = (int*)malloc((len2+1) * sizeof(int));
            dp[i][0] = i;
        }
        for (int i = 0; i <= len2; ++i)
        {
            dp[0][i] = i;
        }

        for (int i = 0; i < len1; ++i)
        {
            for (int j = 0; j < len2; ++j)
            {
                if (word1[i] == word2[j])
                {
                    dp[i+1][j+1] = dp[i][j];
                    continue;
                }
            
                int t1 = 1 + dp[i][j];
                int t2 = dp[i+1][j] + 1;
                if (t2 < t1)
                    t1 = t2;
                int t3 = dp[i][j+1] + 1;
                if (t3 < t1)
                    t1 = t3;

                dp[i+1][j+1] = t1;
            }
        }

        return dp[ len1 ][ len2 ];
    }
};
