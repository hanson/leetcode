#include <iostream>
#include <vector>
#include <string>
#include <stack>
using namespace std;

class Solution
{
    public:
        int total;
        vector<string> ps;
        
        void dfs(vector<int> lpStack, vector<int> p, int n)
        {
            if (n == 0 && p.size() == total)
            {
                string s;
                for (int i = 0; i < p.size(); ++i)
                {
                    if (p[i] == 0)
                    {
                        s+='(';
                    }
                    else
                    {
                        s+=')';
                    }
                }
                ps.push_back(s);
                return;
            }

            if (lpStack.size() == 0)
            {
                p.push_back(0);
                lpStack.push_back(0);
                --n;
                dfs(lpStack, p, n);
                p.pop_back();
                lpStack.pop_back();
                ++n;
            }
            else
            {
                if (n > 0)
                {
                    p.push_back(0);
                    lpStack.push_back(0);
                    --n;
                    dfs(lpStack, p, n);
                    p.pop_back();
                    lpStack.pop_back();
                    ++n;
                }
                    
                p.push_back(1);
                lpStack.pop_back();
                dfs(lpStack, p, n);
                p.pop_back();
                lpStack.push_back(0);
            }
        }

        vector<string> generateParenthesis(int n) {
            ps.clear();
            vector<int> p;
            vector<int> lpStack;
            lpStack.clear();
            p.clear();
            total = 2*n;
            dfs(lpStack, p, n);

            return ps;
        }
};

int main()
{

    return 0;
}

