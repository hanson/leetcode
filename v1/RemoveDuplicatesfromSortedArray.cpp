#include <iostream>
using namespace std;

class Solution
{
    public:
        int removeDuplicates(int A[], int n) {
            if (n == 0)
            {
                return 0;
            }

            int ret = 1;
            for (int p = 1; p < n; ++p)
            {
                if (A[p] == A[ret-1])
                {
                    continue;
                }
                else
                {
                    ++ret;
                    A[ret-1] = A[p];
                }
            }

            return ret;
        }
};

int main()
{
    return 0;
}

