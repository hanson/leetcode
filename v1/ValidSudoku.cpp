#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        bool isvalid(const vector<char>& vec) {
            vector<bool> occur(9, false);
            for (int i = 0; i < 9; ++i) {
                if (vec[i] == '.')
                    continue;
                if (occur[vec[i]-'1']) 
                    return false;
                else 
                    occur[vec[i]-'1'] = true;
            }
   
            return true;
        }

        bool checkvalid(vector<vector<char> > &board)
        {
            //check row
            for (int r = 0; r < 9; ++r)
            {
                if (isvalid(board[r]) == false)
                {
                    return false;
                }
            }
            //check col
            for (int c = 0; c < 9; ++c)
            {
                vector<char> chs(9);
                for (int i = 0; i < 9; ++i)
                {
                    chs[i] = (board[i][c]);
                }
                if (isvalid(chs) == false)
                {
                    return false;
                }
            }
            //check grid
            for (int gr = 0; gr < 3; ++gr)
            {
                for (int gc = 0; gc < 3; ++gc)
                {
                    vector<char> chs2;
                    for (int tr = gr*3; tr < gr*3 + 3; ++tr)
                    {
                        for (int tc = gc*3; tc < gc*3 + 3; ++tc)
                        {
                            chs2.push_back( (board[tr][tc]) );
                        }
                    }
                    if (isvalid(chs2) == false)
                    {
                        return false;
                    }
                }
            }
            
            return true;
        }

        bool isValidSudoku(vector<vector<char> > &board) {
            return checkvalid(board);
        }
};

int main()
{
    return 0;
}
