/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
#include <cmath>
#include <iostream>

using namespace std;

class Solution {
public:
    int subTreeHeight(TreeNode *curNode)
    {
        if (curNode == NULL)
            return 0;

        int leftheight = subTreeHeight(curNode->left);
        int rightheight = subTreeHeight(curNode->right);

        if (leftheight < rightheight)
            leftheight = rightheight;
        
        return leftheight+1;
    }

    bool isBalanced(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (root == NULL)
            return true;

        int leftHeight = subTreeHeight(root->left);
        int rightHeight = subTreeHeight(root->right);
        
        if( abs(leftHeight-rightHeight) > 1)
            return false;
        
        return isBalanced(root->left) && isBalanced(root->right);
    }
};

