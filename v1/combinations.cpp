#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<vector<int> > trips;

    void dfs(int cur, vector<int> &elem, int n, int k)
    {
        if (elem.size() == k)
        {   
            vector<int> trip(elem);
            trips.push_back(trip);
            return;
        }
        for (int index = cur; index <= n; ++index)
        {
            elem.push_back(index);
            dfs(index+1, elem, n, k);
            elem.pop_back();
        }
    }

    vector<vector<int> > combine(int n, int k) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        trips.clear();
        vector<int> elem;
        dfs(1, elem, n, k);

        return trips;
    }
};

int main()
{

    return 0;
}

