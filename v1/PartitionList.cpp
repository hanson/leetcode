#include <iostream>
using namespace std;

/**
 *  * Definition for singly-linked list.
 **/

class Solution
{
    public:
        ListNode *partition(ListNode *head, int x) {
            ListNode *lhead = NULL;
            ListNode *lt = NULL;
            ListNode *behead = NULL;
            ListNode *bet = NULL;
            if (head == NULL)
                return lhead;
            while (head != NULL)
            {
                if (head->val < x)
                {
                    if (lt == NULL)
                    {
                        lt = new ListNode(head->val);
                        lhead = lt;
                    }
                    else
                    {
                        lt->next = new ListNode(head->val);
                        lt = lt->next;
                    }
                }
                else
                {
                    if (bet == NULL)
                    {
                        bet = new ListNode(head->val);
                        behead = bet;
                    }
                    else
                    {
                        bet->next = new ListNode(head->val);
                        bet = bet->next;
                    }
                }
                head = head->next;
            }
            //if (behead != NULL)
            if (lt != NULL)
            {
                lt->next = behead;
            }
            else
            {
                lhead = behead;
            }

            return lhead;
        }
};
