#include <iostream>
#include <string.h>
using namespace std;

class Solution
{
    public:
        int lengthOfLastWord(const char *s) {
           int ans = 0;
           if (s == NULL)
           {
                return ans;
           }
            
           int temp = 0;
           for (int i = 0; s[i] != '\0'; ++i)
           {
                if (s[i] == ' ')
                {
                    if (temp != 0)
                    {
                        ans = temp;
                    }
                    temp = 0;
                }
                else
                {
                    ++temp;
                }
           }

           if (temp != 0)
           {
                ans = temp;
           }

           return ans;
        }
};

int main()
{
    return 0;
}

