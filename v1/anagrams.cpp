#include <iostream>
#include <string>
#include <vector>
#include <string.h>
#include <algorithm>

using namespace std;

class YuStr
{
public:
    int chMap[26];
    string str;
    YuStr(string temp)
    {
        str = temp;
        for (int i = 0; i < 26; ++i)
            chMap[i] = 0;
    }
    
    YuStr(const YuStr& rhs)
    { 
        str = rhs.str;
        for (int i = 0; i < 26; ++i)
        {
            chMap[i] = rhs.chMap[i];
        }
    }

    void CalChMap()
    {
        for (int k = 0; k < str.length(); ++k)
        {
            ++chMap[ str[k]-'a' ];
        }
    }

    bool operator<(const YuStr& rhs) const
    { 
        for (int i = 0; i < 26; ++i)
        {
            if (chMap[i] > rhs.chMap[i])
                return false;
            else if (chMap[i] < rhs.chMap[i])
                return true;
        }

        return false;
    }
    
    bool operator>=(const YuStr& rhs) const
    { 
        for (int i = 0; i < 26; ++i)
        {
            if (chMap[i] > rhs.chMap[i])
            {
                return true;
            }
            else if (chMap[i] < rhs.chMap[i])
                return false;
        }

        return true;
    }
    
    bool operator==(const YuStr& rhs) const
    { 
        for (int i = 0; i < 26; ++i)
        {
            if (chMap[i] != rhs.chMap[i])
                return false;
        }

        return true;
    }
    void resetValue(const YuStr& rhs)
    { 
        str = rhs.str;
        for (int i = 0; i < 26; ++i)
        {
            chMap[i] = rhs.chMap[i];
        }
    }
};

class Solution {
public:
    void swap(vector<YuStr> &yuVec, int findex, int lindex)
    {
            YuStr* temp = new YuStr( yuVec[lindex]);
            yuVec[lindex].resetValue( yuVec[findex] );
            yuVec[findex].resetValue( *temp );
    }

    int partition(vector<YuStr> &yuVec, int left, int right)
    {
        if (left >= right)
            return left;
        YuStr* sentinel = new YuStr( yuVec[right] );

        int lindex = right-1;
        int findex = left;

        while (findex < lindex)
        {
            while (yuVec[findex] < *sentinel && findex < right)
            {
                ++findex;
            }
            while (yuVec[lindex] >= *sentinel && lindex>=left)
            {
                --lindex;
            }

            if (lindex <= findex)
                break;
            
            swap(yuVec, findex, lindex);
            --lindex, ++findex;
        }

        if (findex < right)
        {
            if (yuVec[findex] < *sentinel)
            {
                ++findex;
            }
            if (findex < right)
                swap(yuVec, findex, right);
        }

       return findex;
    }

    void yuQsort(vector<YuStr> &yuVec, int left, int right)
    {
       if (left >= right)
           return;
       int p = partition(yuVec, left, right);
       if (left < p-1)
       {
            yuQsort(yuVec, left, p-1);
       }
       if (p+1 < right)
       {
            yuQsort(yuVec, p+1, right);
        }
    }

    vector<string> anagrams(vector<string> &strs) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<YuStr> yuVec;
        for (int i = 0; i < strs.size(); ++i)
        {
            YuStr* ts = new YuStr(strs[i]);
            ts->CalChMap();
            yuVec.push_back(*ts);
        }

       // yuQsort(yuVec, 0, yuVec.size()-1);
        sort(yuVec.begin(), yuVec.end());
        vector<string> ret;
        bool first = true;

        //out(yuVec);
        for (int i = 0; i < yuVec.size(); ++i)
        {
            if (i!=0 && yuVec[i] == yuVec[i-1])
            {
                if (true == first)
                {
                    ret.push_back(yuVec[i-1].str);
                    first = false;
                }

                ret.push_back(yuVec[i].str);
            }
            else
            {
                first = true;
            }
            
        }
        return ret;
    }

    void out(const vector<YuStr>& yuVec)
    {
        for (int i = 0; i < yuVec.size(); ++i)
       
        {
            cout<<yuVec[i].str<<endl;
            for (int k = 0; k < 26; ++k)
            {
                cout<<yuVec[i].chMap[k];
            }
            cout<<endl;
        }
    }
};

int main()
{
    Solution s;
    vector<string> strs;
    strs.push_back("sat");
    strs.push_back("lea");
    strs.push_back("arm");
    strs.push_back("sin");
    strs.push_back("the");
    strs.push_back("nod");
    strs.push_back("guy");
    strs.push_back("ins");
    strs.push_back("rod");
    strs = s.anagrams(strs);
   for (int i = 0; i < strs.size(); ++i)
   {
    cout<<strs[i]<<endl;
   }
    return 0;
}

