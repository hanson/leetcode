#include <algorithm>
#include <iostream>
#include <string>
#include <stack>
#include <vector>
using namespace std;

class Solution
{
    public:
        bool isValid(string s) {
            //0:(; 1:); 2:{; 3:}; 4:[; 5:]
            vector<int> stack;
            for (int i = 0; i < s.length(); ++i)
            {
                if (s[i] == '(')
                {
                    stack.push_back(0);
                }
                else if (s[i] == '{')
                {
                    stack.push_back(2);
                }
                else if (s[i] == '[')
                {
                    stack.push_back(4);
                }
                else if (s[i] == ')')
                {
                    if (stack.size()>0 && stack[stack.size()-1]==0)
                    {
                        stack.pop_back();
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (s[i] == '}')
                {
                    if (stack.size()>0 && stack[ stack.size()-1 ] == 2)
                    {
                        stack.pop_back();
                    }
                    else
                    {
                        return false;
                    }
                    
                }
                else if (s[i] == ']')
                {
                    if (stack.size()>0 && stack[ stack.size()-1 ] == 4)
                    {
                        stack.pop_back();
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return stack.size()==0;
        }
};

int main()
{
    return 0;
}
