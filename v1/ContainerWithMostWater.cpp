#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int maxArea(vector<int> &height) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int ans = 0;

        int left = 0; 
        int right = height.size() - 1;

        int temp;

        while (left < right)
        {
            temp = min(height[left], height[right])*(right-left);
            if (temp > ans)
                ans = temp;

            if (height[left] < height[right])
            {
                ++left;
            }
            else
            {
                --right;
            }
        }

        return ans;
    }
};

int main()
{}
