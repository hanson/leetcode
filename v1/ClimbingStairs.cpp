#include <iostream>
using namespace std;

class Solution {
public:
    int facNum[101];
    
    void makeFacNum(int n)
    {
        facNum[0] = 1;
        facNum[1] = 1;
        for (int i = 2; i <= n; ++i)
        {
            facNum[i] = facNum[i-1] + facNum[i-2];
        }
    }

    int climbStairs(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        makeFacNum(n);
        return facNum[n];
    }
};
