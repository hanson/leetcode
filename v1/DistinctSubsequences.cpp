#include <iostream>
#include <string>
using namespace std;

class Solution
{
    public:
        int dp[50][11000];
        //dp[i][j]: index form 1
        //if t[i-1] != s[j-1], dp[i][j] = dp[i][j-1]
        //else if t[i-1] == s[j-1], dp[i][j] = dp[i][j-1] + dp[i-1][j-1]
        //init:
        //
        int numDistinct(string S, string T)
        {
            int lens = S.length();
            int lent = T.length();
            if (lent > lens)
                return 0;
            
            memset(dp, 0, sizeof(dp));
            for (int is = 0; is <= lens; ++is)
            {
                dp[0][is] = 1;
            }

            for (int is = 0; is < lens; ++is)
            {
                for (int it = 0; it <= is && it < lent; ++it)
                {
                    if (T[it] != S[is])
                    {
                        dp[it+1][is+1] = dp[it+1][is];
                    }
                    else
                    {
                        dp[it+1][is+1] = dp[it+1][is] + dp[it][is];
                    }
                }
            }
 
            return dp[lent][lens];
        }
};
