#include <algorithm>
#include <iostream>
#include <string>
using namespace std;

class Solution
{
    private:
        int chmap[256];
    public:
        Solution()
        {
            for(int i = 0; i < 256; ++i)
            {
                chmap[i] = 0;
            }
        }

        int lengthOfLongestSubstring(string s) {
            int len = s.length();
            int sp = 0;
            int ep = 0;
            int ans = 0;
            
            for(int i = 0; i < 256; ++i)
            {
                chmap[i] = 0;
            }

            while (ep < len)
            {
                if ( chmap[ s[ep] ] == 0)
                {
                    chmap[ s[ep] ] = 1;
                    ++ep;
                }
                else
                {
                    ans = max(ans, ep-sp);
                    for (;s[sp]!=s[ep]; ++sp)
                    {
                        chmap[ s[sp] ] = 0;
                    }
                    ++sp;
                    ++ep;
                }
            }
            
            return max(ans, len-sp);
        }
};

int main()
{
    Solution s;
    cout<<s.lengthOfLongestSubstring("abca")<<endl;
    return 0;
}

