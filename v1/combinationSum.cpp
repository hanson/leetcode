#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    vector<vector<int> > ansVec;

    int compare(vector<int> ft, vector<int> lt)
    {
        for (int i = 0; i < lt.size(); ++i)
        {
            if (ft[i] > lt[i])
            {
                return 1;
            }
            else if (ft[i] < lt[i])
            {
                return -1;
            }
        }
        
        return 0;
    }

    //because add increasing by a, b, c's value
    bool isUnique(vector<int> trip)
    {
        if (ansVec.size() == 0)
            return true;

        int first = 0;
        int last = ansVec.size() - 1;
        int mid = (first + last)/2;

        while (first <= last)
        {
            mid = (first + last)/2;
            //0 is equal, 1 is first bigger, -1 is smaller
            int com = compare( ansVec[mid], trip);
            if (com == 0)
            {
                return false;
            }
            else if (com > 0)
            {
                last = mid - 1;
            }
            else
            {
                first = mid + 1;
            }
        }

        return true;
    } 

    void dfs(int curIndex, int curSum, vector<int> &elem, int target, vector<int> can, int limit)
    {
        if (curSum == target)
        {
            if (isUnique(elem) == false)
                  return;
            vector<int> temp(elem); 
            ansVec.push_back(temp);
        }

        for (int i = curIndex; i < limit; ++i)
        {
            while (i > curIndex && can[i] == can[i-1])
            {
                ++i;
            }

            if (i >= limit)
                break;

            if (curSum+can[i] > target)
                break;

            elem.push_back( can[i] );
            dfs(i, curSum+can[i], elem, target, can, limit);
            elem.pop_back();
        }
    }

    vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ansVec.clear();
        if (candidates.empty() == true)
        {
            return ansVec;
        }
        vector<int> elem;
        elem.clear();
        sort(candidates.begin(), candidates.end());
        dfs(0, 0, elem, target, candidates, candidates.size()); 
    
        return ansVec;
    }
};

int main()
{
	Solution s;
    vector<int> test;
    int k = 1;
    while (k--)
    {
    for (int i = 5; i > 0; --i)
    {
        test.push_back(i);
    }
    }
    vector<vector<int> >ret = s.combinationSum(test, 4);
    vector<vector<int> >::iterator it;
    for (it = ret.begin(); it != ret.end(); ++it)
    {
        vector<int> trip = *it;
        vector<int>::iterator pit;
        for (pit = trip.begin(); pit != trip.end(); ++pit)
        {
            cout<<*pit<<" ";
        }
        cout<<endl;
    }
}

