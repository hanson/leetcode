#include <iostream>
#include <string>

using namespace std;

class Solution
{
    public:
        string cas(string str)
        {
            string ret;

            int count = 0;
            char lastCh = str[0];
            for (int i = 0; i < str.length(); ++i)
            {
                if (lastCh == str[i])
                {
                    ++count;
                }
                else
                {
                    ret.push_back(count+'0');
                    ret.push_back(lastCh);
                    count = 1;
                    lastCh = str[i];
                }
            }

            if (count > 0)
            {
                ret.push_back(count+'0');
                ret.push_back(lastCh);
            }
            return ret;
        }
        string countAndSay(int n)
        {
            if (n < 1)
                return "";
            string str = "1";

            for (int i = 2; i <= n; ++i)
            {
                str = cas(str);
            }

            return str;
        }
};

int main()
{
    return 0;
}

