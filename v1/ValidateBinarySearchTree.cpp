#include <iostream>
#include <cmath>
using namespace std;

/**
 *  Definition for binary tree
 **/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
    private:
        bool isvalid;
        bool isfirst;
        int preval;
        int curval;
    public:   
        void check(TreeNode *cur)
        {
            if (cur == NULL)
            {
                return;
            }

            if (isvalid == false)
            {
                return;
            }
            check(cur->left);
            if (isfirst)
            {
                isfirst = false;
                preval = cur->val;
            }
            else
            {
                curval = cur->val;
                if (preval >= curval)
                {
                    isvalid = false;
                }
                preval = curval;
            }
            check(cur->right);
        }

        bool isValidBST(TreeNode *root) {
            if (root == NULL)
                return true;
            isvalid = true;
            isfirst = true;
            check(root);
            return isvalid;
        }
};

int main()
{
    return 0;
}

