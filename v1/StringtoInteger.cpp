#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        bool isdigit(char ch)
        {
            return (ch>='0' && ch<='9');
        }

        int atoi(const char *str) {
            int num = 0;
            int sign = 1;
            bool over = false;
            bool hassign = false;
            bool hasdigit = false;
            while (*str!='\0')
            {
                if ((hasdigit==true||hassign==true) && isdigit(*str)==false)
                {
                    break;
                }
                while (*str == ' ')
                {
                    ++str;
                }
                if ((*str=='+'||*str=='-'))
                {
                    if (hasdigit==true || hassign==true)
                    {
                        break;
                    }
                    else if (*str=='-')
                    {
                        sign = -1;
                    }
                    hassign = true;
                }
                else if (isdigit(*str) == false)
                {
                    break;
                }
                else
                {
                    hasdigit = true;
                    if (sign > 0)
                    {
                        if (num>214748364 || (num==214748364&&((*str-'0')>=7)))
                        {
                            over = true;
                            break;
                        }
                    }
                    else if (num>214748364 || (num==214748364&&((*str-'0')>7)))
                    {
                        over = true;
                        break;
                    }
                    num *= 10;
                    num += (*str-'0');
                }
                ++str;
            }
       
            int ret = num*sign;
            if (over)
            {
                if (sign>0)
                {
                    return 2147483647;
                }
                else
                {
                    return -2147483648;
                }
            }

            return num*sign;
        }
};

int main()
{
    return 0;
}
