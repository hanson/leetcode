#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int calMax(vector<int> &prices, int lhs, int rhs)
    {
            if (lhs >= rhs)
                return 0;

            int ret = 0;
            int stackHead = prices[lhs];
            for (int i = lhs; i <= rhs; ++i)
            {
                int p;
                if ((p = prices[i] - stackHead) > ret)
                {
                    ret = p;
                }
                if (p < 0)
                {
                    stackHead = prices[i];
                }
            }

            return ret;
    }

    int maxProfit(vector<int> &prices) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if (prices.size() == 0)
            return 0;
       
        vector<int> tPrices;
        tPrices.push_back(prices[0]);
        int t = -1;
        for (int index = 1; index < prices.size(); ++index)
        {
              if (prices[index] >= prices[index-1])
              {
                    t = prices[index];
              }
              else if (t == -1)
              {
                    tPrices.pop_back();
                    tPrices.push_back(prices[index]);
              }
              else
              {
                tPrices.push_back(prices[index - 1]);
                tPrices.push_back(prices[index]);
                t = -1;
              }
        }

        if (t != -1)
        {
           tPrices.push_back(t); 
        }

        int ans = 0;
        for (int index = 0; index < tPrices.size(); ++index)
        {
            int temp = calMax(tPrices, 0, index) + calMax(tPrices, index+1, tPrices.size()-1);

            if (temp > ans)
            {
                ans = temp;
            }
        }
        
        return ans;
    }
};

int main()
{
    Solution s;
    vector<int> prices;
    //2,4,2,5,7,2,4,9,0
    prices.push_back(1);
    prices.push_back(2);
    prices.push_back(4);
    prices.push_back(2);
    prices.push_back(5);
    prices.push_back(7);
    prices.push_back(2);
    prices.push_back(4);
    prices.push_back(9);
    prices.push_back(0);
    cout<<s.maxProfit(prices)<<endl;
    return 0;
}

