#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:

    int compare(vector<int> ft, vector<int> lt)
    {
        for (int i = 0; i < lt.size(); ++i)
        {
            if (ft[i] > lt[i])
            {
                return 1;
            }
            else if (ft[i] < lt[i])
            {
                return -1;
            }
        }
        
        return 0;
    }

    //because add increasing by a, b, c's value
    bool isUnique(vector<vector<int> >trips, vector<int> trip)
    {
        if (trips.size() == 0)
            return true;

        int first = 0;
        int last = trips.size() - 1;
        int mid = (first + last)/2;

        while (first <= last)
        {
            mid = (first + last)/2;
            //0 is equal, 1 is first bigger, -1 is smaller
            int com = compare( trips[mid], trip);
            if (com == 0)
            {
                return false;
            }
            else if (com > 0)
            {
                last = mid - 1;
            }
            else
            {
                first = mid + 1;
            }
        }

        return true;
    }

    vector<vector<int> > threeSum(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        sort(num.begin(), num.end());
        vector<int>::iterator it_a;
        vector<int>::iterator it_b;
        vector<int>::iterator it_c;
        vector<vector<int> > trips;

        for (it_a = num.begin(); it_a != num.end(); ++it_a)
        {
            if (it_a != num.begin())
            {
                while (it_a != num.end() && *it_a == *(it_a - 1))
                {
                    ++it_a;
                }
            }
            if (it_a == num.end())
            {
                break;
            }
            for (it_b = it_a+1; it_b != num.end(); ++it_b)
            {
                if (it_b != (it_a+1))
                {
                    while (it_b != num.end() && *it_b == *(it_b-1))
                    {
                        ++it_b;
                    }
                }
                if (it_b == num.end())
                {
                    break;
                }
                for (it_c = it_b+1; it_c != num.end(); ++it_c)
                {
                    if (it_c != (it_b+1))
                    {
                        while (it_c != num.end() && *it_c == *(it_c-1))
                        {
                            ++it_c;
                        }
                    }
                    if (it_c == num.end())
                    {
                        break;
                    }
                    //cout<<*it_a<<" "<<*it_b<<" "<<*it_c<<endl;
                    if (0 == *it_a + *it_b + *it_c)
                    {
                        vector<int> trip;
                        trip.push_back(*it_a);
                        trip.push_back(*it_b);
                        trip.push_back(*it_c);
                        //if (isUnique(trips, trip) == true)
                        //{
                        trips.push_back(trip);
                        //}
                    }
                }
            }
        }
        
        return trips;
    }
};

int main()
{
    Solution s;
    vector<int> test;
    int k = 3;
    while (k--)
    {
    for (int i = 5; i >= -5; --i)
    {
        test.push_back(i);
    }
    }
    vector<vector<int> >ret = s.threeSum(test);
    vector<vector<int> >::iterator it;
    for (it = ret.begin(); it != ret.end(); ++it)
    {
        vector<int> trip = *it;
        vector<int>::iterator pit;
        for (pit = trip.begin(); pit != trip.end(); ++pit)
        {
            cout<<*pit<<" ";
        }
        cout<<endl;
    }
}

