#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <stdlib.h>
#include <functional>
#include <queue>
using namespace std;

class Solution
{
    private:
        class Step
        {
            public:
            int trans;
            string str;
            Step(int t, string s)
            {
                trans = t;
                str = s;
            }
        };
    public:
        int ladderLength(string start, string end, unordered_set<string> &dict) {
            queue<Step*> que;
            while (!que.empty())
            {
                que.pop();
            }
            Step *head;
            que.push(new Step(1, start));
            while (que.empty() == false)
            {
                head = que.front();
                que.pop();
                int cs = head->trans;
                string str = head->str;
            
                for (int k = 0; k < str.length(); ++k)
                {
                    for (char ch = 'a'; ch <= 'z'; ++ch)
                    {
                        if (str[k] == ch)
                            continue;
                        string cur(str);
                        cur[k] = ch;
                        if (cur == end)
                        {
                            return cs + 1;
                        }

                        if (dict.count(cur))
                        {
                            dict.erase(cur);
                            que.push(new Step(cs+1, cur));
                        }
                    }
                }
            }

            return 0;
        }
};

int main()
{
    return 0;
}
