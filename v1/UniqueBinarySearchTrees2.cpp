#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

/**
 ** Definition for binary tree
 **/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
    public:
        vector<TreeNode *> gen(int left, int right)
        {
            vector<TreeNode *> ret;
            if (left > right)
            {
                ret.push_back(NULL);
                return ret;
            }

            for (int cur = left; cur <= right; ++cur)
            {
                vector<TreeNode *> leftchs = gen(left, cur-1);
                vector<TreeNode *> rightchs = gen(cur+1, right);
                for (int le = 0; le < leftchs.size(); ++le)
                {
                    for (int ri = 0; ri < rightchs.size(); ++ri)
                    {
                        TreeNode *curnode = new TreeNode(cur);
                        curnode->left = leftchs[le];
                        curnode->right = rightchs[ri];
                        ret.push_back(curnode);
                    }
                }
            }

            return ret;
        }
        vector<TreeNode *> generateTrees(int n) {
            return gen(1, n);
        }
};

int main()
{
    return 0;
}
