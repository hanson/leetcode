#include <iostream>

using namespace std;

class Solution
{
    private:
        int *dp;
        int *opt;
    public:
        int jump(int A[], int n) {
            if (n <= 1)
                return 0;

            dp = (int*)malloc(sizeof(int)*(n+1));
            opt = (int*)malloc(sizeof(int)*(n+1));
            
            int jump = 0;
            for (int i = 0; i < n; ++i)
            {
                if (A[i] > jump)
                {
                    jump = A[i];
                    --jump;
                }
                else
                {
                    --jump;
                }
                opt[i] = jump + 1;
                dp[i] = -1;
            }

            dp[0] = 0;
            for (int i = 1; i < n; ++i)
            {
                for (int j = 0; j < i; ++j)
                {
                    if (j+opt[j] >= i)
                    {
                        if (dp[i]==-1 || dp[j]+1 < dp[i])
                        {
                            dp[i] = dp[j] + 1;
                            break;
                        }
                    }
                }
            }

            return dp[n-1];
        }
};

int main()
{
    return 0;
}

