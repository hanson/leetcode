#include <iostream>
using namespace std;

class Solution
{
    public:
        void merge(int A[], int m, int B[], int n) {
            int tm = m-1;
            for (int k = 0; k < n; ++k)
            {
                int temp = B[k];
                
                while (tm>=0 && A[tm]>temp)
                {
                    A[tm+1] = A[tm];
                    --tm;
                }
                A[tm+1] = temp;
                ++m;
                tm = m-1;
            }
        }
};

int main()
{
    return 0;
}

