#include <iostream>
#include <vector>
using namespace std;

/**
 *  * Definition for singly-linked list.
 **/
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class MergeTwoLists
{
    public:
        ListNode *head;
        ListNode *cur;
    
        void add2List(int val)
        {
            if (cur == NULL)
            {
                cur = new ListNode(val);
                head = cur;
            }
            else
            {
                cur->next = new ListNode(val);
                cur = cur->next;
            }
        }

        ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
            head = NULL;
            cur = head;

            while(l1!=NULL && l2!=NULL)
            {
               if (l1->val <= l2->val)
               {
                   add2List(l1->val); 
                   l1 = l1->next;
               }
               else
               {
                   add2List(l2->val); 
                    l2 = l2->next;
               }
            }

            while(l1!=NULL)
            {
                add2List(l1->val); 
                l1 = l1->next;
            }
            while(l2!=NULL)
            {
                add2List(l2->val); 
                l2 = l2->next;
            }

            return head;
        }
};

class Solution
{
    public:
        ListNode *mergesort(int lhs, int rhs,vector<ListNode *> lists)
        {
            if (lhs < rhs)
            {
                int mid = (lhs+rhs)>>1;
                ListNode *head1 = mergesort(lhs, mid, lists);
                ListNode *head2 = mergesort(mid+1, rhs, lists);
                MergeTwoLists *mtl = new MergeTwoLists();
                return mtl->mergeTwoLists(head1, head2);
            }
            
            return lists[lhs];
        }

        ListNode *mergeKLists(vector<ListNode *> &lists) 
        {
            if (lists.size() == 0)
            {
                return NULL;
            }

            return mergesort(0, lists.size()-1, lists);    
        }
};

int main()
{
    return 0;
}

