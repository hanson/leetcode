#include <iostream>
using namespace std;

class Solution
{
    public:
        double pow(double x, int n) {
            double ans;
            if (n == 0)
            {
                return 1;
            }
            if (n < 0)
            {
                n = -n;
                x = 1.0/x;
            }
            
            if (x == 0.0)
            {
                return 0;
            }
            else if (x == 1.0)
            {
                return 1;
            }
            else if (x == -1.0)
            {
                if (n&1)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }

            ans = 1;
            double base = x;
            while (n)
            {
                if (n&1)
                {
                    ans *= base;
                }

                base *= base;
                n>>=1;
            }

            return ans;
        }
};

