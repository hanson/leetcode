#include <iostream>
#include <vector>
#include <stack>
using namespace std;

class Solution
{
    public:
        vector<int> grayCode(int n) {
            vector<int> grays;
            grays.push_back(0);
            if (n == 0)
                return grays;

            stack<int> gs;
            while (gs.empty() == false)
            {
                gs.pop();
            }
            
            grays.push_back(1);
            int mask = 1;
            for (int i = 1; i < n; ++i)
            {
                mask = 1 << i;
                for (int i = 0; i < grays.size(); ++i)
                {
                    gs.push(grays[i] | mask);        
                }

                while (gs.empty() == false)
                {
                    grays.push_back(gs.top());
                    gs.pop();
                }
            }

            return grays;
        }
};

int main()
{
    return 0;
}

