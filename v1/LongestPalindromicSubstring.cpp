#include <iostream>
#include <string>
using namespace std;

class Solution
{
    private:
        int dp[1005][1005];
    public:
        string longestPalindrome(string s) {
            string pal;
            int len = s.length();
            if (len == 0)
                return "";
            else if (len == 1)
                return s;

            for (int i = 0; i <= len; ++i)
            {
                for (int j = 0; j <= len; ++j)
                {
                    if (i >= j)
                    {
                        dp[i][j] = 1;
                    }
                    else
                    {
                        dp[i][j] = 0;
                    }
                }
            }
            int lhs = 0;
            int rhs = 0;
            int maxlen = 1;
            for (int k = 1; k < len; ++k)
            {
                for (int str = 0; str+k < len; ++str)
                {
                    int end = str+k;
                    if (s[str] != s[end])
                    {
                        dp[str][end] = 0;
                    }
                    else
                    {
                        dp[str][end] = dp[str+1][end-1];
                        if (dp[str][end])
                        {
                            if (k+1 > maxlen)
                            {
                                maxlen = k+1;
                                lhs = str;
                                rhs = end;
                            }
                        }
                    }
                }
            }

            return s.substr(lhs, (rhs-lhs+1));
        }
};

int main()
{
    Solution S;
    cout<<S.longestPalindrome("12321")<<endl;
    return 0;
}

