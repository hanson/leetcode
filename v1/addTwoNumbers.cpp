/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
*/
#include <iostream>
using namespace std;

struct ListNode {
        int val;
        ListNode *next;
        ListNode(int x) : val(x), next(NULL) {} 
 };

class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode* retHead;
        ListNode *tHead;
        
        bool first = true;
        int temp = 0;
        int isCarry = 0;
        int v1 = 0, v2 = 0;
        while (l1 != NULL || l2 != NULL)
        {
            v1 = v2 = 0;
            if (l1 != NULL)
            {
                v1 = l1->val;
                l1 = l1->next;
            }
            if (l2 != NULL)
            {
                v2 = l2->val;
                l2 = l2->next;
            }

            temp = isCarry + v1 + v2;
            isCarry = temp / 10;

            if (first == true)
            {
                first = false;
                retHead = new ListNode(temp % 10);
                tHead = retHead;
            }
            else
            {
                tHead->next = new ListNode(temp%10);
                tHead = tHead->next;
            }
        }

        if (isCarry != 0)
        {
                tHead->next = new ListNode(isCarry);
                tHead = tHead->next;
        }
        return retHead;
    }
};

int main()
{
    ListNode* pl1;
    ListNode* pl2;

    ListNode l1(1);
    ListNode l2(-1);
    
    pl1 = &l1;
    pl2 = &l2;

    Solution s;
    pl2 = s.addTwoNumbers(&l1, &l2);
    while (pl2 != NULL)
    {
        cout<<pl2->val<<endl;
        pl2 = pl2->next;
    }
    return 0;
}

