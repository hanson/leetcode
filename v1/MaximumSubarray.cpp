#include <iostream>
using namespace std;

class Solution
{
    public:
        int maxSubArray(int A[], int n) {
            if (n == 0)
                return 0;
            int ans = A[0];
            int temp = 0;
            for (int i = 0; i < n; ++i)
            {
                temp += A[i];
                if (temp > ans)
                {
                    ans = temp;
                }

                if (temp < 0)
                {
                    temp = 0;
                }
            }

            return ans;
        }
};

int main()
{}
