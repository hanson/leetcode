#include <iostream>
using namespace std;

/**
 ** Definition for singly-linked list.
 **/ 
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
    public:   
        bool candel(ListNode *cur, int dv)
        {
            if (cur->next!=NULL && cur->next->val==dv)
            {
                return true;
            }

            return false;
        }
        ListNode *deleteDuplicates(ListNode *head) {
            //check head
            
            while (head!=NULL && candel(head, head->val))
            {
                int hv = head->val;
                if (head->next!=NULL && head->next->val==hv)
                {
                    head = head->next->next;
                    while (head!=NULL && head->val==hv)
                    {
                        head = head->next;
                    }
                }
            }

            ListNode *tmp = head;
            while (tmp != NULL)
            {
                while (tmp->next!=NULL && candel(tmp->next, tmp->next->val))
                {
                    int tv = tmp->next->val;
                    tmp->next = tmp->next->next->next;
                    while (tmp->next!=NULL && tmp->next->val==tv)
                    {
                        tmp->next = tmp->next->next;
                    }
                }
                tmp = tmp->next;
            }

            return head;
        }
};

int main()
{
    return 0;
}

